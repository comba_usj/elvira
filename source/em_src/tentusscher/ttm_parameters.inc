!------------------------------------------------------------------------------
! This are the parameters for the TenTusscher Model
! KHWJ ten Tusscher, D Noble, PJ Noble, AV Panfilov.
! Am J Phys 286:H1573-H1589, 2004.
! KHWJ ten Tusscher and AV Panfilov.
! Am J Phys 291:H1088-H1100, 2006
! Constants
!
! Parameters that can be modified through the input file
!
  !.--Parameters for IKs -------------------------------------------------------
  real(rp), parameter, private :: p_pKNa = 0.03;             ! nS/pF
  !.--Parameters for IKr -------------------------------------------------------
  real(rp), parameter, private :: p_GKr = 0.153;             ! nS/pF
  !.--Parameters for IK1 -------------------------------------------------------
  real(rp), parameter, private :: p_GK1  = 5.405;             ! nS/pF
  !.--Parametros de corriente IKs e Ito ----------------------------------------
  real(rp), parameter, private :: p_GKs_endo =  0.392;  ! IKs conductance for endo
  real(rp), parameter, private :: p_GKs_mid  =  0.098;  ! IKs conductance for mid
  real(rp), parameter, private :: p_GKs_epi  =  0.392;  ! IKs conductance for epi
  real(rp), parameter, private :: p_Gto_endo =  0.073;  ! Ito conductance for endo
  real(rp), parameter, private :: p_Gto_mid  =  0.294;  ! Ito conductance for mid
  real(rp), parameter, private :: p_Gto_epi  =  0.294;  ! Ito conductance for epi
  real(rp), parameter, private :: p_nks = -1.0;         ! IKs channel density. If nks<0
                                                        ! implies a deterministic model
  !.--Parameters for INa -------------------------------------------------------
  real(rp), parameter, private :: p_GNa   = 14.838;           ! nS/pF
  !.--Parameters for ICaL ------------------------------------------------------
  real(rp), parameter, private :: p_GCaL  = 3.98E-5;          ! cm ms^-1 micro_F^-1
  !.--Parameter for IbNa -------------------------------------------------------
  real(rp), parameter, private :: p_GbNa  = 2.9E-4;           ! nS/pF
  !.--Parameter for IbCa -------------------------------------------------------
  real(rp), parameter, private :: p_GbCa  = 5.92E-4;          ! nS/pF
  !.--Parameter for IpCa -------------------------------------------------------
  real(rp), parameter, private :: p_GpCa  = 0.1238;           ! nS/pF
  !.--Parameter for IpK --------------------------------------------------------
  real(rp), parameter, private :: p_GpK   = 0.0146;           ! nS/pF
  !.--Parameters for INaK ------------------------------------------------------
  real(rp), parameter, private :: p_PNaK  = 2.724;            ! pA/pF
  !.--Parameter for INaCa ------------------------------------------------------
  real(rp), parameter, private :: p_kNaCa = 1000.0;           ! pA/pF
  !.--Parametros de corriente IKATP --------------------------------------------
  real(rp), parameter, private :: p_p0ATP_endo =  0.3348;  ! max chan open prob in absence of ATP
  real(rp), parameter, private :: p_p0ATP_mid  =  0.3348; ! max chan open prob in absence of ATP
  real(rp), parameter, private :: p_p0ATP_epi  =  0.3348; ! max chan open prob in absence of ATP
  real(rp), parameter, private :: p_aIKATP_endo=  0.32; ! parametro de Km
  real(rp), parameter, private :: p_aIKATP_mid =  0.86; ! parametro de Km
  real(rp), parameter, private :: p_aIKATP_epi =  1.0;  ! parametro de Km
  real(rp), parameter, private :: p_bIKATP_endo=  6.0;  ! parametro de H
  real(rp), parameter, private :: p_bIKATP_mid =  6.0;  ! parametro de H
  real(rp), parameter, private :: p_bIKATP_epi =  6.0;  ! parametro de H
  real(rp), parameter, private :: p_ATPi    =  6.8;            ! mmol intracellular ATP concentration
  real(rp), parameter, private :: p_ADPi    =  15.0;           ! mmol intracellular ADP concentration
  !.--External concentrations --------------------------------------------------
  real(rp), parameter, private :: p_Ko    = 5.4;               ! mM
  real(rp), parameter, private :: p_Nao   = 140.0;             ! mM
  real(rp), parameter, private :: p_Cao   = 2.0;               ! mM
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     =  8314.472;      !. JK^-1mol^-1  
  real(rp), parameter, private :: p_T     =   310.0;           !. K
  real(rp), parameter, private :: p_F     = 96485.3415;     !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;    !. J/C
  real(rp), parameter, private :: p_iRTF  = 1.0/p_RTF;
  real(rp), parameter, private :: p_pi    =  3.1415926535897931159980;      !. JK^-1mol^-1  
  !.--Capacitance --------------------------------------------------------------
  real(rp), parameter, private :: p_Cap   = 0.185;             !
  !.--Intracellular Volume -----------------------------------------------------
  real(rp), parameter, private :: p_Vc    = 0.016404;          ! micro_m^3
  real(rp), parameter, private :: p_Vsr   = 0.001094;          ! micro_m^3
  real(rp), parameter, private :: p_Vss   = 0.00005468;        ! micro_m^3
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_iVcF  = 1.0/(p_Vc*p_F);
  real(rp), parameter, private :: p_iVcF2 = 0.5*p_iVcF;
  real(rp), parameter, private :: p_iVssF2= 0.5/(p_Vss*p_F);
  !.--Correction factor --------------------------------------------------------
  real(rp),  parameter,private :: corFac  = (p_T/35.0-55.0/7.0)*sqrt(p_Ko/5.4);
  !.--Parameters for INaK ------------------------------------------------------
  real(rp), parameter, private :: p_KmK   = 1.0;               ! mM
  real(rp), parameter, private :: p_KmNa  = 40.0;              ! mM
  !.--Parameter for INaCa ------------------------------------------------------
  real(rp), parameter, private :: p_KmNai = 87.5;              ! mM
  real(rp), parameter, private :: p_KmCa  = 1.38;              ! mM
  real(rp), parameter, private :: p_ksat  = 0.1;               !
  real(rp), parameter, private :: p_gam   = 0.35;              !
  real(rp), parameter, private :: p_alf   = 2.5;               !
  !.--Parameter for IpCa -------------------------------------------------------
  real(rp), parameter, private :: p_KpCa  = 5.0E-4;            ! mM
  !.--Parameter for IpK --------------------------------------------------------
  !.--Intracellular calcium flux dynamics --------------------------------------
  real(rp), parameter, private :: p_Vmxu  = 6.375E-3;          ! mM/ms
  real(rp), parameter, private :: p_Kup   = 2.5E-4;            ! mM
  real(rp), parameter, private :: p_Vrel  = 0.102;             ! mM/ms
  real(rp), parameter, private :: p_k1p   = 0.15;              ! mM^-2 ms^-1
  real(rp), parameter, private :: p_k2p   = 0.045;             ! mM^-1 ms^-1
  real(rp), parameter, private :: p_k3    = 0.06;              ! ms^-1
  real(rp), parameter, private :: p_k4    = 0.005;             ! ms^-1
  real(rp), parameter, private :: p_EC    = 1.5;               ! mM
  real(rp), parameter, private :: p_maxsr = 2.5;               !
  real(rp), parameter, private :: p_minsr = 1.0;               !
  real(rp), parameter, private :: p_Vleak = 3.6E-4;            ! mM/ms
  real(rp), parameter, private :: p_Vxfer = 3.8E-3;            ! mM/ms
  !.--Calcium buffering dynamics -----------------------------------------------
  real(rp), parameter, private :: p_Bufc  = 0.2;               ! mM
  real(rp), parameter, private :: p_Kbufc = 0.001;             ! mM
  real(rp), parameter, private :: p_Bufsr = 10.0;              ! mM
  real(rp), parameter, private :: p_Kbufsr= 0.3;               ! mM
  real(rp), parameter, private :: p_Bufss = 0.4;               ! mM
  real(rp), parameter, private :: p_Kbufss= 2.5E-4;            ! mM
  !.--Parameter for IKATP ------------------------------------------------------
  real(rp), parameter, private :: p_sigATP  =  3.8e8;          ! channel density
  real(rp), parameter, private :: p_MgiATP  =  0.50;           ! mmol/L intracellular Mg
  real(rp), parameter, private :: p_dMgATP  =  0.32;           ! electric distance for Mg
  real(rp), parameter, private :: p_dNaATP  =  0.35;           ! electric distance for Na
  real(rp), parameter, private :: p_Kh0NaATP= 25.9;            !.mmol/L
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS ------------------------------------------------------
! Value corresponding to 30 minutes pacing at BCL=1000ms
!------------------------------------------------------------------------------
real(rp), parameter :: Vi_ENDO_TT = -8.5444032657e+01, Vi_MID_TT = -8.5289523078e+01,&
                       Vi_EPI_TT  = -8.5403998235e+01;
real(rp), parameter :: end_Cai =  1.0302330000e-04, mid_Cai =  1.1603440000e-04,&
                        epi_Cai =  1.0499010000e-04, ttd_Cai =  1.0499010000e-04;
real(rp), parameter :: end_CaSR =  3.4225964164e+00, mid_CaSR =  4.1489844211e+00,&
                       epi_CaSR =  3.5211564164e+00, ttd_CaSR =  3.5211564164e+00;
real(rp), parameter :: end_CaSS =  2.1191500000e-04, mid_CaSS =  2.3327530000e-04,&
                       epi_CaSS =  2.1369610000e-04, ttd_CaSS =  2.1369610000e-04;
real(rp), parameter :: end_Nai =  9.6826154537e+00, mid_Nai =  9.4997097375e+00,&
                       epi_Nai =  9.9292835868e+00, ttd_Nai =  9.9292835868e+00;
real(rp), parameter :: end_Ki =  1.3584985711e+02, mid_Ki =  1.3591402586e+02,&
                       epi_Ki =  1.3558662002e+02, ttd_Ki =  1.3558662002e+02;
real(rp), parameter :: end_m =  1.6391752000e-03, mid_m =  1.6938733000e-03,&
                       epi_m =  1.6531795000e-03, ttd_m =  1.6531795000e-03;
real(rp), parameter :: end_h =  7.5072844380e-01, mid_h =  7.4652265100e-01,&
                       epi_h =  7.4964594160e-01, ttd_h =  7.4964594160e-01;
real(rp), parameter :: end_j =  7.5035400180e-01, mid_j =  7.4569534840e-01,&
                       epi_j =  7.4926206290e-01, ttd_j =  7.4926206290e-01;
real(rp), parameter :: end_xs =  3.2167963000e-03, mid_xs =  3.3402198000e-03,&
                       epi_xs =  3.2251183000e-03, ttd_xs =  3.2251183000e-03;
real(rp), parameter :: end_r =  2.3300000000e-08, mid_r =  2.3900000000e-08,&
                       epi_r =  2.3500000000e-08, ttd_r =  2.3500000000e-08;
real(rp), parameter :: end_s =  6.4003744940e-01, mid_s =  9.9999786630e-01,&
                       epi_s =  9.9999791480e-01, ttd_s =  9.9999791480e-01;
real(rp), parameter :: end_d =  3.2773200000e-05, mid_d =  3.3455400000e-05,&
                       epi_d =  3.2948600000e-05, ttd_d =  3.2948600000e-05;
real(rp), parameter :: end_f =  9.7725169040e-01, mid_f =  9.5986587370e-01,&
                       epi_f =  9.7717472010e-01, ttd_f =  9.7717472010e-01;
real(rp), parameter :: end_f2 =  9.9950318230e-01, mid_f2 =  9.9949204940e-01,&
                       epi_f2 =  9.9950033390e-01, ttd_f2 =  9.9950033390e-01;
real(rp), parameter :: end_fcass =  9.9997224760e-01, mid_fcass =  9.9996112080e-01,&
                       epi_fcass =  9.9997253650e-01, ttd_fcass =  9.9997253650e-01;
real(rp), parameter :: end_rr =  9.8906089700e-01, mid_rr =  9.8737278640e-01,&
                       epi_rr =  9.8911588090e-01, ttd_rr =  9.8911588090e-01;
real(rp), parameter :: end_oo =  8.9400000000e-08, mid_oo =  1.1450000000e-07,&
                       epi_oo =  9.1800000000e-08, ttd_oo =  9.1800000000e-08;
real(rp), parameter :: end_xr1 =  2.0565190000e-04, mid_xr1 =  2.1381860000e-04,&
                       epi_xr1 =  2.0685860000e-04, ttd_xr1 =  2.0685860000e-04;
real(rp), parameter :: end_xr2 =  4.7339975940e-01, mid_xr2 =  4.7179479240e-01,&
                       epi_xr2 =  4.7298392590e-01, ttd_xr2 =  4.7298392590e-01;
!------------------------------------------------------------------------------
!    tt_ic = (/ 0.000126, 3.64, 0.00036,  8.604, 136.89,  0.00172,        &  Oxford benchmark
!               0.7444,   0.7045, 0.0095, 2.42e-8, 0.99998, 3.373e-5,    &
!               0.7888,   0.9755, 0.9953, 0.9073,  2.414e-7,  0.00621,      &
!               0.4712 /)
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: TT_ENDO_mod =  1;  ! tenTusscher ENDO model    (VH)
  integer(ip), parameter :: TT_MID_mod  =  2;  ! tenTusscher MID model     (VH)
  integer(ip), parameter :: TT_EPI_mod  =  3;  ! tenTusscher EPI model     (VH)
  integer(ip),parameter  :: nvar_tt     = 19;  ! Number of state variables
  integer(ip),parameter  :: ncur_tt     = 14;  ! Number of currents
  integer(ip),parameter  :: np_tt       = 25;  ! Number of modifiable paramters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.  
  integer(ip), parameter    :: m_stpTT   =   5
  integer(ip), parameter    :: m_iteTT   =   5
  real(rp),    parameter    :: m_dvdtTT  = 1.0
!------------------------------------------------------------------------------
!-- INITIAL CONDITIONS OLD   --------------------------------------------------
!------------------------------------------------------------------------------
!
!  real(rp), parameter   :: Vi_TT  = -85.4440324065;
!  real(rp), parameter :: end_Cai  =   1.030231e-4, mid_Cai  =  1.0789e-4,    &     
!                         epi_Cai  =   9.4467e-5, ttd_Cai  =  9.4467e-5;    ! mM
!  real(rp), parameter :: end_CaSR =   3.4225860694,    mid_CaSR =  3.7618,       &      
!                         epi_CaSR =   3.0436,    ttd_CaSR =  3.0436;       ! mM
!  real(rp), parameter :: end_CaSS =   2.119148e-4, mid_CaSS =  1.728e-4,     &
!                         epi_CaSS =   1.5803e-4, ttd_CaSS =  1.5803e-4;    ! mM
!  real(rp), parameter :: end_Nai  =   9.6825482853,    mid_Nai  =  8.9137,       &
!                         epi_Nai  =   8.2473,    ttd_Nai  =  8.2473;       ! mM
!  real(rp), parameter :: end_Ki   = 135.8498866512,    mid_Ki   = 136.5626,      &
!                         epi_Ki   = 137.3508,    ttd_Ki   = 137.3508;      ! mM
!  real(rp), parameter :: end_m    =   0.0014,    mid_m    =   0.00150,     &
!                         epi_m    =   0.0014,    ttd_m    =   0.00140;     ! 
!  real(rp), parameter :: end_h    =   0.7766,    mid_h    =   0.7614,      &
!                         epi_h    =   0.7666,    ttd_h    =   0.7666;      !
!  real(rp), parameter :: end_j    =   0.7765,    mid_j    =   0.7613,      &      
!                         epi_j    =   0.7660,    ttd_j    =   0.7660;      !
!  real(rp), parameter :: end_xs   =   0.0031,    mid_xs   =   0.0031,      &    
!                         epi_xs   =   0.0031,    ttd_xs   =   0.0031;      !
!  real(rp), parameter :: end_r    =   2.1091e-08,   mid_r =   2.1813e-08,  &   
!                         epi_r    =   2.1161e-08,   ttd_r =   2.1161e-08;  !
!  real(rp), parameter :: end_s    =   0.9129,    mid_s    =   1.0,         &      
!                         epi_s    =   1.0,       ttd_s    =   1.0;         !
!  real(rp), parameter :: end_d    =   3.0242e-5,   mid_d  =   3.1068e-5,   &   
!                         epi_d    =   3.0323e-5,   ttd_d  =   3.0323e-5;   !
!  real(rp), parameter :: end_f    =   0.9998,    mid_f    =   0.9996,      &     
!                         epi_f    =   0.9998,    ttd_f    =   0.9998;      !
!  real(rp), parameter :: end_f2   =   0.9995,    mid_f2   =   0.9995,      &     
!                         epi_f2   =   0.9995,    ttd_f2   =   0.9995;      ! 
!  real(rp), parameter :: end_fcass=   1.0,      mid_fcass =   1.0,         &       
!                         epi_fcass=   1.0,      ttd_fcass =   1.0;         !    
!  real(rp), parameter :: end_rr   =   0.9981,   mid_rr    =   0.9980,      &        
!                         epi_rr   =   0.9981,   ttd_rr    =   0.9981;      !   
!  real(rp), parameter :: end_oo   =   4.6333e-08, mid_oo = 6.1783e-08,    &        
!                         epi_oo   =   4.8188e-08, ttd_oo  = 4.8188e-08;    ! 
!  real(rp), parameter :: end_xr1  =   1.8820e-04, mid_xr1 = 1.9373e-4,     &    
!                         epi_xr1  =   1.8874e-04, ttd_xr1 = 1.8874e-4;     !
!  real(rp), parameter :: end_xr2  =   0.47970,  mid_xr2   =   0.4776,      &    
!                         epi_xr2  =   0.47950,  ttd_xr2   =   0.47950;     !
