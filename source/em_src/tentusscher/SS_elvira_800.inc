real(rp), parameter :: Vi_ENDO_TT = -8.5249885996e+01, Vi_MID_TT = -8.5112248500e+01,&
                       Vi_EPI_TT  = -8.5204836522e+01;
real(rp), parameter :: end_Cai =  1.1423474891e-04, mid_Cai =  1.2703227106e-04,&
                       epi_Cai =  1.1630572404e-04, ttd_Cai =  1.1630572404e-04;
real(rp), parameter :: end_CaSR =  3.9014765236e+00, mid_CaSR =  4.5658669132e+00,&
                       epi_CaSR =  3.9998174882e+00, ttd_CaSR =  3.9998174882e+00;
real(rp), parameter :: end_CaSS =  2.5503918542e-04, mid_CaSS =  2.8428942336e-04,&
                       epi_CaSS =  2.5696599013e-04, ttd_CaSS =  2.5696599013e-04;
real(rp), parameter :: end_Nai =  1.0624037140e+01, mid_Nai =  1.0207109579e+01,&
                       epi_Nai =  1.0952985546e+01, ttd_Nai =  1.0952985546e+01;
real(rp), parameter :: end_Ki =  1.3482853751e+02, mid_Ki =  1.3513968487e+02,&
                       epi_Ki =  1.3448362407e+02, ttd_Ki =  1.3448362407e+02;
real(rp), parameter :: end_m =  1.7081915922e-03, mid_m =  1.7588345862e-03,&
                       epi_m =  1.7246077608e-03, ttd_m =  1.7246077608e-03;
real(rp), parameter :: end_h =  7.4540681561e-01, mid_h =  7.4156348909e-01,&
                       epi_h =  7.4416846995e-01, ttd_h =  7.4416846995e-01;
real(rp), parameter :: end_j =  7.4266921109e-01, mid_j =  7.3460891822e-01,&
                       epi_j =  7.4135142911e-01, ttd_j =  7.4135142911e-01;
real(rp), parameter :: end_xs =  3.5559556241e-03, mid_xs =  4.4413338691e-03,&
                       epi_xs =  3.5536359042e-03, ttd_xs =  3.5536359042e-03;
real(rp), parameter :: end_r =  2.4089911849e-08, mid_r =  2.4652227052e-08,&
                       epi_r =  2.4271521098e-08, ttd_r =  2.4271521098e-08;
real(rp), parameter :: end_s =  5.2536763653e-01, mid_s =  9.9999778797e-01,&
                       epi_s =  9.9999782935e-01, ttd_s =  9.9999782935e-01;
real(rp), parameter :: end_d =  3.3632953915e-05, mid_d =  3.4256219347e-05,&
                       epi_d =  3.3835580034e-05, ttd_d =  3.3835580034e-05;
real(rp), parameter :: end_f =  9.3936488314e-01, mid_f =  8.9643568232e-01,&
                       epi_f =  9.3934898620e-01, ttd_f =  9.3934898620e-01;
real(rp), parameter :: end_f2 =  9.9948898694e-01, mid_f2 =  9.9947857025e-01,&
                       epi_f2 =  9.9948568590e-01, ttd_f2 =  9.9948568590e-01;
real(rp), parameter :: end_fcass =  9.9981160251e-01, mid_fcass =  9.9973606119e-01,&
                       epi_fcass =  9.9981847649e-01, ttd_fcass =  9.9981847649e-01;
real(rp), parameter :: end_rr =  9.7440753235e-01, mid_rr =  9.7082753931e-01,&
                       epi_rr =  9.7460946174e-01, ttd_rr =  9.7460946174e-01;
real(rp), parameter :: end_oo =  1.3280311397e-07, mid_oo =  1.7115628037e-07,&
                       epi_oo =  1.3578125674e-07, ttd_oo =  1.3578125674e-07;
real(rp), parameter :: end_xr1 =  2.5141724983e-04, mid_xr1 =  4.8574705480e-04,&
                       epi_xr1 =  2.5448260349e-04, ttd_xr1 =  2.5448260349e-04;
real(rp), parameter :: end_xr2 =  4.7138220680e-01, mid_xr2 =  4.6995183539e-01,&
                       epi_xr2 =  4.7091447750e-01, ttd_xr2 =  4.7091447750e-01;
