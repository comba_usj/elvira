!------------------------------------------------------------------------------
! This are the parameters for the Grandi Model
! Grandi E, et al, A novel computational model of the human ventricular action potential and Ca 
! transient, J Mol Cell Cardiol (2009), doi:10.1016/j.yjmcc.2009.09.019

! Constants
!
! Parameters that can be modified through the input file
!
  !.--K currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_pNaK = 0.01833;             ! nS/pF
  real(rp), parameter, private :: p_GKp = 0.002;				! mS/uF
  real(rp), parameter, private :: p_GKsJunc = 0.0035;			! mS/uF
  real(rp), parameter, private :: p_GKsSL = 0.0035;			! mS/uF
  real(rp), parameter, private :: p_Gtos_endo =  0.13*0.3*0.964;  ! Ito conductance for endo
  real(rp), parameter, private :: p_Gtos_epi  =  1.0*0.13*0.12;   ! Ito conductance for epi
  real(rp), parameter, private :: p_Gtof_endo =  0.13*0.3*0.036;  ! Ito conductance for endo
  real(rp), parameter, private :: p_Gtof_epi  =  1.0*0.13*0.88;   ! Ito conductance for epi
  !.--Na transport -------------------------------------------------------------
  real(rp), parameter, private :: p_GNa   = 23;              ! nS/pF
  real(rp), parameter, private :: p_GNaB   = 0.597e-3;              ! nS/pF
  !.--Ca transport -------------------------------------------------------------
  real(rp), parameter, private :: p_pNa  = 0.50*1.5e-8;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pCa  = 0.50*5.4e-4;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_pK  = 0.50*2.7e-7;            ! cm ms^-1 micro_F^-1
  real(rp), parameter, private :: p_GCaB = 5.513e-4;      ! [A/F]
  !.--Cl currents --------------------------------------------------------------
  real(rp), parameter, private :: p_GClCa  = 0.5* 0.109625;               !  [mS/uF]
  real(rp), parameter, private :: p_GClB  = 9e-3;               !  [mS/uF]
  !.--Concentrations ----------------------------------------------------------
  real(rp), parameter, private :: p_Cli = 15;	! [mM]
  real(rp), parameter, private :: p_Clo = 150;	! [mM]
  real(rp), parameter, private :: p_Ko = 5.4;	! [mM]
  real(rp), parameter, private :: p_Nao = 140;	! [mM]
  real(rp), parameter, private :: p_Cao = 1.8;	! [mM]
  real(rp), parameter, private :: p_Mgi = 1;	! [mM]
!------------------------------------------------------------------------------
!-- FIXED PARAMETERS ----------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter, private :: p_R     = 8314;			!. JK^-1kmol^-1  
  real(rp), parameter, private :: p_T     = 310.0;          !. K
  real(rp), parameter, private :: p_F     = 96485;          !. C/mol
  real(rp), parameter, private :: p_RTF   = p_R*p_T/p_F;    !. 1/mV
  real(rp), parameter, private :: p_iRTF  = p_F/p_R/p_T;    !. mV
  !.--Capacitance --------------------------------------------------------------
  real(rp), parameter, private :: p_Cap   = 1.3810e-10;	! [F]
  !.--Enviromental parameters --------------------------------------------------
  real(rp), parameter, private :: pi = 2*acos(0.0);
  real(rp), parameter, private :: p_cellLength = 100;     !. cell length [um]
  real(rp), parameter, private :: p_cellRadius = 10.25;   !. cell radius [um]
  real(rp), parameter, private :: p_JuncLength = 160e-3;  !. Junc length [um]
  real(rp), parameter, private :: p_JuncRadius = 15e-3;   !. Junc radius [um]
  real(rp), parameter, private :: p_distSLcyto = 0.45;    !. dist. SL to cytosol [um]
  real(rp), parameter, private :: p_distJuncSL = 0.5;     !. dist. Junc to SL [um]
  real(rp), parameter, private :: p_DcaJuncSL = 1.64e-6;  !. Dca Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DcaSLcyto = 1.22e-6;  !. Dca SL to cyto [cm^2/sec]
  real(rp), parameter, private :: p_DnaJuncSL = 1.09e-5;  !. Dna Junc to SL [cm^2/sec]
  real(rp), parameter, private :: p_DnaSLcyto = 1.79e-5;  !. Dna SL to cyto [cm^2/sec] 
  real(rp), parameter, private :: p_Vc = pi*p_cellRadius**2*p_cellLength*1e-15;  ! [L]
  real(rp), parameter, private :: p_VMyo = 0.65*p_Vc;
  real(rp), parameter, private :: p_VSR = 0.035*p_Vc;
  real(rp), parameter, private :: p_VSL = 0.02*p_Vc;
  real(rp), parameter, private :: p_VJunc = 0.0539*0.01*p_Vc; 
  real(rp), parameter, private :: p_SAJunc = 20150*pi*2*p_JuncLength*p_JuncRadius;  ! [um^2]
  real(rp), parameter, private :: p_SASL = pi*2*p_cellRadius*p_cellLength;          ! [um^2]
  !------------------------------------------------------------------------------
  real(rp), parameter, private :: p_JCaJuncSL =1/1.2134e12; ! [L/msec] = 8.2413e-13
  real(rp), parameter, private :: p_JCaSLMyo = 1/2.68510e11; ! [L/msec] = 3.2743e-12
  real(rp), parameter, private :: p_JNaJuncSL = 1/(1.6382e12/3*100); ! [L/msec] = 6.1043e-13
  real(rp), parameter, private :: p_JNaSLMyo = 1/(1.8308e10/3*100);  ! [L/msec] = 5.4621e-11
  !.--Fractional Currents--------------------------------------------------------
  real(rp), parameter, private :: p_FJunc = 0.11 !
  real(rp), parameter, private :: p_FSL = 1 - p_FJunc !
  real(rp), parameter, private :: p_FJuncCaL = 0.9 !
  real(rp), parameter, private :: p_FSLCaL = 1 - p_FJuncCaL !
  !.--Na Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_IbarNaK = 1.0*1.8;		!. [A/F]
  real(rp), parameter, private :: p_KmNaip = 11;		!. [mM]
  real(rp), parameter, private :: p_KmKo = 1.5;			!. [mM]
  !.--Cl Currents ---------------------------------------------------------------
  real(rp), parameter, private :: p_KdClCa = 100e-3;	!. [mM]
  !.--Ca Transport --------------------------------------------------------------
  real(rp), parameter, private :: p_IbarNCX = 1.0*4.5;           ! [A/F]
  real(rp), parameter, private :: p_KmCai = 3.59e-3;      ! [mM]
  real(rp), parameter, private :: p_KmCao = 1.3;          ! [mM]
  real(rp), parameter, private :: p_KmNai = 12.29;        ! [mM]
  real(rp), parameter, private :: p_KmNao = 87.5;         ! [mM]
  real(rp), parameter, private :: p_ksat = 0.32;           ! [-]
  real(rp), parameter, private :: p_nu = 0.27;              ! [-]
  real(rp), parameter, private :: p_Kdact = 0.150e-3;      ! [mM]
  real(rp), parameter, private :: p_IbarPMCA = 0.0673;        ! [A/F]
  real(rp), parameter, private :: p_KmPCa = 0.5e-3;        ![mM]
  !.--SR Ca Fluxes --------------------------------------------------------------
  real(rp), parameter, private :: p_VmaxSRCaP = 1.0*5.3114e-3;  ! [mM/ms]
  real(rp), parameter, private :: p_Kmf = 0.246e-3;            ! [mM]
  real(rp), parameter, private :: p_Kmr = 1.7;                 ! [mM]
  real(rp), parameter, private :: p_hillSRCaP = 1.787;       ! [-]
  real(rp), parameter, private :: p_ks = 25;                    ! [ms^-1]
  real(rp), parameter, private :: p_koCa = 10;                 ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kom = 0.06;                ! [ms^-1]
  real(rp), parameter, private :: p_kiCa = 0.5;                ! [ms^-1*mM^2]
  real(rp), parameter, private :: p_kim = 0.005;               ! [ms^-1]
  real(rp), parameter, private :: p_ec50SR = 0.45;             ! [mM]
  !.--Buffering -----------------------------------------------------------------
  real(rp), parameter, private :: p_BmaxNaj = 7.561;                         ! [mM]
  real(rp), parameter, private :: p_BmaxNaSL = 1.65;                         ! [mM]
  real(rp), parameter, private :: p_koffNa = 1e-3;                            ! [ms^-1]
  real(rp), parameter, private :: p_konNa = 0.1e-3;                           ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnClow = 70e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffTnCl = 19.6e-3;                      ! [ms^-1]
  real(rp), parameter, private :: p_konTnCl = 32.7;                          ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxTnChigh = 140e-3;                    ! [mM]
  real(rp), parameter, private :: p_koffTnChCa = 0.032e-3;                  ! [ms^-1]
  real(rp), parameter, private :: p_konTnChCa = 2.37;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffTnChMg = 3.33e-3;                   ! [ms^-1]
  real(rp), parameter, private :: p_konTnChMg = 3e-3;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCaM = 24e-3;                          ! [mM]
  real(rp), parameter, private :: p_koffCaM = 238e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konCaM = 34;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxMyosin = 140e-3;                      ! [mM]
  real(rp), parameter, private :: p_koffMyoCa = 0.46e-3;                     ! [ms^-1]
  real(rp), parameter, private :: p_konMyoCa = 13.8;                         ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_koffMyoMg = 0.057e-3;                    ! [ms^-1]
  real(rp), parameter, private :: p_konMyoMg = 0.0157;                       ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSR = 19*.9e-3;                        ! [mM]
  real(rp), parameter, private :: p_koffSR = 60e-3;                           ! [ms^-1]
  real(rp), parameter, private :: p_konSR = 100;                              ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLlowSL = 37.4e-3*p_VMyo/p_VSL;       ! [mM]
  real(rp), parameter, private :: p_BmaxSLlowj = 4.6e-3*p_VMyo/p_VJunc*0.1;     ! [mM]
  real(rp), parameter, private :: p_koffSLl = 1300e-3;                       ! [ms^-1]
  real(rp), parameter, private :: p_konSLl = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxSLhighSL = 13.4e-3*p_VMyo/p_VSL;      ! [mM]
  real(rp), parameter, private :: p_BmaxSLhighj = 1.65e-3*p_VMyo/p_VJunc*0.1;   ! [mM]
  real(rp), parameter, private :: p_koffSLh = 30e-3;                         ! [ms^-1]
  real(rp), parameter, private :: p_konSLh = 100;                            ! [ms^-1*mM^-1]
  real(rp), parameter, private :: p_BmaxCsqn = 140e-3*p_VMyo/p_VSR;             ! [mM]
  real(rp), parameter, private :: p_koffCsqn = 65;                            ! [ms^-1]
  real(rp), parameter, private :: p_konCsqn = 100;                            ! [ms^-1*mM^-1]

!------------------------------------------------------------------------------
!-- INITIAL VALUES ------------------------------------------------------------
!------------------------------------------------------------------------------
  real(rp), parameter :: end_m =  3.911094e-003,       epi_m =  3.900097e-003,       &
                         def_m =  3.900097e-003        !
  real(rp), parameter :: end_h =  6.210155e-001,       epi_h =  6.214958e-001,       &
                         def_h =  6.214958e-001        !
  real(rp), parameter :: end_j =  6.189618e-001,       epi_j =  6.196458e-001,       &
                         def_j =  6.196458e-001        !
  real(rp), parameter :: end_d =  2.996835e-006,       epi_d =  2.990069e-006,       &
                         def_d =  2.990069e-006        !
  real(rp), parameter :: end_f =  9.950482e-001,       epi_f =  9.950564e-001,       &
                         def_f =  9.950564e-001        !
  real(rp), parameter :: end_fCaBJ =  2.469851e-002,   epi_fCaBJ =  2.453941e-002,   &
                         def_fCaBJ =  2.453941e-002    !
  real(rp), parameter :: end_fCaBSL =  1.512535e-002,  epi_fCaBSL =  1.505070e-002,  &
                         def_fCaBSL =  1.505070e-002   !
  real(rp), parameter :: end_xtos =  4.454684e-004,    epi_xtos =  4.450043e-004,    &
                         def_xtos =  4.450043e-004     !
  real(rp), parameter :: end_ytos =  7.988522e-001,    epi_ytos =  7.894415e-001,    &
                         def_ytos =  7.894415e-001     !
  real(rp), parameter :: end_xtof =  4.454607e-004,    epi_xtof =  4.449965e-004,    &
                         def_xtof =  4.449965e-004     !
  real(rp), parameter :: end_ytof =  9.999957e-001,    epi_ytof =  9.999957e-001,    &
                         def_ytof =  9.999957e-001     !
  real(rp), parameter :: end_xkr =  2.393894e-002,     epi_xkr =  2.073871e-002,     &
                         def_xkr =  2.073871e-002      !
  real(rp), parameter :: end_xks =  4.324501e-003,     epi_xks =  4.320405e-003,     &
                         def_xks =  4.320405e-003      !
  real(rp), parameter :: end_RyRr =  8.938999e-001,    epi_RyRr =  8.937827e-001,    &
                         def_RyRr =  8.937827e-001     !
  real(rp), parameter :: end_RyRo =  7.725057e-007,    epi_RyRo =  7.538132e-007,    &
                         def_RyRo =  7.538132e-007     !
  real(rp), parameter :: end_RyRi =  9.169068e-008,    epi_RyRi =  8.958258e-008,    &
                         def_RyRi =  8.958258e-008     !
  real(rp), parameter :: end_NaBJ =  3.367266e+000,    epi_NaBJ =  3.372983e+000,    &
                         def_NaBJ =  3.372983e+000     !
  real(rp), parameter :: end_NABSL =  7.346819e-001,   epi_NABSL =  7.359366e-001,   &
                         def_NABSL =  7.359366e-001    !
  real(rp), parameter :: end_TnCL =  9.119216e-003,    epi_TnCL =  9.060890e-003,    &
                         def_TnCL =  9.060890e-003     !
  real(rp), parameter :: end_TnCHC =  1.184444e-001,   epi_TnCHC =  1.186054e-001,   &
                         def_TnCHC =  1.186054e-001    !
  real(rp), parameter :: end_TnCHM =  1.011995e-002,   epi_TnCHM =  1.004046e-002,   &
                         def_TnCHM =  1.004046e-002    !
  real(rp), parameter :: end_CaM =  3.025161e-004,     epi_CaM =  3.003395e-004,     &
                         def_CaM =  3.003395e-004      !
  real(rp), parameter :: end_MyoC =  2.022374e-003,    epi_MyoC =  2.035135e-003,    &
                         def_MyoC =  2.035135e-003     !
  real(rp), parameter :: end_MyoM =  1.374598e-001,    epi_MyoM =  1.374465e-001,    &
                         def_MyoM =  1.374465e-001     !
  real(rp), parameter :: end_SRB =  2.218591e-003,     epi_SRB =  2.204483e-003,     &
                         def_SRB =  2.204483e-003      !
  real(rp), parameter :: end_SLLJ =  7.414411e-003,    epi_SLLJ =  7.367636e-003,    &
                         def_SLLJ =  7.367636e-003     !
  real(rp), parameter :: end_SLLSL =  9.812321e-003,   epi_SLLSL =  9.766825e-003,   &
                         def_SLLSL =  9.766825e-003    !
  real(rp), parameter :: end_SLHJ =  7.364450e-002,    epi_SLHJ =  7.334626e-002,    &
                         def_SLHJ =  7.334626e-002     !
  real(rp), parameter :: end_SLHSL =  1.137895e-001,   epi_SLHSL =  1.133926e-001,   &
                         def_SLHSL =  1.133926e-001    !
  real(rp), parameter :: end_CSQNB =  1.214938e+000,   epi_CSQNB =  1.209384e+000,   &
                         def_CSQNB =  1.209384e+000    !
  real(rp), parameter :: end_CaSR =  5.701623e-001,    epi_CaSR =  5.652888e-001,    &
                         def_CaSR =  5.652888e-001     !
  real(rp), parameter :: end_NaJ =  8.025553e+000,     epi_NaJ =  8.050023e+000,     &
                         def_NaJ =  8.050023e+000      !
  real(rp), parameter :: end_NaSL =  8.024220e+000,    epi_NaSL =  8.048801e+000,    &
                         def_NaSL =  8.048801e+000     !
  real(rp), parameter :: end_Nai =  8.024364e+000,     epi_Nai =  8.048952e+000,     &
                         def_Nai =  8.048952e+000      !
  real(rp), parameter :: end_Ki =  1.200000e+002,      epi_Ki =  1.200000e+002,      &
                         def_Ki =  1.200000e+002       !
  real(rp), parameter :: end_CaJ =  1.761034e-004,     epi_CaJ =  1.749776e-004,     &
                         def_CaJ =  1.749776e-004      !
  real(rp), parameter :: end_CaSL =  1.057899e-004,    epi_CaSL =  1.052956e-004,    &
                         def_CaSL =  1.052956e-004     !
  real(rp), parameter :: end_Cai =  8.932657e-005,     epi_Cai =  8.867626e-005,     &
                         def_Cai =  8.867626e-005      !
  real(rp), parameter :: Vi_GR_end  = -8.130773e+001,  Vi_GR_epi  = -8.132129e+001   
!------------------------------------------------------------------------------
!-- STRUCTURE PARAMETERS ------------------------------------------------------
!------------------------------------------------------------------------------
  integer(ip), parameter :: GR_ENDO_mod = 12;  ! Grandi ENDO model         (VH)
  integer(ip), parameter :: GR_EPI_mod  = 13;  ! Grandi EPI model          (VH)
  integer(ip),parameter  :: nvar_gr     = 38;  ! Number of state variables
  integer(ip),parameter  :: ncur_gr     = 24;  ! Number of currents
  integer(ip),parameter  :: np_gr       = 20;  ! Number of modifiable parameters
!------------------------------------------------------------------------------
!-- ADAPTIVE TIME STEPING -----------------------------------------------------
!------------------------------------------------------------------------------
!.  
  integer(ip), parameter    :: m_stpGR   =    5
  real(rp),    parameter    :: m_dvdtGR  =  1.0
