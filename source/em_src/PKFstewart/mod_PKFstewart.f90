! This module implements the Stewart et al. Model for Purkinje FIbers
! Mathematical models of the electrical action potential of Purkinje fibre
! cells
! Philip Stewart, Oleg V. Aslanidi, Denis Noble, Penelope J. Noble, Mark R.
! Boyett and Henggui Zhang
! Phil. Trans. R. Soc. A 2009 367, 2225-2255
! doi: 10.1098/rsta.2008.0283
!
! 30-11-2009: PKFstewart_A_P01 returns currents vector for output (EAH)
! 22-03-2010: Modification for updating concentrations (JFR)
!
!------------------------------------------------------------------------------
module mod_PKFstewart
!------------------------------------------------------------------------------
  use mod_precision
  implicit none
  include 'pkf_parameters.inc'
  !.
  type t_pkf
    sequence
    real(rp)  ::  Cai, CaSR, CaSS,  Nai, Ki, m,   h,   j,   xs,  r,  s, d,     &
                  f,   f2,   fcass, rr,  oo, xr1, xr2, y
  end type t_pkf
  !.
  type, private :: t_cur
    sequence
    real(rp)  ::  IK1, Ito, Isus, INa, ICaL, IKs, IKr, INaCa, INaK, IpCa,   &
                  IpK, IbNa, IbCa, IfNa, IfK 
  end type t_cur
  
  public  :: get_parameter_PKF, ic_PKFstewart, PKFstewart_A_P01
  private :: concentrations, gates, currents

  type, public:: t_prm
    private
    real(rp) :: p_pKNa        !.  1
    real(rp) :: p_GKr         !.  2
    real(rp) :: p_GK1         !.  3
    real(rp) :: p_GKs         !.  4
    real(rp) :: p_Gto         !.  5
    real(rp) :: p_GNa         !.  6
    real(rp) :: p_GCaL        !.  7
    real(rp) :: p_GbNa        !.  8
    real(rp) :: p_GbCa        !.  9
    real(rp) :: p_GpCa        !. 10
    real(rp) :: p_GpK         !. 11
    real(rp) :: p_PNaK        !. 12
    real(rp) :: p_kNaCa       !. 13
    real(rp) :: p_Ko          !. 14
    real(rp) :: p_Nao         !. 15
    real(rp) :: p_Cao         !. 16
    real(rp) :: p_Gsus        !. 17
    real(rp) :: p_GfK         !. 18
    real(rp) :: p_GfNa        !. 19
  end type t_prm
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_ST_PK(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)',advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'CaSR '
    write(lu_st,'(A)',advance='no') 'CaSS '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'h '
    write(lu_st,'(A)',advance='no') 'j '
    write(lu_st,'(A)',advance='no') 'xs '
    write(lu_st,'(A)',advance='no') 'r '
    write(lu_st,'(A)',advance='no') 's '
    write(lu_st,'(A)',advance='no') 'd '
    write(lu_st,'(A)',advance='no') 'f '
    write(lu_st,'(A)',advance='no') 'f2 '
    write(lu_st,'(A)',advance='no') 'fcass '
    write(lu_st,'(A)',advance='no') 'rr '
    write(lu_st,'(A)',advance='no') 'oo '
    write(lu_st,'(A)',advance='no') 'xr1 '
    write(lu_st,'(A)',advance='no') 'xr2 '
    write(lu_st,'(A)',advance='no') 'y '
end subroutine write_state_ST_PK
!------------------------------------------------------------------------------!
subroutine write_current_ST_PK(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)',advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'IpK '
    write(lu_cr,'(A)',advance='no') 'Ito '
    write(lu_cr,'(A)',advance='no') 'Isus '
    write(lu_cr,'(A)',advance='no') 'IfK '
    write(lu_cr,'(A)',advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'IfNa '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'IbCa '
    write(lu_cr,'(A)',advance='no') 'IpCa '
    write(lu_cr,'(A)',advance='no') 'Itot '
end subroutine write_current_ST_PK
!------------------------------------------------------------------------------!
function get_parameter_PKF (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_pkf)

  select case (tcell)
  case (ST_PK_mod)
    v_prm = (/ 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, & 
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, &
               1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp, 1.0_rp /)
  case default
    write (*,10) tcell; stop
  end select
  return
10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_PKF
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)     :: v_prm(:)
  type (t_prm), intent(out):: param

  param%p_pKNa  = v_prm( 1)*p_pKNa    !.  1
  param%p_GKr   = v_prm( 2)*p_GKr     !.  2
  param%p_GK1   = v_prm( 3)*p_GK1     !.  3
  param%p_GKs   = v_prm( 4)*p_GKs     !.  4
  param%p_Gto   = v_prm( 5)*p_Gto     !.  5
  param%p_GNa   = v_prm( 6)*p_GNa     !.  6
  param%p_GCaL  = v_prm( 7)*p_GCaL    !.  7
  param%p_GbNa  = v_prm( 8)*p_GbNa    !.  8
  param%p_GbCa  = v_prm( 9)*p_GbCa    !.  9
  param%p_GpCa  = v_prm(10)*p_GpCa    !. 10
  param%p_GpK   = v_prm(11)*p_GpK     !. 11
  param%p_PNaK  = v_prm(12)*p_PNaK    !. 12
  param%p_kNaCa = v_prm(13)*p_kNaCa   !. 13
  param%p_Ko    = v_prm(14)*p_Ko      !. 14
  param%p_Nao   = v_prm(15)*p_Nao     !. 15
  param%p_Cao   = v_prm(16)*p_Cao     !. 16
  param%p_Gsus  = v_prm(17)*p_Gsus    !. 17
  param%p_GfK   = v_prm(18)*p_GfK     !. 18
  param%p_GfNa  = v_prm(19)*p_GfNa    !. 19
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_PKFstewart(ict) result(pkf_ic)
!------------------------------------------------------------------------------
! This function sets the initial conditions for the Stewart model for Purkinje
! Fibers pkf_m is the main structure which contains the membrane potential, 
! ioninc concentrations, and gate variables
!------------------------------------------------------------------------------
  implicit none
!
 !.
  integer (ip), intent(in)  :: ict
  real(rp)                  :: pkf_ic(nvar_pkf)
  
  select case (ict)
  case(ST_PK_mod)
    pkf_ic = (/ Cai_i, CaSR_i, CaSS_i, Nai_i, Ki_i, m_i, h_i, j_i, xs_i, r_i, &
                s_i, d_i, f_i, f2_i, fcass_i, rr_i, oo_i, xr1_i, xr2_i, y_i/)
  case default
    pkf_ic = (/ Cai_i, CaSR_i, CaSS_i, Nai_i, Ki_i, m_i, h_i, j_i, xs_i, r_i, &
                s_i, d_i, f_i, f2_i, fcass_i, rr_i, oo_i, xr1_i, xr2_i, y_i/)
  end select
  !.

  return
end function ic_PKFstewart
!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_pkf), intent(in) :: str_me
  real(rp), intent(out)    :: v_me(nvar_pkf)
   
  v_me( 1) = str_me%Cai
  v_me( 2) = str_me%CaSR
  v_me( 3) = str_me%CaSS
  v_me( 4) = str_me%Nai
  v_me( 5) = str_me%Ki
  v_me( 6) = str_me%m
  v_me( 7) = str_me%h
  v_me( 8) = str_me%j
  v_me( 9) = str_me%xs
  v_me(10) = str_me%r
  v_me(11) = str_me%s
  v_me(12) = str_me%d
  v_me(13) = str_me%f
  v_me(14) = str_me%f2
  v_me(15) = str_me%fcass
  v_me(16) = str_me%rr
  v_me(17) = str_me%oo
  v_me(18) = str_me%xr1
  v_me(19) = str_me%xr2
  v_me(20) = str_me%y
  return
end subroutine get_me_struct
!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)      :: v_me(:)
  type (t_pkf), intent(out) :: str_me
  
  str_me%Cai   = v_me(1)
  str_me%CaSR  = v_me(2)
  str_me%CaSS  = v_me(3)
  str_me%Nai   = v_me(4)
  str_me%Ki    = v_me(5)
  str_me%m     = v_me(6)
  str_me%h     = v_me(7)
  str_me%j     = v_me(8)
  str_me%xs    = v_me(9)
  str_me%r     = v_me(10)
  str_me%s     = v_me(11)
  str_me%d     = v_me(12)
  str_me%f     = v_me(13)
  str_me%f2    = v_me(14)
  str_me%fcass = v_me(15)
  str_me%rr    = v_me(16)
  str_me%oo    = v_me(17)
  str_me%xr1   = v_me(18)
  str_me%xr2   = v_me(19)
  str_me%y     = v_me(20)
  return
end subroutine put_me_struct
!-------------------------------------------------------------------------------
subroutine concentrations (dt, Istim, cur, prm, pkf)
!-------------------------------------------------------------------------------
! Function to update Ion concentrations
!-------------------------------------------------------------------------------
  implicit none
  real(rp),     intent(in)      :: dt, Istim
  type (t_cur), intent(in)      :: cur
  type (t_prm), intent(in)      :: prm
  type (t_pkf),   intent(inout) :: pkf
  !.
  real(rp)                      :: kCaSR , k1, k2, VcF_Cap,   &
                                   dRR, Irel, Ileak, Iup, Ixfer, dCaSR, &
                                   CaSSBuf, dCaSS, CaBuf, dCai,   &
                                   totINa, dNai, totIK, dKi
  real(rp)                      :: CaCSQN, bjsr, cjsr, bcss, ccss,      &
                                   bc, cc


  VcF_Cap = p_iVcF*p_Cap
  !.*
  kCaSR   = p_maxsr-((p_maxsr-p_minsr)/(1.0+(p_EC/pkf%CaSR)**2));
  k1      = p_k1p/kCaSR;
  k2      = p_k2p*kCaSR;
  dRR     = p_k4*(1.0 - pkf%rr) - k2*pkf%CaSS*pkf%rr;
  pkf%rr  = pkf%rr + dt*dRR;
  pkf%oo  = k1*pkf%CaSS*pkf%CaSS*pkf%rr/(p_k3 + k1*pkf%CaSS*pkf%CaSS);
  !.*
  Irel    = p_Vrel*pkf%oo*(pkf%CaSR-pkf%CaSS);
  Ileak   = p_Vleak*(pkf%CaSR-pkf%Cai);
  Iup     = p_Vmxu/(1.0+((p_Kup*p_Kup)/ (pkf%Cai*pkf%Cai)));
  Ixfer   = p_Vxfer*(pkf%CaSS - pkf%Cai);
  !.*
  CaCSQN  = p_Bufsr*pkf%CaSR/(pkf%CaSR + p_Kbufsr);
  dCaSR   = dt*(Iup- Irel- Ileak);
  bjsr    = p_Bufsr - CaCSQN - dCaSR - pkf%CaSR + p_Kbufsr;
  cjsr    = p_Kbufsr*(CaCSQN + dCaSR + pkf%CaSR);
  pkf%CaSR  = 0.5*(sqrt(bjsr*bjsr+4.0*cjsr) - bjsr);
  !dCaSR = dt/(1.0+p_Bufsr*p_Kbufsr/(pkf%CaSR+p_Kbufsr)**2);
  !pkf%CaSR = pkf%CaSR + dCaSR*(Iup-Irel-Ileak);
  !.*
  CaSSBuf = p_Bufss*pkf%CaSS/(pkf%CaSS + p_Kbufss);
  dCaSS   = dt*(-Ixfer*(p_Vc/p_Vss) + Irel*(p_Vsr/p_Vss) + (-cur%ICaL*p_iVssF2*p_Cap));
  bcss    = p_Bufss - CaSSBuf-dCaSS-pkf%CaSS+p_Kbufss;
  ccss    = p_Kbufss*(CaSSBuf+dCaSS+pkf%CaSS);
  pkf%CaSS  = 0.5*(sqrt(bcss*bcss+4.0*ccss)-bcss);
  !CaSSBuf = 1.0/(1.0+p_Bufss*p_Kbufss/(pkf%CaSS+p_Kbufss)**2);
  !dCaSS = dt*(-Ixfer*(p_Vc/p_Vss)+Irel*(p_Vsr/p_Vss)+(-cur%ICaL*p_iVssF2*p_Cap));
  !pkf%CaSS = pkf%CaSS + CaSSBuf*dCaSS;
  !.*
  CaBuf   = p_Bufc*pkf%Cai/(pkf%Cai+p_Kbufc);
  dCai    = dt*((-(cur%IbCa+cur%IpCa-2.0*cur%INaCa)*p_iVcF2*p_Cap)-(Iup-Ileak)*p_Vsr/p_Vc+Ixfer);
  bc      = p_Bufc-CaBuf-dCai-pkf%Cai+p_Kbufc;
  cc      = p_Kbufc*(CaBuf+dCai+pkf%Cai);
  pkf%Cai   = 0.5*(sqrt(bc*bc+4.0*cc)-bc);
  !CaBuf= 1.0/(1.0+p_Bufc*p_Kbufc/(pkf%Cai+p_Kbufc)**2);
  !dCai= dt*(-(cur%IbCa+cur%IpCa-2.0*cur%INaCa)*p_iVcF2*p_Cap-(Iup-Ileak)*p_Vsr/p_Vc+Ixfer);
  !pkf%Cai = pkf%Cai + CaBuf*dCai;
  !.*
  totINa  =cur%INa+cur%IbNa+3.0*(cur%INaK+cur%INaCa)+cur%IfNa;
  dNai    = -totINa*p_iVcF*p_Cap;
  pkf%Nai = pkf%Nai+dt*dNai;
  !.*
  totIK   = Istim+cur%IK1+cur%Ito+cur%IKr+cur%IKs+cur%IfK+cur%Isus-2.0*cur%INaK+cur%IpK;
  dKi     = -totIK*p_iVcF*p_Cap;
  pkf%Ki  =  pkf%Ki+dt*dKi;
  !.
  return
end subroutine concentrations
!------------------------------------------------------------------------------
subroutine gates(dt, U, pkf)
!------------------------------------------------------------------------------
! This function updates the gating variables
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)    :: dt, U
  type(t_pkf), intent(inout) :: pkf
  !.
  real (rp)                  :: AM, BM, TAUM, c1, MINF, AH1, BH1, TAUH, HINF,   &
                                AJ1, BJ1, TAUJ, AJ2, BJ2, JINF, &
                                XsINF, Axs, Bxs, TAUXs, RINF, TAUR, SINF,  &
                                TAUS, DINF, Ad, Bd, Cd, TAUD, FINF, Af, Bf, Cf, &
                                TAUF, F2INF, Af2, Bf2, Cf2, TAUF2, denCaSS,     &
                                FCaSSINF, TAUFCaSS, AH2, BH2, AY, BY, YINF,TAUY
  real (rp)                  :: Xr1INF, axr1, bxr1, TAUXr1, Xr2INF, axr2, bxr2,&
                                TAUXr2
  !.
  !.--- Fast Na+ current
  AM    = 1.0/(1.0 + exp(0.2*(-60.0 - U)));
  BM    = 0.1/(1.0 + exp(0.2*( U + 35.0))) + 0.1/(1.0 + exp(0.005*(U - 50.0)));
  TAUM  = AM*BM;
  c1    = 1.0/(1.0 + exp((-56.86 - U)/9.03));
  MINF  = c1*c1;
  if (U >= -40.0) then
     AH1  = 0.0_rp;
     BH1  = 0.77/(0.13*(1.0 + exp(-(U+10.66)/11.1)));
     TAUH = 1.0/(AH1 + BH1);
  else
     AH2   = 0.057*exp(-(U+80.0)/6.8);
     BH2   = 2.7*exp(0.079*U) + 3.1E5*exp(0.3485*U);
     TAUH  = 1.0/(AH2 + BH2);
  endif
  c1   = 1.0/(1.0+exp((U + 71.55)/7.43));
  HINF = c1*c1;
  if (U >= -40.0) then
     AJ1  = 0.0_rp;
     BJ1  = 0.6*exp(0.057*U)/(1.0 + exp(-0.1*(U+32.0)));
     TAUJ = 1.0/(AJ1 + BJ1);
  else
     c1   = -2.5428E4*exp(0.2444*U) - 6.948e-6*exp(-0.04391*U);
     AJ2  = c1*(U + 37.78)/(1. + exp(0.311*(U+79.23)));
     BJ2  = 0.02424*exp(-0.01052*U)/(1.+exp(-0.1378*(U + 40.14)));
     TAUJ = 1.0/(AJ2 + BJ2);
  end if
  JINF  = HINF;
  !
  ! Rapid delay rectifier IKr,
  !
   Xr1INF=1.0/(1.0+exp((-26.0-U)/7.0));
   axr1=450.0/(1.0+exp(0.1*(-45.0-U)));
   bxr1=6.0/(1.0+exp((U-(-30.0))/11.5));
   TAUXr1=axr1*bxr1;
   Xr2INF=1.0/(1.0+exp((U+88.0)/24.0));
   axr2=3.0/(1.0+exp(0.05*(-60.0-U)));
   bxr2=1.12/(1.0+exp(0.05*(U-60.0)));
   TAUXr2=axr2*bxr2;
  !
  ! Slow delay rectifier IKs
  !
  XsINF  = 1.0/(1.0+exp((-5.0-U)/14.0));
  Axs    = (1400.0/(sqrt(1.0+exp((5.0-U)/6.0))));
  Bxs    = (1.0/(1.0+exp((U-35.0)/15.0)));
  TAUXs  = Axs*Bxs+80.0;
  !
  ! Transcient Outward current Ito
  !
  RINF = 1.0/(1.0+exp((20.0-U)/13.0));
  TAUR = 10.45*exp(-(U+40.0)**2.0/1800.0)+7.3;
  !.
  SINF = 1.0/(1.0+exp((U+27.0)/13.0));
  TAUS = 85.0*exp(-0.003125*(U+25.0)**2.0)+5.0/(1.0+exp(0.2*(U-40.0)))+42.0;
  !
  ! L-Type Ca2+ current ICaL
  !
  DINF = 1.0/(1.0+exp((-8.0-U)/7.5));      
  Ad   = 1.4/(1.0+exp((-35.0-U)/13.0))+0.25;
  Bd   = 1.4/(1.0+exp(0.2*(U+5.0)));
  Cd   = 1.0/(1.0+exp(0.05*(50.0-U)));
  TAUD = Ad*Bd+Cd;
  !.
  FINF = 1.0/(1.0+exp((U+20.0)/7.0));
  Af   = 1102.5*exp(-(U+27.0)**2.0/225.0);
  Bf   = 200.0/(1.0+exp(0.1*(13.0-U)));
  Cf   = 180.0/(1.0+exp(0.1*(U+30.0)))+20.0;
  TAUF=Af+Bf+Cf;                          
  !.
  F2INF = 0.67/(1.0+exp((U+35.0)/7.0))+0.33; 
  Af2   =562.0*exp(-(U+27.0)**2.0/240.0);   
  Bf2   = 31.0/(1.0+exp(0.1*(25.0-U)));    
  Cf2   = 80.0/(1.0+exp(0.1*(U+30.0)));   
  TAUF2 = Af2+Bf2+Cf2;                      
  !.
  denCaSS  = 1.0/(1.0+400.0*pkf%CaSS*pkf%CaSS);
  FCaSSINF = 0.6*denCaSS+0.4;              
  TAUFCaSS = 80.0*denCaSS+2.0;
  !.
  YINF = 1.0/(1.0+exp((U+80.6)/6.8));
  AY = exp(-2.9 -0.04*U);
  BY = exp(3.6+0.11*U);
  TAUY = 4000.0/(AY+BY);
  ! Update gates
  pkf%m     = MINF-(MINF-pkf%m)*exp(-dt/TAUM);
  pkf%h     = HINF-(HINF-pkf%h)*exp(-dt/TAUH);
  pkf%j     = JINF-(JINF-pkf%j)*exp(-dt/TAUJ);
  pkf%xr1   = Xr1INF-(Xr1INF-pkf%xr1)*exp(-dt/TAUXr1);
  pkf%xr2   = Xr2INF-(Xr2INF-pkf%xr2)*exp(-dt/TAUXr2);
  pkf%xs    = XsINF-(XsINF-pkf%xs)*exp(-dt/TAUXs);
  pkf%s     = SINF-(SINF-pkf%s)*exp(-dt/TAUS);
  pkf%r     = RINF-(RINF-pkf%r)*exp(-dt/TAUR);
  pkf%d     = DINF-(DINF-pkf%d)*exp(-dt/TAUD);
  pkf%f     = FINF-(FINF-pkf%f)*exp(-dt/TAUF);
  pkf%f2    = F2INF-(F2INF-pkf%f2)*exp(-dt/TAUF2);
  pkf%fcass = FCaSSINF-(FCaSSINF-pkf%fcass)*exp(-dt/TAUFCaSS);
  pkf%y     = YINF-(YINF-pkf%y)*exp(-dt/TAUY);
  return
end subroutine gates
!------------------------------------------------------------------------------
subroutine currents ( U, pkf, Iion, prm, cur) 
!------------------------------------------------------------------------------
! This function computes currents 
!------------------------------------------------------------------------------
  implicit none
  real(rp),    intent(in)   :: U
  type(t_pkf), intent(in)   :: pkf
  real(rp),    intent(out)  :: Iion
  type (t_prm), intent(in)  :: prm
  type(t_cur), intent(out)  :: cur
  !.
  real(rp)                  :: EK, ENa, EKs,ECa, atmp, rec_iNaK, rec_ipK, atmp1,&
                               gam0, KhM, fM, KhN,   &
                               fN, fT, fATP, denICaL, ICaL, atmp2, denINaCa1,   &
                               denINaK,IK,INa,ICa, Km, H

  !.
  EK      = p_RTF*log(prm%p_Ko/pkf%Ki);   !*
  ENa     = p_RTF*log(prm%p_Nao/pkf%Nai); !*
  EKs     = p_RTF*log((prm%p_Ko+prm%p_pKNa*prm%p_Nao)/(pkf%Ki+prm%p_pKNa*pkf%Nai)); !*
  ECa     = 0.5*p_RTF*log(prm%p_Cao/pkf%Cai); !*
  atmp    = U*p_iRTF;!*
  rec_iNaK= 1.0/(1.0+0.1245*exp(-0.1*atmp)+0.0353*exp(-atmp)); !*
  rec_ipK = 1.0/(1.0+exp((25.0-U)/5.98));  !*
  atmp1   = U-EK; !*
  !.--- Ito current*
  cur%Ito = prm%p_Gto*pkf%r*pkf%s*atmp1;
  !.--- IKr current*
  cur%IKr=prm%p_GKr*sqrt(prm%p_Ko/5.4)*pkf%xr1*pkf%xr2*atmp1;
  !.--- IK1 current*
  cur%IK1=prm%p_GK1*(atmp1-8.0)/(1.0+exp(0.1*(U+75.44)));   
  !.--- IpK current*
  cur%IpK = prm%p_GpK*rec_ipK*atmp1;
  !.--- Isus current*
  cur%Isus = prm%p_Gsus*atmp1/(1.0+exp((5.0-U)/17.0));
  !.--- IfK current*
  cur%IfK = prm%p_GfK*pkf%y*atmp1 
  !.--- IfNa current*
  atmp1     = U-ENa;
  cur%IfNa = prm%p_GfNa*pkf%y*atmp1 
  !.--- INa current*
  cur%INa   = prm%p_GNa*(pkf%m*pkf%m*pkf%m)*pkf%h*pkf%j*atmp1;
  !.--- IbNa current*
  cur%IbNa  = prm%p_GbNa*atmp1;
  !.--- ICaL current*
  atmp1     = 2.0*(U - 15.0)*p_iRTF;
  denICaL   = exp(atmp1)-1.0;
  ICaL      = 2.0*prm%p_GCaL*pkf%d*pkf%f*pkf%f2*pkf%fcass*(atmp1*p_F)*(0.25*pkf%CaSS*exp(atmp1)-prm%p_Cao);
  cur%ICaL  = ICaL/denICaL;
  !.--- IKs current*
  cur%IKs   = prm%p_GKs*pkf%xs*pkf%xs*(U-EKs);
  !.--- INaCa current*
  atmp1     = exp(p_gam*atmp);
  atmp2     = exp((p_gam-1.0)*atmp);
  denINaCa1 = (p_KmNai**3.0+prm%p_Nao**3.0)*(p_KmCa+prm%p_Cao)*(1.0+p_ksat*atmp2);
  cur%INaCa = prm%p_kNaCa*(atmp1*pkf%Nai**3.0*prm%p_Cao-atmp2*prm%p_Nao**3.0*pkf%Cai*p_alf)/denINaCa1;
  !.--- INaK current*
  denINaK   = (prm%p_Ko+p_KmK)*(pkf%Nai+p_KmNa);
  cur%INaK  = prm%p_PNaK*prm%p_Ko*pkf%Nai*rec_iNaK/denINaK;
  !.--- IpCa current*
  cur%IpCa  = prm%p_GpCa*pkf%Cai/(p_KpCa+pkf%Cai);
  !.--- IbCa current*
  cur%IbCa  = prm%p_GbCa*(U-ECa);
  !.--- Calculating total current
  IK   = cur%IKr  + cur%IKs  + cur%IK1  + cur%IpK   + cur%Ito + cur%IfK + cur%Isus;
  INa  = cur%INa  + cur%IbNa + cur%INaK + cur%INaCa + cur%IfNa;
  ICa  = cur%ICaL + cur%IbCa + cur%IpCa;
  !.
  Iion = IK + INa + ICa;
  !.
  return
end subroutine currents
!-------------------------------------------------------------------------------
subroutine PKFstewart_A_P01 (ict,dt,Istm,U,Iion,v_prm,v_pk,v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict   !.Cell model
  real(rp),    intent(in)    :: dt, Istm, U
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(in)    :: v_prm(:)
  real(rp),    intent(inout) :: v_pk(nvar_pkf)
  real(rp),    intent(out)   :: v_cr(ncur_pkf)   
  !.
  type(t_pkf)                :: pkf
  type (t_prm)               :: param
  type(t_cur)                :: crr
  !
  call put_me_struct(v_pk,pkf)
  call put_param(v_prm,param)
  !
  call currents ( U, pkf, Iion, param, crr) 
  call concentrations ( dt, Istm, crr, param, pkf)
  call gates( dt, U, pkf)
  !  
  call get_me_struct(pkf,v_pk)
  !.
  v_cr(1:ncur_pkf) = (/ crr%IKr, crr%IKs,  crr%IK1,  crr%IpK, crr%Ito, crr%Isus, & 
                  crr%IfK, crr%INa, crr%IbNa, crr%INaK, crr%IfNa,crr%INaCa,&
                  crr%ICaL, crr%IbCa, crr%IpCa, Iion+Istm /)
  !.
  return
end subroutine PKFstewart_A_P01
!-------------------------------------------------------------------------------
end module mod_PKFstewart
!-------------------------------------------------------------------------------
