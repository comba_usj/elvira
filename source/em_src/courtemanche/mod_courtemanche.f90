! This module implements the Courtemanche Model  (M Courtemanche, RJ Ramirez, S Nattel; Am J Physiol 275:H301-H321, 1998)
!
! Implemented by Laura Martínez Mateu (laumarma@gbio.i3bh.es)  20/01/2014
!
!------------------------------------------------------------------------------
module mod_courtemanche
!------------------------------------------------------------------------------
  use mod_precision
  implicit none   ! Para que Fortran me obligue a declarar las variables
  include 'courtemanche_parameters.inc'
  !.
  type t_courte
    sequence
    real(rp) ::   u, v, w, d, h, xr, Nai, Ki, Carel, oi, ui, m, j, f, xs, oa, &
                  ua, fCa, Cai, Caup
  end type t_courte
  
  type t_cur
    sequence
    real(rp) ::   INa,IK1,Ito,IKur,IKr,IKs,ICaL,INaK,INaCa,IbCa,IbNa,IpCa,Irel,&
                  Itr,Iup,Ileak
  end type t_cur
  
  !.
  public  :: get_parameter_CRN , ic_Courte, Courtemanche_A_P01
  
  private :: put_param, f_currents, f_gates, f_concentrations
  !.
  type, public:: t_prm
    private
    real(rp)  :: p_gNa    ! 1	
    real(rp)  :: p_gCaL   ! 2  
    real(rp)  :: p_gto    ! 3
    real(rp)  :: p_gKur   ! 4
    real(rp)  :: p_gKr    ! 5 
    real(rp)  :: p_gKs    ! 6 
    real(rp)  :: p_gK1    ! 7 
    real(rp)  :: p_gbCa   ! 8
    real(rp)  :: p_gbNa   ! 9 
    !real(rp)  :: p_ACh    ! 13
  end type t_prm
  
!------------------------------------------------------------------------------!
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  !
!------------------------------------------------------------------------------!
contains
!------------------------------------------------------------------------------!
subroutine write_state_Courte(lu_st)
implicit none
integer(ip), intent(in)   :: lu_st

    write(lu_st,'(A)',advance='no') '% '
    write(lu_st,'(A)',advance='no') 'u '
    write(lu_st,'(A)',advance='no') 'v '
    write(lu_st,'(A)',advance='no') 'w '
    write(lu_st,'(A)',advance='no') 'd '
    write(lu_st,'(A)',advance='no') 'h '
    write(lu_st,'(A)',advance='no') 'xr '
    write(lu_st,'(A)',advance='no') 'Nai '
    write(lu_st,'(A)',advance='no') 'Ki '
    write(lu_st,'(A)',advance='no') 'Carel '
    write(lu_st,'(A)',advance='no') 'oi '
    write(lu_st,'(A)',advance='no') 'ui '
    write(lu_st,'(A)',advance='no') 'm '
    write(lu_st,'(A)',advance='no') 'j '
    write(lu_st,'(A)',advance='no') 'f '
    write(lu_st,'(A)',advance='no') 'xs '
    write(lu_st,'(A)',advance='no') 'oa '
    write(lu_st,'(A)',advance='no') 'ua '
    write(lu_st,'(A)',advance='no') 'fCa '
    write(lu_st,'(A)',advance='no') 'Cai '
    write(lu_st,'(A)',advance='no') 'Caup '
    
end subroutine write_state_Courte
!------------------------------------------------------------------------------!
subroutine write_current_Courte(lu_cr)
implicit none
integer(ip), intent(in)   :: lu_cr

    write(lu_cr,'(A)',advance='no') '% '
    write(lu_cr,'(A)',advance='no') 'INa '
    write(lu_cr,'(A)',advance='no') 'IK1 '
    write(lu_cr,'(A)',advance='no') 'Ito '
    write(lu_cr,'(A)',advance='no') 'IKur '
    write(lu_cr,'(A)',advance='no') 'IKr '
    write(lu_cr,'(A)',advance='no') 'IKs '
    write(lu_cr,'(A)',advance='no') 'ICaL '
    write(lu_cr,'(A)',advance='no') 'INaK '
    write(lu_cr,'(A)',advance='no') 'INaCa '
    write(lu_cr,'(A)',advance='no') 'IbCa '
    write(lu_cr,'(A)',advance='no') 'IbNa '
    write(lu_cr,'(A)',advance='no') 'IpCa '
    write(lu_cr,'(A)',advance='no') 'Irel '
    write(lu_cr,'(A)',advance='no') 'Itr '
    write(lu_cr,'(A)',advance='no') 'Iup '
    write(lu_cr,'(A)',advance='no') 'Ileak '
    write(lu_cr,'(A)',advance='no') 'Iion '

end subroutine write_current_Courte
!------------------------------------------------------------------------------!
function get_parameter_CRN (tcell) result (v_prm)
!------------------------------------------------------------------------------!
  implicit none
  integer(ip), intent(in) :: tcell
  real (rp)               :: v_prm(np_courte)

  select case (tcell)
  case (Courte_mod)
    v_prm = (/ p_gNa, p_gCaL, p_gto, p_gKur, p_gKr, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_RA_PM)
    v_prm = (/ p_gNa, p_gCaL, p_gto, p_gKur, p_gKr, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_CT_BB)
    v_prm = (/ p_gNa, p_gCaL_CT_BB, p_gto, p_gKur, p_gKr, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_TVR)
    v_prm = (/ p_gNa, p_gCaL_TVR, p_gto, p_gKur, p_gKr_TVR, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_RAA)
    v_prm = (/ p_gNa, p_gCaL, p_gto_RAA, p_gKur, p_gKr_RAA, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_LA)
    v_prm = (/ p_gNa, p_gCaL, p_gto, p_gKur, p_gKr_LA, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_PV)
    v_prm = (/ p_gNa, p_gCaL, p_gto, p_gKur, p_gKr_PV, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case (Courte_mod_LAA)
    v_prm = (/ p_gNa, p_gCaL, p_gto_LAA, p_gKur, p_gKr_LAA, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)   
  case (Courte_mod_MVR)
    v_prm = (/ p_gNa, p_gCaL_MVR, p_gto, p_gKur, p_gKr_MVR, p_gKs, p_gK1, &
               p_gbCa, p_gbNa/)
  case default
    write (*,10) tcell; stop
  end select
  !.
  return
  10 format ('###.Specified Cell type is not defined:',I3)
end function get_parameter_CRN
!------------------------------------------------------------------------------
subroutine put_param(v_prm,param)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)    :: v_prm(:)
  type (t_prm)            :: param
  param%p_gNa    = v_prm(1)  ! 1	
  param%p_gCaL   = v_prm(2)  ! 2  
  param%p_gto    = v_prm(3)  ! 3
  param%p_gKur   = v_prm(4)  ! 4
  param%p_gKr    = v_prm(5)  ! 5 
  param%p_gKs    = v_prm(6)  ! 6 
  param%p_gK1    = v_prm(7)  ! 7 
  param%p_gbCa   = v_prm(8)  ! 8
  param%p_gbNa   = v_prm(9)  ! 9 
  
  !param%p_ACh    = v_prm(13) ! 13 
  return
end subroutine put_param
!------------------------------------------------------------------------------
function ic_Courte(ict) result(courte_ic)
!------------------------------------------------------------------------------------
! This function sets the initial conditions described in courtemanche_parameters.inc 
!------------------------------------------------------------------------------------
  implicit none
  !.
  integer (ip), intent(in)  :: ict
  real(rp)                  :: courte_ic(nvar_courte)
  
  select case (ict)
  case (Courte_mod)
    courte_ic = (/ uic, vic, wic, dic, hic, xric, Naiic, Kiic, Carelic, oiic, uiic, &
                   mic, jic, fic, xsic, oaic, uaic, fCaic, Caiic, Caupic/)!
  case(Courte_mod_RA_PM) !.----ATRIA
    courte_ic = (/ uic_RA_PM, vic_RA_PM, wic_RA_PM, dic_RA_PM, hic_RA_PM, xric_RA_PM, &
                   Naiic_RA_PM, Kiic_RA_PM, Carelic_RA_PM, oiic_RA_PM, uiic_RA_PM,    &
                   mic_RA_PM, jic_RA_PM, fic_RA_PM, xsic_RA_PM, oaic_RA_PM,           &
                   uaic_RA_PM, fCaic_RA_PM, Caiic_RA_PM, Caupic_RA_PM/)!
  case(Courte_mod_CT_BB) !.----ATRIA
    courte_ic = (/ uic_CT_BB, vic_CT_BB, wic_CT_BB, dic_CT_BB, hic_CT_BB, xric_CT_BB, &
                   Naiic_CT_BB, Kiic_CT_BB, Carelic_CT_BB, oiic_CT_BB, uiic_CT_BB,    &
                   mic_CT_BB, jic_CT_BB, fic_CT_BB, xsic_CT_BB, oaic_CT_BB,           &
                   uaic_CT_BB, fCaic_CT_BB, Caiic_CT_BB, Caupic_CT_BB/)!
  case(Courte_mod_TVR) !.----ATRIA
    courte_ic = (/ uic_TVR, vic_TVR, wic_TVR, dic_TVR, hic_TVR, xric_TVR, Naiic_TVR,  &
                   Kiic_TVR, Carelic_TVR, oiic_TVR, uiic_TVR, mic_TVR, jic_TVR,       &
                   fic_TVR, xsic_TVR, oaic_TVR, uaic_TVR, fCaic_TVR, Caiic_TVR,       &
                   Caupic_TVR/)!
  case(Courte_mod_RAA) !.----ATRIA
    courte_ic = (/ uic_RAA, vic_RAA, wic_RAA, dic_RAA, hic_RAA, xric_RAA, Naiic_RAA,  &
                   Kiic_RAA, Carelic_RAA, oiic_RAA, uiic_RAA, mic_RAA, jic_RAA,       &
                   fic_RAA, xsic_RAA, oaic_RAA, uaic_RAA, fCaic_RAA, Caiic_RAA,       &
                   Caupic_RAA/)!
  case(Courte_mod_LA) !.----ATRIA
    courte_ic = (/ uic_LA, vic_LA, wic_LA, dic_LA, hic_LA, xric_LA, Naiic_LA, Kiic_LA,&
                   Carelic_LA, oiic_LA, uiic_LA, mic_LA, jic_LA, fic_LA, xsic_LA,     &
                   oaic_LA, uaic_LA, fCaic_LA, Caiic_LA, Caupic_LA/)!
  case(Courte_mod_PV) !.----ATRIA
    courte_ic = (/ uic_PV, vic_PV, wic_PV, dic_PV, hic_PV, xric_PV, Naiic_PV, Kiic_PV,&
                   Carelic_PV, oiic_PV, uiic_PV, mic_PV, jic_PV, fic_PV, xsic_PV,     &
                   oaic_PV, uaic_PV, fCaic_PV, Caiic_PV, Caupic_PV/)!
  case(Courte_mod_LAA) !.----ATRIA
    courte_ic = (/ uic_LAA, vic_LAA, wic_LAA, dic_LAA, hic_LAA, xric_LAA, Naiic_LAA,  &
                   Kiic_LAA, Carelic_LAA, oiic_LAA, uiic_LAA, mic_LAA, jic_LAA,       &
                   fic_LAA, xsic_LAA, oaic_LAA, uaic_LAA, fCaic_LAA, Caiic_LAA,       &
                   Caupic_LAA/)!
  case(Courte_mod_MVR) !.----ATRIA
    courte_ic = (/ uic_MVR, vic_MVR, wic_MVR, dic_MVR, hic_MVR, xric_MVR, Naiic_MVR,  &
                   Kiic_MVR, Carelic_MVR, oiic_MVR, uiic_MVR, mic_MVR, jic_MVR,       &
                   fic_MVR, xsic_MVR, oaic_MVR, uaic_MVR, fCaic_MVR, Caiic_MVR,       &
                   Caupic_MVR/)!
  case default
    courte_ic = (/ uic, vic, wic, dic, hic, xric, Naiic, Kiic, Carelic, oiic, uiic,   &
                   mic, jic, fic, xsic, oaic, uaic, fCaic, Caiic, Caupic/)!
  end select
  !.
 
  return
end function ic_Courte

!------------------------------------------------------------------------------
subroutine get_me_struct(str_me,v_me)
!------------------------------------------------------------------------------
  implicit none
  type (t_courte), intent(in) :: str_me
  real(rp), intent(out)       :: v_me(nvar_courte)
  
  v_me(1)  =   str_me%u 
  v_me(2)  =   str_me%v 
  v_me(3)  =   str_me%w
  v_me(4)  =   str_me%d
  v_me(5)  =   str_me%h
  v_me(6)  =   str_me%xr
  v_me(7)  =   str_me%Nai
  v_me(8)  =   str_me%Ki
  v_me(9)  =   str_me%Carel
  v_me(10) =   str_me%oi
  v_me(11) =   str_me%ui
  v_me(12) =   str_me%m
  v_me(13) =   str_me%j
  v_me(14) =   str_me%f
  v_me(15) =   str_me%xs
  v_me(16) =   str_me%oa
  v_me(17) =   str_me%ua
  v_me(18) =   str_me%fCa
  v_me(19) =   str_me%Cai
  v_me(20) =   str_me%Caup
  return
    
end subroutine get_me_struct

!------------------------------------------------------------------------------
subroutine put_me_struct(v_me,str_me)
!------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)         :: v_me(:)
  type (t_courte), intent(out) :: str_me
  str_me%u     = v_me(1)
  str_me%v     = v_me(2)
  str_me%w     = v_me(3)
  str_me%d     = v_me(4)
  str_me%h     = v_me(5)
  str_me%xr    = v_me(6)
  str_me%Nai   = v_me(7)
  str_me%Ki    = v_me(8)
  str_me%Carel = v_me(9)
  str_me%oi    = v_me(10)
  str_me%ui    = v_me(11)
  str_me%m     = v_me(12)
  str_me%j     = v_me(13)
  str_me%f     = v_me(14)
  str_me%xs    = v_me(15)
  str_me%oa    = v_me(16)
  str_me%ua    = v_me(17)
  str_me%fCa   = v_me(18)
  str_me%Cai   = v_me(19)
  str_me%Caup  = v_me(20)
  return
end subroutine put_me_struct
! ======================================================================================
! ------------------------------------------------------------------------------------- 
! ===================================================================================== 

subroutine f_currents(Vm,prm,cur,cm_m,Iion)
! ===================================================================================== 
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: Vm
  real(rp),intent(out)   :: Iion
  type (t_prm)           :: prm
  type(t_courte)         :: cm_m
  type (t_cur)           :: cur
  real(rp)               :: ECa,ENa,EK,sigma,fNaK,tau_tr
  real(rp)               :: den, num

  ECa = (p_R*p_T/(2.0*p_F))*log(p_Cao/cm_m%Cai);
  ENa = (p_R*p_T/p_F)*log(p_Nao/cm_m%Nai);
  EK = (p_R*p_T/p_F)*log(p_Ko/cm_m%Ki);

  ! L type Ca channel
  
!  cur%ICaL = p_Cm*prm%p_gCaL*cm_m%d*cm_m%f*cm_m%fCa*(Vm-65.0);
  cur%ICaL = prm%p_gCaL*cm_m%d*cm_m%f*cm_m%fCa*(Vm-65.0);
  
  ! NaCa exchanger current
  
  den = (((p_KmNa**3)+(p_Nao**3))*(p_KmCa+p_Cao)*(1.0+p_ksat*exp(((p_gamma-1.0)*p_F*Vm)/(p_R*p_T))));
  num = exp((p_gamma*p_F*Vm)/(p_R*p_T))*(cm_m%Nai**3)*p_Cao 
  num = num - exp(((p_gamma-1.0)*p_F*Vm)/(p_R*p_T))*(p_Nao**3)*cm_m%Cai
!  cur%INaCa = p_Cm*p_INaCamax*num/den
  cur%INaCa = p_INaCamax*num/den
  
  ! Background currents
  
!  cur%IbNa = p_Cm*prm%p_gbNa*(Vm-ENa);
!  cur%IbCa = p_Cm*prm%p_gbCa*(Vm-ECa);
  cur%IbNa = prm%p_gbNa*(Vm-ENa);
  cur%IbCa = prm%p_gbCa*(Vm-ECa);
  
  ! Fast sodium current
  
!  cur%INa = p_Cm*prm%p_gNa*(cm_m%m**3)*cm_m%h*cm_m%j*(Vm-ENa);
  cur%INa = prm%p_gNa*(cm_m%m**3)*cm_m%h*cm_m%j*(Vm-ENa);
  
  ! Rapid delayed rectifier K current
  
!  cur%IKr = p_Cm*prm%p_gKr*cm_m%xr*(Vm-EK)/(1.0+exp((Vm+15.0)/22.4));
  cur%IKr = prm%p_gKr*cm_m%xr*(Vm-EK)/(1.0+exp((Vm+15.0)/22.4));
  
  ! Sarcolemmal Ca pump current
  
!  cur%IpCa = p_Cm*p_IpCamax*cm_m%Cai/(0.0005+cm_m%Cai);
  cur%IpCa = p_IpCamax*cm_m%Cai/(0.0005+cm_m%Cai);
  
  ! Slow delayed rectifier K current
  
!  cur%IKs = p_Cm*prm%p_gKs*(cm_m%xs**2)*(Vm-EK);
  cur%IKs = prm%p_gKs*(cm_m%xs**2)*(Vm-EK);
  
  ! Sodium potassium pump
  
  sigma = (1.0/7.0)*(exp(p_Nao/67.3)-1.0);
  fNaK = 1.0/(1.0+0.1245*exp((-0.1*p_F*Vm)/(p_R*p_T))+0.0365*sigma*exp((-p_F*Vm)/(p_R*p_T)));
!  cur%INaK = (p_Ko*p_Cm*p_INaKmax*fNaK/(1.0+((p_KmNai/cm_m%Nai)**1.5)))/(p_Ko+p_KmKo);
  cur%INaK = (p_Ko*p_INaKmax*fNaK/(1.0+((p_KmNai/cm_m%Nai)**1.5)))/(p_Ko+p_KmKo);
  
  ! Time independent potassium current
  
!  cur%IK1 = p_Cm*prm%p_gK1*(Vm-EK)/(1.0+exp(0.07*(Vm+80.0)));
  cur%IK1 = prm%p_gK1*(Vm-EK)/(1.0+exp(0.07*(Vm+80.0)));
  
  ! Transfer current from NSR to JSR
  
  tau_tr = 180.0;
  cur%Itr = (cm_m%Caup-cm_m%Carel)/tau_tr;
  
  ! Transient outward K current
  
!  Ito = p_Cm*prm%p_gto*(cm_m%oa**3)*cm_m%oi*(Vm-EK);
  cur%Ito = prm%p_gto*(cm_m%oa**3)*cm_m%oi*(Vm-EK);
  
  ! Ultrarapid delayed rectifier K current
  
  prm%p_gKur = 0.005+0.05/(1.0+exp((Vm-15.0)/(-13.0)));
!  IKur = p_Cm*prm%p_gKur*(cm_m%ua**3)*cm_m%ui*(Vm-EK);
  cur%IKur = prm%p_gKur*(cm_m%ua**3)*cm_m%ui*(Vm-EK);
  
!  Iion = (INa+IK1+Ito+IKur+IKr+IKs+IbNa+IbCa+INaK+IpCa+INaCa+ICaL)/p_Cm;
  Iion = cur%INa+cur%IK1+cur%Ito+cur%IKur+cur%IKr+cur%IKs+ &
         cur%IbNa+ cur%IbCa+ cur%INaK+ cur%IpCa+ cur%INaCa+&
         cur%ICaL
  
  return
end subroutine f_currents
!-----------------------------------------------------------------------------------------------------------------

subroutine f_concentrations(dt,cur,cm_m,Istm)
! ===================================================================================== 
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,Istm
  type(t_courte)         :: cm_m
  type (t_cur)           :: cur
  real(rp)               :: Ca_Cmdn,Ca_Trpn,Ca_Csqn,B1,B2
  
  ! Ca buffers
  
  Ca_Cmdn = (p_CMDNmax*cm_m%Cai)/(cm_m%Cai+p_KmCmdn);
  Ca_Trpn = (p_TRPNmax*cm_m%Cai)/(cm_m%Cai+p_KmTrpn);
  Ca_Csqn = (p_CSQNmax*cm_m%Carel)/(cm_m%Carel+p_KmCsqn);
  
  ! Ca leak current by the NSR
  
  cur%Ileak = p_Iupmax*cm_m%Caup/p_Caupmax;
  
  ! Ca uptake current by the NSR
  
  cur%Iup = p_Iupmax/(1.0+p_Kup/cm_m%Cai);

  ! Ca release current from JSR
  cur%Irel = p_krel*(cm_m%u**2)*cm_m%v*cm_m%w*(cm_m%Carel-cm_m%Cai);

  ! Intracellular ion concentrations
  
  cm_m%Nai = cm_m%Nai + dt*p_Cm*(-3.0*cur%INaK-(3.0*cur%INaCa+cur%IbNa+cur%INa))/(p_Vi*p_F);
  !
  cm_m%Ki = cm_m%Ki + dt*p_Cm*(2.0*cur%INaK-(Istm+cur%IK1+cur%Ito+cur%IKur+cur%IKr+cur%IKs))/(p_Vi*p_F);
  !
  B1 = p_Cm*(2.0*cur%INaCa-(cur%IpCa+cur%ICaL+cur%IbCa))/(2.0*p_Vi*p_F) + (p_Vup*(cur%Ileak-cur%Iup)+cur%Irel*p_Vrel)/p_Vi;
  B2 = 1.0 + (p_TRPNmax*p_KmTrpn)/((cm_m%Cai+p_KmTrpn)**2) + (p_CMDNmax*p_KmCmdn)/((cm_m%Cai+p_KmCmdn)**2);
  cm_m%Cai = cm_m%Cai + dt*B1/B2;
  !
  cm_m%Caup = cm_m%Caup + dt*(cur%Iup-(cur%Ileak+cur%Itr*p_Vrel/p_Vup));
  !
  cm_m%Carel = cm_m%Carel + dt*(cur%Itr-cur%Irel)/(1.0+p_CSQNmax*p_KmCsqn/((cm_m%Carel+p_KmCsqn)**2));  
  return
end subroutine f_concentrations
!-----------------------------------------------------------------------------------------------------------------

subroutine f_gates(dt,Vm,cur,cm_m)
! ===================================================================================== 
! ===================================================================================== 
  implicit none
  real(rp),intent(in)    :: dt,Vm
  type(t_courte)         :: cm_m
  type (t_cur)           :: cur
  real(rp)               :: Fn, tau_u,u_inf,tau_v,v_inf,tau_w,w_inf,d_inf,tau_d,tau_fCa,fCa_inf,&
                            f_inf,tau_f,alfa_h,beta_h,h_inf,tau_h,alfa_j,beta_j,j_inf,tau_j,alfa_m,&
                            beta_m,m_inf,tau_m,alfa_xr,beta_xr,tau_xr,xr_inf,alfa_xs,beta_xs,tau_xs,&
                            xs_inf,alfa_oa,beta_oa,tau_oa,oa_inf,alfa_oi,beta_oi,tau_oi,oi_inf,&
                            alfa_ui,beta_ui,tau_ui,ui_inf,alfa_ua,beta_ua,tau_ua,ua_inf

  
  Fn = 1.0E03*(1E-15*p_Vrel*cur%Irel-p_Cm*((1E-15/(2.0*p_F))*(0.5*cur%ICaL-0.2*cur%INaCa)));
  
  ! Ca release current from JSR u gate
  
  tau_u = 8.0;
  u_inf = 1.0/(1.0+exp(-(Fn-3.4175E-13)/13.67E-16));
  cm_m%u = u_inf-(u_inf-cm_m%u)*exp(-dt/tau_u);
  
  ! Ca release current from JSR v gate
  
  tau_v = 1.91+2.09/(1+exp(-(Fn-3.4175E-13)/13.67E-16));
  v_inf = 1.0-1.0/(1.0+exp(-(Fn-6.835E-14)/13.67E-16));
  cm_m%v = v_inf-(v_inf-cm_m%v)*exp(-dt/tau_v);
  
  ! Ca release current from JSR w gate
  
  if (abs(Vm-7.9)<1.0E-10) then
    tau_w = 6.0*0.2/1.3;
  else
    tau_w = 6.0*(1.0-exp(-(Vm-7.9)/5.0))/((1.0+0.3*exp(-(Vm-7.9)/5.0))*(Vm-7.9));
  end if
  w_inf = 1.0-1.0/(1.0+exp(-(Vm-40.0)/17.0));
  cm_m%w = w_inf-(w_inf-cm_m%w)*exp(-dt/tau_w);
  
  ! L type Ca channel d gate
  
  d_inf = 1.0/(1.0+exp((Vm+10.0)/(-8.0)));
  if (abs(Vm+10.0)<1.0E-10) then
    tau_d = 4.579/(1.0+exp((Vm+10.0)/(-6.24)));
  else
    tau_d = (1.0-exp((Vm+10.0)/(-6.24)))/(0.035*(Vm+10.0)*(1.0+exp((Vm+10.0)/(-6.24))));
  end if
  cm_m%d = d_inf-(d_inf-cm_m%d)*exp(-dt/tau_d);
  
  ! L type Ca channel fCa gate
  
  fCa_inf = 1.0/(1.0+cm_m%Cai/0.00035);
  tau_fCa = 2.0;
  cm_m%fCa = fCa_inf-(fCa_inf-cm_m%fCa)*exp(-dt/tau_fCa);
  
  ! L type Ca channel f gate
  
  f_inf = exp(-(Vm+28.0)/6.9)/(1.0+exp(-(Vm+28.0)/6.9));
  tau_f = 9.0/(0.0197*exp(-(0.0337**2)*((Vm+10.0)**2))+0.02);
  cm_m%f = f_inf-(f_inf-cm_m%f)*exp(-dt/tau_f);
  
  ! Fast sodium current h and j gate
  
  if (Vm<-40.0) then
    alfa_h = 0.135*exp((Vm+80.0)/(-6.8));
    beta_h = 3.56*exp(0.079*Vm)+3.1E05*exp(0.35*Vm);
    alfa_j = (-1.2714E05*exp(0.2444*Vm)-3.474E-05*exp(-0.04391*Vm))*(Vm+37.78)/(1.0+exp(0.311*(Vm+79.23)));
    beta_j = (0.1212*exp(-0.01052*Vm))/(1.0+exp(-0.1378*(Vm+40.14)));
  else
    alfa_h = 0.0;
    beta_h = 1.0/(0.13*(1.0+exp((Vm+10.66)/(-11.1))));
    alfa_j = 0.0;
    beta_j = (0.3*exp(-2.535E-07*Vm))/(1.0+exp(-0.1*(Vm+32.0)));
  end if
  h_inf = alfa_h/(alfa_h+beta_h);
  tau_h = 1.0/(alfa_h+beta_h);
  cm_m%h = h_inf-(h_inf-cm_m%h)*exp(-dt/tau_h);
  j_inf = alfa_j/(alfa_j+beta_j);
  tau_j = 1.0/(alfa_j+beta_j);
  cm_m%j = j_inf-(j_inf-cm_m%j)*exp(-dt/tau_j);
  
  ! Fast sodium current m gate
  
  if (Vm == -47.13) then
    alfa_m = 3.2;
  else 
    alfa_m = 0.32*(Vm+47.13)/(1.0-exp(-0.1*(Vm+47.13)));
  end if
  beta_m = 0.08*exp(-Vm/11.0);
  m_inf = alfa_m/(alfa_m+beta_m);
  tau_m = 1.0/(alfa_m+beta_m); 
  cm_m%m = m_inf-(m_inf-cm_m%m)*exp(-dt/tau_m);  
  
  ! Rapid delayed rectifier K current xr gate
  
  if (abs(Vm+14.1)<1.0E-10) then
    alfa_xr = 0.0015;
  else
    alfa_xr = 0.0003*(Vm+14.1)/(1.0-exp((Vm+14.1)/(-5.0)));
  end if
  if (abs(Vm-3.3328)<1.0E-10) then
    beta_xr = 3.7836118E-04;
  else
    beta_xr = 0.000073898*(Vm-3.3328)/(exp((Vm-3.3328)/5.1237)-1.0);
  end if
  tau_xr = 1.0/(alfa_xr+beta_xr);
  xr_inf = 1.0/(1.0+exp((Vm+14.1)/(-6.5)));
  cm_m%xr = xr_inf-(xr_inf-cm_m%xr)*exp(-dt/tau_xr);  

  ! Slow delayed rectifier K current xs gate
  
  if (abs(Vm-19.9)<1.0E-10) then
    alfa_xs = 0.00068;
    beta_xs = 0.000315;
  else
    alfa_xs = 0.00004*(Vm-19.9)/(1.0-exp((Vm-19.9)/(-17.0)));
    beta_xs = 0.000035*(Vm-19.9)/(exp((Vm-19.9)/9.0)-1.0);
  end if
  tau_xs = 0.5/((alfa_xs+beta_xs));
  xs_inf = 1.0/(sqrt(1.0+exp((Vm-19.9)/(-12.7))));
  cm_m%xs = xs_inf-(xs_inf-cm_m%xs)*exp(-dt/tau_xs);
  
  ! Transient outward K current oa gate
  
  alfa_oa = 0.65/(exp((Vm+10.0)/(-8.5))+exp((Vm-30.0)/(-59.0)));
  beta_oa = 0.65/(2.5+exp((Vm+82.0)/17.0));
  tau_oa = 1.0/((alfa_oa+beta_oa)*p_KQ10);
  oa_inf = 1.0/(1.0+exp((Vm+20.47)/(-17.54)));
  cm_m%oa = oa_inf-(oa_inf-cm_m%oa)*exp(-dt/tau_oa);
  
  ! Transient outward K current oi gate
  
  alfa_oi = 1.0/(18.53+exp((Vm+113.7)/10.95));
  beta_oi = 1.0/(35.56+exp((Vm+1.26)/(-7.44)));
  tau_oi = 1.0/((alfa_oi+beta_oi)*p_KQ10);
  oi_inf = 1.0/(1.0+exp((Vm+43.1)/5.3));
  cm_m%oi = oi_inf-(oi_inf-cm_m%oi)*exp(-dt/tau_oi);
  
  ! Ultrarapid delayed rectifier K current ua gate
  
  alfa_ua = 0.65/(exp((Vm+10.0)/(-8.5))+exp((Vm-30.0)/(-59.0)));
  beta_ua = 0.65/(2.5+exp((Vm+82.0)/17.0));
  tau_ua = 1.0/((alfa_ua+beta_ua)*p_KQ10);
  ua_inf = 1.0/(1.0+exp((Vm+30.3)/(-9.6)));
  cm_m%ua = ua_inf-(ua_inf-cm_m%ua)*exp(-dt/tau_ua);
  
  ! Ultrarapid delayed rectifier K current ui gate
  
  alfa_ui = 1.0/(21.0+exp((Vm-185.0)/(-28.0)));
  beta_ui = 1.0/(exp((Vm-158.0)/(-16.0)));
  tau_ui = 1.0/((alfa_ui+beta_ui)*p_KQ10);
  ui_inf = 1.0/(1.0+exp((Vm-99.45)/(27.48)));
  cm_m%ui = ui_inf-(ui_inf-cm_m%ui)*exp(-dt/tau_ui);
  
  return
end subroutine f_gates
!-----------------------------------------------------------------------------------------------------------------

!=================================================================================
!=================================================================================

!-------------------------------------------------------------------------------
subroutine Courtemanche_A_P01 (ict, dt, Vm, Istm, Iion, v_prm, v_courte, v_cr)
!-------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: ict
  real(rp),    intent(in)    :: dt, Istm, Vm
  real(rp),    intent(in)    :: v_prm(:)
  real(rp),    intent(out)   :: Iion
  real(rp),    intent(inout) :: v_courte(nvar_courte)
  real(rp),    intent(out)   :: v_cr(ncur_courte)
  type(t_courte)             :: courte
  type (t_prm)               :: param
  type (t_cur)               :: cur
  !
  call put_me_struct(v_courte,courte)
  call put_param(v_prm,param)
  !
  call f_currents(Vm,param,cur,courte,Iion)
  call f_concentrations(dt,cur,courte,Istm)
  call f_gates(dt,Vm,cur,courte)
     
  call get_me_struct(courte,v_courte)

  v_cr(1:ncur_courte) = (/ cur%INa,cur%IK1,cur%Ito,cur%IKur,cur%IKr,cur%IKs,cur%ICaL,&
                           cur%INaK,cur%INaCa,cur%IbCa, cur%IbNa,cur%IpCa,cur%Irel,  &
                           cur%Itr,cur%Iup,cur%Ileak,Iion  /)  
  return
end subroutine Courtemanche_A_P01
!-------------------------------------------------------------------------------
end module mod_courtemanche
!-------------------------------------------------------------------------------
