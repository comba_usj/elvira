! ------------------------------------------------------------------------------
module mod_genfunc
! ------------------------------------------------------------------------------
! This module contains general mathematical and useful functions
! to be used in finite element implementations. The catalog of
! implemented functions are:
! 1) det(A)      : calculates de determinant of A
! 2) eye(n)      : creates a nxn identity matrix
! 3) cross(a,b)  : calculates the cross product axb
! 4) gammaln(x)  : calculates the logarithm of the gamma function
! 5) gammax(x)   : calculates the gamma function
! 6) betai(x)    : calculates the incomplete beta function
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_error
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
! General functions used by other subrotuines
! from the module
! ------------------------------------------------------------------------------
function det(A)
! ------------------------------------------------------------------------------
! Returns the determinant of A
!
  implicit none
  real(rp), intent(in) :: A(:,:)
  real(rp)             :: x(3),det
  integer(ip)          :: i(3),j(3)
  data i /2,3,1/; data j /3,1,2/

  x=A(2,i)*A(3,j)-A(2,j)*A(3,i)
  det=sum(A(1,:)*x)
end function det
! ------------------------------------------------------------------------------
function eye(n)
! ------------------------------------------------------------------------------
! Returns a nxn identity matrix
!
  implicit none
  integer(ip), intent(in):: n
  integer(ip)            :: i
  real(rp)               :: eye(n,n)

  eye=0.0
  do i=1,n
     eye(i,i)=1.0
  end do
  return
end function eye
! ------------------------------------------------------------------------------
function inv(A)
! ------------------------------------------------------------------------------
! Returns the inverse of A. A 3x3 matrix

  implicit none
  real(rp),intent(in) :: A(:,:)
  real(rp)            :: detA, inv(size(A,dim=1),size(A,dim=2))

  detA=det(A)
  if (abs(detA).LT.1e-6) then
       call w_error(501,'inv(.)',1)
  else
    detA=1.0/detA
    inv(1,1)=(A(2,2)*A(3,3)-A(2,3)*A(3,2))
    inv(1,2)=-(A(1,2)*A(3,3)-A(1,3)*A(3,2))
    inv(1,3)=(A(1,2)*A(2,3)-A(1,3)*A(2,2))
    inv(2,1)=-(A(2,1)*A(3,3)-A(2,3)*A(3,1))
    inv(2,2)=(A(1,1)*A(3,3)-A(1,3)*A(3,1))
    inv(2,3)=-(A(1,1)*A(2,3)-A(1,3)*A(2,1))
    inv(3,1)=(A(2,1)*A(3,2)-A(2,2)*A(3,1))
    inv(3,2)=-(A(1,1)*A(3,2)-A(1,2)*A(3,1))
    inv(3,3)=(A(1,1)*A(2,2)-A(1,2)*A(2,1))
    inv=inv*detA
  endif
  return
end function inv
! ------------------------------------------------------------------------------
function cross(a,b)
! ------------------------------------------------------------------------------
! Returns the cross product axb
!
  implicit none
  real(rp),intent(in)  :: a(:),b(:)
  real(rp)             :: cross(size(a))
  integer(ip)          :: i(3),j(3)
  data i /2,3,1/, j /3,1,2/

  cross=a(i)*b(j)-a(j)*b(i)
  return
end function cross
! ------------------------------------------------------------------------------
function normainf(a)
! ------------------------------------------------------------------------------
! Return the inf-norm of a vector
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(in) :: a(:)
  real(rp)            :: normainf
  integer(ip)         :: i,n

  n=size(a)
  normainf=abs(a(1))
  do i=2,n
    if (abs(a(i)) > normainf) then
       normainf=abs(a(i))
    endif
  enddo
end function normainf
! ------------------------------------------------------------------------------
function norma(a)
! ------------------------------------------------------------------------------
! Return the 2-norm of a vector
!
  implicit none
  real(rp),intent(in) :: a(:)
  real(rp)            :: norma
  integer(ip)         :: i,n

  n=size(a)
  norma=0.0
  do i=1,n
    norma=norma+a(i)*a(i)
  enddo
  norma = sqrt(norma)
  return
end function norma
! ------------------------------------------------------------------------------
subroutine gammaln(x,gammln)
! ------------------------------------------------------------------------------
! Calculates the logarithm of the gamma function
! 
        
  real(rp), intent(in)  :: x
  real(rp), intent(out) :: gammln
  real(rp)              :: tmp,ser,cof(6),prog(6)
  real(rp)              :: c1,c2
  integer               :: j

  cof=(/76.18009173,-86.50532033,24.01409822,-1.231739516,0.120858003e-2, &
        -0.536382e-5/)
  c1=2.5066282746310005
  c2=1.000000000190015
  if(x .lt. 0) then
    gammln=1.0
  else
    tmp=x+5.5
    tmp=(x+0.5)*log(tmp)-tmp
    prog=(/(1.0/(x+j),j=1,size(cof))/)
    ser=c1*(c2+sum(cof*prog))/x
    gammln=tmp+log(ser)
  end if
end subroutine gammaln
! ------------------------------------------------------------------------------
subroutine gammax(x,gammx)
! ------------------------------------------------------------------------------
  real(rp), parameter  :: PI=3.141596535897932384626
  real(rp), intent(in) :: x
  real(rp), intent(out):: gammx
  real(rp)             :: gamln,gamtmp,sgn
  
  if(x .lt. 0) then
    write(*,*) 'Negative argument. Gamma function not defined'
    gamtmp=0.0
  else
    sgn=sign(1.0_rp,x-1.0_rp)
    if(abs(x-1.0) .gt. 1.0e-5) then
      call gammaln(x,gamln)
      gamtmp=exp(sgn*gamln)
    else
      call gammaln(2-x,gamln)
      gamtmp=PI*(1-x)/sin(PI*(1.0-x))
      gamtmp = gamtmp*exp(-gamln);
    end if
  endif
  gammx=gamtmp
  return 
end subroutine gammax
! ------------------------------------------------------------------------------
!function gammaln(x)
! ------------------------------------------------------------------------------
! Calculates the logarithm of the gamma function
!
!  implicit none
!  real(rp), intent(in)  :: x
!  real(rp)              :: gammaln
!  real(rp)              :: tmp,ser,cof(6),prog(6)
!  integer(ip)                 :: j
! 
!  cof=(/76.18009173,-86.50532033,24.01409822, &
!     -1.231739516,0.120858003e-2,-0.536382e-5/)

!  if(x<0) then
!    call w_error(lu_log,0,501,'gammaln(.)',1)
!    gammaln=1.0
!  else
!    tmp=x+5.5
!    tmp=(x+0.5)*log(tmp)-tmp
!    prog=(/(1.0/(x+j),j=1,size(cof))/)
!    ser=2.5066282746310005*(1.000000000190015+sum(cof*prog))/x
!    gammaln=tmp+log(ser)
!  end if
!end function gammaln          
! ------------------------------------------------------------------------------
!function gammax(x)
! ------------------------------------------------------------------------------
! Calculates the gamma function
!
!  implicit none
!  real(rp), intent(in) :: x
!  real(rp)             :: gammax
!  real(rp)             :: gamln,gamtmp
! 
!  if (x<0) then
!     call w_error(lu_log,0,502,'gammax(.)',1)
!     gamtmp=0.0
!  else
!     gamln=gammaln(x)
!     gamtmp=exp(gamln)
!  end if
!  gammax=gamtmp;
!end function gammax                                                     
! ------------------------------------------------------------------------------
subroutine betai(a,b,x,bti)
  
  real(rp), intent(in) :: a,b,x
  real(rp), intent(out):: bti
  real(rp)             :: bt,btcf
  real(rp)             :: D11,D12,D13
!
  if (abs(x) .le. 1.0E-6 .or. abs(x-1.0) .le. 1.0E-6) then
    bt=0.0
  else
    call gammaln(a+b,D11)
    call gammaln(a,D12)
    call gammaln(b,D13)
    bt=exp(D11-D12-D13+a*log(x)+b*log(1.0-x))
  end if
  if (x < (a+1.0)/(a+b+2.0)) then
    call betacf(a,b,x,btcf)
    bti=bt*btcf/a
  else
    call betacf(b,a,1.0-x,btcf)
    bti=1.0-bt*btcf/b
  end if
end subroutine betai
! ------------------------------------------------------------------------------
!function betai(a,b,x)
! ------------------------------------------------------------------------------
!
! Returns the incomplete beta function Ix(a, b).
!
!  implicit none
!  real(rp), intent(in) :: a,b,x
!  real(rp)             :: betai
!  real(rp)             :: bt

!  if (x < 0.0 .OR. x > 1.0) then
!     write(*,*) 'Bad x in routine betai'
!  end if
!  if (x == 0.0 .OR. x == 1.0) then
!     bt=0.0
!  else                       ! Factors in front of the continued fraction.
!     bt=exp(gammaln(a+b)-gammaln(a)-gammaln(b)+a*log(x)+b*log(1.0-x));
!  end if
!  if (x < (a+1.0)/(a+b+2.0)) then! Use continued fraction directly.
!      betai= bt*betacf(a,b,x)/a;
!  else                    ! Use continued fraction after 
!      betai=1.0-bt*betacf(b,a,1.0d0-x)/b; !  making the symmetry transformation.
!  endif
!end function betai
! ------------------------------------------------------------------------------
subroutine betacf(a,b,x,btcf)
  
  real(rp), intent(in) :: a,b,x
  real(rp), intent(out):: btcf
  integer, parameter :: MAXIT=100
  real(rp), parameter  :: EPS=3.0E-7, FPMIN=1.0E-30
  real(rp)             :: aa,c,d,del,h,qab,qam,qap
  integer(ip)            :: m,m2
!
  qab=a+b
  qap=a+1.0
  qam=a-1.0
  c=1.0
  d=1.0-qab*x/qap
  if (abs(d) < FPMIN) d=FPMIN
  d=1.0/d
  h=d
  do m=1,MAXIT
    m2=2*m
    aa=m*(b-m)*x/((qam+m2)*(a+m2))
    d=1.0+aa*d
    if (abs(d) < FPMIN) d=FPMIN
    c=1.0+aa/c
    if (abs(c) < FPMIN) c=FPMIN
    d=1.0/d
    h=h*d*c
    aa=-(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
    d=1.0+aa*d
    if (abs(d) < FPMIN) d=FPMIN
    c=1.0+aa/c
    if (abs(c) < FPMIN) c=FPMIN
    d=1.0/d
    del=d*c
!
    h=h*del
    if (abs(del-1.0) <= EPS) exit
  end do
  if (m > MAXIT) write(*,*) 'a or b too big, or MAXIT too small in betacf_s'
  btcf=h
end subroutine betacf
! ------------------------------------------------------------------------------
!function betacf(a,b,x)
!
! Used by betai: Evaluates continued fraction for 
! incomplete beta function by modified Lentz method
!
! implicit none
! integer(ip), parameter :: MAXIT=100
! real(rp), parameter    :: FPMIN=1.0E-30, EPS=3.0E-7
! real(rp), intent(in)   :: a,b,x
! real(rp)               :: betacf
! real(rp)               :: aa,c,d,del,h,qab,qam,qap
! integer(ip)            :: m,m2

!  qab=a+b;    ! These q~Rs will be used in factors that occur
!  qap=a+1.0;  ! in the coefficients (6.4.6). 
!  qam=a-1.0;
!  c=1.0;      ! First step of Lentz~Rs method.
!  d=1.0-qab*x/qap;
!  if (abs(d) < FPMIN) d=FPMIN
!  d=1.0/d
!  h=d
!  do m=1,MAXIT 
!     m2=2*m
!     aa=m*(b-m)*x/((qam+m2)*(a+m2))
!     d=1.0+aa*d ! One step (the even one) of the recurrence.
!     if (abs(d) < FPMIN) d=FPMIN
!     c=1.0+aa/c
!     if (abs(c) < FPMIN) c=FPMIN
!     d=1.0/d
!     h = h*d*c
!     aa = -(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
!     d=1.0+aa*d; ! Next step of the recurrence (the odd one).
!     if (abs(d) < FPMIN) d=FPMIN
!     c=1.0+aa/c
!     if (abs(c) < FPMIN) c=FPMIN
!     d=1.0/d
!     del=d*c
!     h = h*del
!     if (abs(del-1.0) < EPS) exit ! Are we done?
!  end do
!  if (m > MAXIT) then
!     call w_error(lu_log,0,503,'betacf',1)
!  end if       
!  betacf=h
!end function betacf
! ------------------------------------------------------------------------------
end module mod_genfunc
