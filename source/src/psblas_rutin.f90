! ------------------------------------------------------------------------------
module mod_psblas_solver 
! ------------------------------------------------------------------------------
  use mod_precision
  use mpi_mod;
  use mod_node
  use psb_base_mod;   use psb_prec_mod
  use psb_krylov_mod; use psb_util_mod
  !.
  use mod_estsparse
  implicit none
  
  type, public:: t_sset
    character(len=8) :: cmethd  !.'CG','CGS','BICG','BICGSTAB','BICGSTABL','RGMRES'
    character(len=4) :: ptype   !.Preconditioner NONE  DIAG  BJAC
    character(len=4) :: afmt    !.Storage format CSR COO JAD 
    integer(ip)      :: istopc  !.Stopping criterion
    integer(ip)      :: itmax   !.MAXIT
    integer(ip)      :: itrace  !.> 0 imprime info de la convergencia c/itrace iter.
    integer(ip)      :: irst    !.Restart for RGMRES  and BiCGSTABL
    real(rp)         :: eps     !.
  end type t_sset

!  -------------------------- Nodos a subdominio -------------------------------
!  Este dato me indica a cuantos busdominios pertenece un nodo y cuales son
  type, public :: t_dof_d
    private
    integer(ip), pointer  :: nd_d(:)=>NULL()
  end type t_dof_d
!  -----------------------------------------------------------------------------
  type(t_dof_d), private, pointer, save :: nd_sd(:)
!  -----------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
subroutine fill_psb_parts(n_nod, np, node)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: n_nod, np
  type(t_obnd)              :: node
  !.
  integer(ip)               :: i_err, i, n_d
  integer(ip)               :: dom(np)


  allocate (nd_sd(n_nod), stat=i_err)
  !.
  do i=1,n_nod
    call get_node_subdomain(i, n_d, dom, node)
    allocate (nd_sd(i)%nd_d(n_d), stat=i_err)
    nd_sd(i)%nd_d=dom(1:n_d)-1
  end do
  return
end subroutine fill_psb_parts
! ------------------------------------------------------------------------------
subroutine parts_psb (g_indx, n, np, pv, nv)
! ------------------------------------------------------------------------------
  implicit none
  integer, intent(in)  :: g_indx, n, np
  integer, intent(out) :: nv
  integer, intent(out) :: pv(*)
  !.
  
  nv      = size(nd_sd(g_indx)%nd_d)
  pv(1:nv)= nd_sd(g_indx)%nd_d(1:nv)
  write (*,10) '###.parts_psb::: g_indx, n, np, nv, pv ',g_indx, n, np, nv, pv(1:nv)
  return
10 format (A,20(:,1X,I4))
end subroutine parts_psb
! ------------------------------------------------------------------------------
subroutine psblas_solver (iflg, isol,slvrset, node, Fg, Xg)
! ------------------------------------------------------------------------------
  implicit none
  !.
  integer(ip), parameter     :: max=100
  integer(ip), intent(in)    :: iflg, isol
  type(t_slvr)               :: slvrset
  type(t_obnd)               :: node
  real(rp),intent(in)        :: Fg(:)
  real(rp),intent(inout)     :: Xg(:)                   
  !.-- sparse matrix and preconditioner
  type(psb_dspmat_type), save :: A
  type(psb_dprec_type),  save :: prec
  !.-- descriptor
  type(psb_desc_type),   save :: desc_a
  !.-- dense matrices
  real(rp), allocatable ,save :: B(:), X(:)
  !.-- blacs setting
  type (t_sset),         save :: psb_p
  !.
  integer(ip),           save :: ictxt, iam, np
  integer(ip),           save :: ndof_l, ite, iter_c
  integer(ip)                 :: ngdl, nnz, n_vl, k 
  !.
  integer(ip)                 :: row_g(max), col_g(max),n
  real(rp)                    :: val_g(max), Zt(10), err

  integer(ip),allocatable,save:: r_ind(:), halo_i(:) 
  integer(ip)                 :: i, iter,  err_act, info, i_err
  integer(ip),allocatable     :: row_i(:), col_i(:), bndel(:)
  real(rp)   ,allocatable,save :: X_i(:)
  real(rp)   ,allocatable      :: val_i(:)
  character (len=20)           :: name


  select case (iflg)
  case(1)
    psb_p = set_psblas_solver (isol) !.Recupero los parametros del solver
    !.Inicializacion de las librerias psblas
    call psb_erractionsave(err_act)
    call psb_init(ictxt)
    call psb_info(ictxt, iam, np)
    !.
    call psb_barrier(ictxt) 
    !.ngdl => numero de grados de libertad globales.
    ngdl = slvrset%ndof_g  
    call fill_psb_parts(ngdl, np, node)
    !.
    call psb_cdall(ictxt, desc_a, info, mg= ngdl, parts = parts_psb)
    !.
    if(info /= 0) then
      call psb_error(ictxt);  call psb_errpush(info,name)
      stop
    end if
    !.
    nnz  = get_nnz_sprs_m(slvrset%sprs_m) 
    call psb_spall( A, desc_a, info, nnz = nnz)
    if(info /= 0)  then
      call psb_error(ictxt); call psb_errpush(info,name)
      stop
    end if
    !.ndof_l => numero de grados de libertad en el dominio
    ndof_l = get_ndof_l_sprs_m(slvrset%sprs_m)
    !.--- Allocate B and X -----------------------------------------------------
    call psb_geall(  B, desc_a, info, n = ndof_l)
    call psb_geall(  X, desc_a, info, n = ndof_l)
    !.
    if(info /= 0) stop '###.Error en solver iterativo'
    call psb_barrier(ictxt)
    !.
    allocate (r_ind(ndof_l), stat=i_err)
    !.
    do i= 1, ndof_l
      call get_global_rowcol_sprs_m(i, slvrset%sprs_m, n_vl, row_g, col_g, val_g)
      call psb_spins(n_vl, row_g(1:n_vl), col_g(1:n_vl), val_g(1:n_vl),A,desc_a,info)
      r_ind(i)=row_g(1)
    end do
    !.
    call psb_barrier(ictxt)    
    !.
    call psb_cdasb( desc_a, info)
    call psb_spasb( A, desc_a, info, dupl=psb_dupl_err_,afmt=psb_p%afmt);! dupl=psb_dupl_add_
    if(info /= 0)  then
      call psb_errpush(info,name)
      stop
    end if
    call psb_barrier(ictxt)
    !----------------------------------------------------------------------------
    ! call psb_get_boundary(bndel, desc_a, info)
    call psb_get_overlap (bndel, desc_a, info)
    write (iam+550,'(A,20(:1X,I4))')'iam, LOCAL : ',iam, bndel 
    nnz=size(bndel)
    call psb_loc_to_glob(bndel(1:nnz), desc_a, info)
    write (iam+550,'(A,20(:1X,I4))')'iam, GLOBAL: ',iam, bndel(1:nnz)
    !----------------------------------------------------------------------------
    ! call psb_local_index(nnz, bndel(i), desc_a, info)
    do i=1, ndof_l
      call psb_sp_getrow(i, A, nnz, row_i, col_i, val_i, info, lrw=i)
      call psb_loc_to_glob(row_i(1:nnz), desc_a, info)
      call psb_loc_to_glob(col_i(1:nnz), desc_a, info)
      !.
      write (iam+550,'(A,20(:1X,I4))')'i , n_vl, row_g',i,nnz,row_i(1:nnz) 
      write (iam+550,'(A,20(:1X,I4))')'i , n_vl, col_g',i,nnz,col_i(1:nnz)
      write (iam+550,'(A,2(1X,I4),20(:1X,G12.6))')'i , n_vl, col_g', i, nnz, val_i(1:nnz)
    end do
    !.
    call psb_precinit( prec,  psb_p%ptype, info)
    call psb_barrier(ictxt)
    call psb_precbld(A, desc_a, prec,info)
    !.
    ite = 0; iter_c = 0
    !stop
  case (2)
    !.
    call psb_geins(ndof_l, r_ind(1:ndof_l), Fg(1:ndof_l), B, desc_a, info, psb_dupl_ovwrt_)
    call psb_geins(ndof_l, r_ind(1:ndof_l), Xg(1:ndof_l), X, desc_a, info, psb_dupl_ovwrt_)
    !.
    call psb_geasb( B, desc_a, info); call psb_geasb( X, desc_a, info)
    if (mod (ite, 2) == 0)  write (iam+550,'(A,20(:1X,G12.6))') 'X_antes:',X
    call psb_barrier(ictxt)
    call psb_krylov (psb_p%cmethd, A, prec, B, X, psb_p%eps, desc_a, info, &
                     itmax = psb_p%itmax,  iter=iter, err=err,          &
                     itrace= psb_p%itrace, istop=psb_p%istopc, irst=psb_p%irst)
    !.
    if (mod (ite, 2) == 0)  write (iam+550,'(A,20(:1X,G12.6))') 'X_solver:',X
    Xg=X 
    !call psb_barrier(ictxt)
    !.
    iter_c= iter + iter_c
    if (mod (ite, 250) == 0) then
      if ( iam ==    0  ) write (*,20) iam, ite, nint(iter_c/250.0), err
      if ( iam == (np-1)) write (*,20) iam, ite, nint(iter_c/250.0), err
      iter_c = 0
    end if
    !.
    ite =ite  + 1
    if (ite == 14) stop
  case (3:10) !.cleanup storage and exit
    call psb_gefree(B, desc_a, info)
    call psb_gefree(X, desc_a, info)
    call psb_spfree(A, desc_a, info)
    call psb_precfree(prec,   info)
    call psb_cdfree  (desc_a, info) 
  case default
    write (*,10) iflg; stop 
  end select
  return
10 format ('##.Error en psblas_solver, iflg erroneo:',I6)
20 format ('##.Iam: ', I4,' Step: ',I6,' Iterations: ', I6,' error: ',E10.3)
end subroutine psblas_solver
! ------------------------------------------------------------------------------
function set_psblas_solver (isol) result(sol_p)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)   :: isol
  type (t_sset) :: sol_p
  
  sol_p = t_sset('BICGSTAB', 'BJAC', 'CSR', 1, 1000, -1, 2, 1.d-8)  
  !.
  select case (isol)
  case (1);    sol_p%cmethd = 'CG'
  case (2);    sol_p%cmethd = 'CGS'
  case (3);    sol_p%cmethd = 'BICG'
  case (4);    sol_p%cmethd = 'BICGSTAB'
  case (5);    sol_p%cmethd = 'BICGSTABL'
  case (6);    sol_p%cmethd = 'RGMRES'
  case default;sol_p%cmethd = 'BICGSTAB'
  end select
  !.
  return
end function set_psblas_solver
! ------------------------------------------------------------------------------

! ------------------------------------------------------------------------------
end module mod_psblas_solver
