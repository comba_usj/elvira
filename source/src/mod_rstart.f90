!  -----------------------------------------------------------------------------
module mod_rstart
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error 
  use mod_filename
!  use mod_USER_monodomain
!  -----------------------------------------------------------------------------
  implicit none
!-------------------------------------------------------------------------------
!   Depending on the value of parameter RESTART, bainary files are generated
! with the necessary data to continue the simulation from that point.
! Transmembrane potentiales and ionic model state varaibles are saved. 
! Parameter RESTART has the following options:
! ------------------------------------------------------------------------------
!#RESTART, -1   
! Does not generates any restart file
! ------------------------------------------------------------------------------
!#RESTART, 0, FILE_R:"rstrt_25000_"    
! Reads the restart file. The root name of the file is "rstrt_25000_". Elvira
! adds the process number and the extension "bin". For instance, for process 8, 
! the full name of the restart file would be rstrt_25000_8.bin   
! ------------------------------------------------------------------------------
!#RESTART, 1, FILE_W:"rstrt_" 
! 2 5000 25000  
! Writes restart files. It can generate "n" files. In this case elvira generates 
! 2 files at increments 5000 y 25000, using the root name "rstrt_"
! ------------------------------------------------------------------------------
!#RESTART, 2, FILE_R:"rstrt_5000_", FILE_W:"rstrt02_"
! 2 1500 5000 
! Reads and writes restart files. FILE_R and FILE_W indicate the root name for
! the files to be read and written. In the example, the program will read
! restart files with the rootname "rstrt_5000_" and generates 2 restart files at
! increments 1500 and 5000 respectively
! -------------------------------------------------------------------------------
!#RESTART,   3, FILE_W:"rstrt_" 
! 5000
! This option wrtes a restart file every n iterations. In the example shown,
! elvira will generate a restart file every 5000 increments. 
! -------------------------------------------------------------------------------
!#RESTART,   4, FILE_R:"rstrt_5000_", FILE_W:"rstrt02_"
! 12500
! This option reads the reestart file, which root name is "rstrt_5000_", and 
! generates a restart file every 12500 increments
! -------------------------------------------------------------------------------
 type, public :: t_rsrt
   private
   integer(ip)             :: tipo=-1!.restart option (no restart as default)
   character (len=256)     :: fn_w   !.rootname for generated files 
   character (len=256)     :: fn_r   !.rootname for read files
   integer(ip), allocatable:: data(:)!.Inc at which restart files will be gen.
 end type t_rsrt
!  -----------------------------------------------------------------------------	
contains
! -----------------------------------------------------------------------------
! -----------------------------------------------------------------------------
subroutine lecc_rstart (lu_m, arch_d, strout, rstrt)
! -----------------------------------------------------------------------------
! This subroutine reeads the restart parameters
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  character (len=*), intent(in) :: arch_d, strout
  type(t_rsrt)                  :: rstrt
  !.
  character(len=12)             :: name
  integer(ip)                   :: tipo, io_err,lst


  backspace(lu_m)
  read (lu_m,*,iostat=io_err) name, tipo
  if (io_err > 0) call w_error (57,'name, tipo',1)
  !.
  rstrt%tipo= tipo
  !.
  select case (tipo)
  case(0);
    !.
    rstrt%fn_r = FileNameRelPath_WR (strout,'FILE_R',arch_d); !.Read file
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    lst = len_trim(rstrt%fn_r)
    write(*,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    write(lu_log,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    !.
  case(1); 
    !.
    rstrt%fn_w = FileNameRelPath_WR (strout,'FILE_W',arch_d); !.Writing file
    call read_inc(0,lu_m,rstrt);
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    write(*,20) size(rstrt%data)
    write(lu_log,20) size(rstrt%data)
    !.
  case(2);
    !.
    rstrt%fn_r = FileNameRelPath_WR (strout,'FILE_R',arch_d); !.Read file
    rstrt%fn_w = FileNameRelPath_WR (strout,'FILE_W',arch_d); !.Writing file
    call read_inc(2,lu_m,rstrt);
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    lst = len_trim(rstrt%fn_r)
    write(*,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    write(*,20) size(rstrt%data)
    write(lu_log,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    write(lu_log,20) size(rstrt%data)
    !.
  case(3); 
    !.
    rstrt%fn_w = FileNameRelPath_WR (strout,'FILE_W',arch_d); !.Writing file
    call read_inc(3,lu_m,rstrt);
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    write(*,30) rstrt%data(1)
    write(lu_log,30) rstrt%data(1)
    !.
  case(4); 
    !.
    rstrt%fn_r = FileNameRelPath_WR (strout,'FILE_R',arch_d); !.Read file
    rstrt%fn_w = FileNameRelPath_WR (strout,'FILE_W',arch_d); !.Writing file
    call read_inc(4,lu_m,rstrt);
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    lst = len_trim(rstrt%fn_r)
    write(*,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    write(*,30) rstrt%data(1)
    write(lu_log,*) '  Initial conditions from ',rstrt%fn_r(1:lst),'X.bin' 
    write(lu_log,30) rstrt%data(1)
    !.
  case default
    !.
    rstrt%tipo = -1
    write(*,10)  rstrt%tipo; write(lu_log,10)  rstrt%tipo 
    write(*,*) '  No restart files will be generated' 
    !.
  end select
  !.
  !.
10 format ('###.Restart parameter                             : ',I4)                 
20 format('  ',I14,' restart files are being generated')
30 format('   Restart files are being generated every',I14,' increments') 
end subroutine lecc_rstart
! -----------------------------------------------------------------------------
! -----------------------------------------------------------------------------
subroutine read_inc(ind, lu_m, rstrt)
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: ind, lu_m
  type(t_rsrt)                  :: rstrt
  integer(ip)                   :: n, io_err, i_err


  if (ind == 3 .or. ind == 4) then
    !.
    allocate(rstrt%data(1),stat=i_err)
    if (i_err > 0) call w_error(58, n, 1)
    read (lu_m,*,iostat=io_err) rstrt%data(1)
    !.
  else
    !.
    read (lu_m,*,iostat=io_err) n
    if (io_err > 0) call w_error (57,'n',1)
    allocate(rstrt%data(n),stat=i_err)
    if (i_err > 0) call w_error(58, n, 1)
    backspace(lu_m)
    read (lu_m,*,iostat=io_err) n, rstrt%data(1:n)
    !.
  end if
  !.
  if (io_err > 0) call w_error (57,'n, rstrt%data(1:n)',1)
  !.
  return
end subroutine read_inc
! ------------------------------------------------------------------------------
subroutine bin_write_rstart(lu_m,rstrt)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_rsrt)                  :: rstrt

  write (lu_m) '*RESTART', rstrt%tipo
  !.
  select case (rstrt%tipo)
  case(-1);
  case( 0);
    !.
    write (lu_m) rstrt%fn_r
    !.
  case( 1);
    !.
    write (lu_m) rstrt%fn_w
    call bin_write_data(lu_m,rstrt)
    !.
  case( 2);
    !.
    write (lu_m) rstrt%fn_r, rstrt%fn_w 
    call bin_write_data(lu_m,rstrt)
    !.
  case( 3);
    !.
    write (lu_m) rstrt%fn_w
    call bin_write_data(lu_m,rstrt)
    !.
  case( 4);
    !.
    write (lu_m) rstrt%fn_r, rstrt%fn_w
    call bin_write_data(lu_m,rstrt)
    !.
  case default
    write (*,*) 'Error in subroutine bin_write_rstart'
  end select
  !.
  return
end subroutine bin_write_rstart
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_write_data(lu_m, rstrt)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_rsrt)                  :: rstrt


  write (lu_m) size(rstrt%data)
  write (lu_m)      rstrt%data
  return
end subroutine bin_write_data
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine ascii_write_rstart(lu_m,rstrt)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_rsrt)                  :: rstrt

  write (lu_m,10) '*RESTART', rstrt%tipo
  !.
  select case (rstrt%tipo)
  case(-1);
  case( 0); 
    write (lu_m,15) rstrt%fn_r
  case( 1); 
    write (lu_m,15) rstrt%fn_w
    write (lu_m,30) size(rstrt%data), rstrt%data;
  case( 2); 
    write (lu_m,20) rstrt%fn_r, rstrt%fn_w 
    write (lu_m,30) size(rstrt%data), rstrt%data;
  case( 3);
    write (lu_m,15) rstrt%fn_w
    write (lu_m,30) size(rstrt%data), rstrt%data;
  case( 4); 
    write (lu_m,20) rstrt%fn_r, rstrt%fn_w
    write (lu_m,30) size(rstrt%data), rstrt%data;
  case default
    write (*,*) 'Error in subroutine bin_write_rstart'
  end select
  !.
  return

10 format (A,2X,I8)
15 format (A)
20 format (A,A)
30 format (I4,2X,10(:,2X,I12))
end subroutine ascii_write_rstart
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_read_rstart(lu_m, rstrt)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_rsrt)                  :: rstrt
  !.
  character (len=8)             :: str
  integer(ip)                   :: io_err

  read (lu_m,iostat=io_err) str, rstrt%tipo
  !.
  select case (rstrt%tipo)
  case(-1);
  case( 0);    
    !.
    read (lu_m,iostat=io_err) rstrt%fn_r
    !.
  case( 1);
    !.
    read (lu_m,iostat=io_err) rstrt%fn_w
    call bin_read_data(lu_m,rstrt)
    !.
  case( 2); 
    !.
    read (lu_m,iostat=io_err) rstrt%fn_r, rstrt%fn_w 
    call bin_read_data(lu_m,rstrt)
    !.
  case( 3);
    !.
    read (lu_m,iostat=io_err) rstrt%fn_w
    call bin_read_data(lu_m,rstrt)
    !.
  case( 4);
    !.
    read (lu_m,iostat=io_err) rstrt%fn_r, rstrt%fn_w
    call bin_read_data(lu_m,rstrt)
    !.
  case default
    write (*,*) 'Error in subroutine bin_read_rstart'
  end select
  !.
  return
end subroutine bin_read_rstart
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_read_data(lu_m, rstrt)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_rsrt)                  :: rstrt
  !.
  integer(ip)                   :: n, io_err, i_err


  read (lu_m, iostat=io_err) n
  if (io_err > 0) call w_error (58,'n',1)
  !.
  allocate(rstrt%data(n),stat=i_err)
  if (i_err > 0) call w_error(58, n, 1)
  !.
  read (lu_m,iostat=io_err) rstrt%data
  if (io_err > 0) call w_error (58,'n, rstrt%data(1:n)',1)
  !.
  return
end subroutine bin_read_data
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_t_rstrt(rstrt, tipo, n_rst)
! ------------------------------------------------------------------------------
  implicit none
  type(t_rsrt), intent(in)  :: rstrt
  integer(ip),  intent(out) :: tipo, n_rst

  tipo  = rstrt%tipo
  !.
  select case (tipo)
  case(-1:0);
     n_rst = 0
  case(1:2);
    !.
    n_rst = size(rstrt%data)
    !.
  case(3:4);
    !.
    n_rst = 1
    !.
  case default
    write(*,10) tipo; write(lu_log,10)  tipo 
  end select
  return
10 format ('###.Warning, par. restart: ', I8,' incorrect ')          

end subroutine get_t_rstrt
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_rstrt_fn_r (rstrt, fn_r, m)
! ------------------------------------------------------------------------------
  implicit none
  type(t_rsrt), intent(in)  :: rstrt
  integer(ip),  intent(out) :: m
  character (len=*)         :: fn_r 

  m        = len_trim(rstrt%fn_r)
  fn_r(1:m)= rstrt%fn_r(1:m)
  return
end subroutine get_rstrt_fn_r

! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_rstrt_fn_w (rstrt, fn_w, m)
! ------------------------------------------------------------------------------
  implicit none
  type(t_rsrt), intent(in)  :: rstrt
  integer(ip),  intent(out) :: m
  character (len=*)         :: fn_w 

  m        = len_trim(rstrt%fn_w)
  fn_w(1:m)= rstrt%fn_w(1:m)
  return
end subroutine get_rstrt_fn_w
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_rstrt_data(rstrt, i)  result(dt_i)
! ------------------------------------------------------------------------------
  implicit none
  type(t_rsrt), intent(in) :: rstrt
  integer(ip),  intent(in) :: i 
  integer(ip)              :: dt_i


  dt_i = rstrt%data(i)

  return
end function get_rstrt_data
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function deallocate_rstrt (rstrt) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type (t_rsrt)                :: rstrt
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: istat
  flg = 0
  if (allocated(rstrt%data)) then
      deallocate(rstrt%data, stat=istat)
      if (istat /= 0) flg = 1 
  else
      flg = -1
  endif
10 format ('###.Error freeing memory in deallocate_rstrt (mod_rstart.f90)')
end function deallocate_rstrt
end module mod_rstart
! ------------------------------------------------------------------------------
