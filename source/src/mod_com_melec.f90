! ------------------------------------------------------------------------------
module mod_com_EM_nd
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_error
  use mod_Iion
  use mod_meshdata
  use mod_stepdata
  use savestorCurrent
! ------------------------------------------------------------------------------
  implicit none
  !.
  integer(ip), parameter, private :: n_mc = 200 !.Max. Num. of cell models
  integer(ip), parameter, private :: mx_p = 100!.Max. Num. of par. of cell model
  integer(ip), parameter, private :: mx_cur=100!.Max. Num. of currents in cell model
  type ob_me_nd
    private
    integer(ip)               :: h_stm   !.flag to stimulus
    integer(ip)               :: p_prm   !.pointer to nodal parameter
    integer(ip)               :: t_cel   !.cell type
    real(rp), allocatable     :: v_me(:) !.cell model state variable vector
    integer(ip)               :: stp     !.count how many times the model 
  end type ob_me_nd                      !.has not been updated   
  !.
  type(ob_me_nd), private, allocatable, save  :: mcel(:)
  real(rp),       private,              save  :: I_Cm(n_mc) = 1.0 !.Cm inverse
  integer(ip),    private,              save  :: n_nod
  !.
  public   :: Inic_me_nd, eq_unicel_nd, get_EM_nd, set_EM_nd  
  private  :: Dim_ModElect, fill_stm_param, PotQionSopt, PotQionCopt  
!--------------------------------------------------------------------------------
contains
!--------------------------------------------------------------------------------
subroutine Inic_me_nd (meshdata, stepdata)
!--------------------------------------------------------------------------------
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
!--------------------------------------------------------------------------------
  implicit none
  type(t_mesh), intent(in) :: meshdata
  type(t_step), intent(in) :: stepdata
  !.
  integer(ip)              :: i, ki, kf, t_cel, n_cel

  n_nod = get_nnod(meshdata%node) !.Number of nodes
  !.
  call Dim_ModElect (meshdata)
  !.
  call fill_stm_param (n_nod,stepdata)
  !.
  ki=minval(mcel(:)%t_cel)
  kf=maxval(mcel(:)%t_cel)
  do i=ki,kf
    t_cel = i
    n_cel = count(mcel(:)%t_cel == t_cel)
    if (n_cel > 0) call PrintNumCellEM ('###.NODES',t_cel,n_cel)
  end do
  !.
  return
end subroutine Inic_me_nd
! -------------------------------------------------------------------------------
subroutine Dim_ModElect ( meshdata)
! -------------------------------------------------------------------------------
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
! -------------------------------------------------------------------------------
  implicit none
  type(t_mesh),intent(in)  :: meshdata
  !.
  integer(ip)              :: ierr
  integer(ip)              :: i, n_var, t_cel, n_prop
  real(rp)                 :: ICm

  allocate(mcel(n_nod),stat=ierr)
  if (ierr /= 0 ) call w_error (64,'mcel(nnod)',1)
  !.
  do i=1,n_nod
    call get_TcelMat_nd (meshdata, i, t_cel, ICm)
    n_prop = get_node_prop(i, meshdata%node)
    n_var = get_numstatevar (t_cel) 
    allocate(mcel(i)%v_me(n_var), stat=ierr)
    if (ierr /= 0 ) call w_error (63,'mcel(i)%v_me(n_var)',1)
    mcel(i)%h_stm = 0                        !.flag to stimulus
    mcel(i)%p_prm = 0                        !.pointer to nodal parameter
    mcel(i)%t_cel = t_cel                    !.cell type
    mcel(i)%stp   = 0                        !.Time optimization
    mcel(i)%v_me  = get_ic_em(t_cel, n_var)  !.Cell model initialization
    I_Cm(n_prop)   = ICm
  end do
  !.
  return
end subroutine Dim_ModElect
! -------------------------------------------------------------------------------
subroutine fill_stm_param (nnod, stepdata)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)  :: nnod
  type(t_step), intent(in)  :: stepdata
  !.
  integer(ip)               :: iaux(n_nod)
  integer(ip)               :: n, i

  !.Retrieving stimulation information. Filling pointer to stimulus
  iaux=0;  call get_stmls_nd(stepdata%stmls, n, iaux )
  if ( n > 0) then
    do i=1,nnod
      mcel(i)%h_stm = iaux(i)
    end do
  end if
  !.
  !.Retrieving nodal parameters for cell model
  iaux=0; call get_parampointer (stepdata%prmnd, n, iaux )
  if ( n > 0) then
    do i=1,nnod
      mcel(i)%p_prm = iaux(i)
    end do
  end if
  !.
  return
end subroutine fill_stm_param
! -------------------------------------------------------------------------------
! Function to retrieve the max_number of model parameters
! -------------------------------------------------------------------------------
function max_num_state_var ( meshdata) result (max_nvar)
! -------------------------------------------------------------------------------
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
! -------------------------------------------------------------------------------
  implicit none
  type(t_mesh),intent(in)  :: meshdata
  !.
  integer(ip)              :: i, n_var, t_cel
  integer(ip)              :: max_nvar
  real(rp)                 :: ICm

  max_nvar = 0
  do i=1,n_nod
    call get_TcelMat_nd (meshdata, i, t_cel, ICm)
    n_var = get_numstatevar (t_cel) 
    if(n_var>max_nvar) then
      max_nvar = n_var
    end if
  end do
  !.
  return
end function max_num_state_var
! -------------------------------------------------------------------------------
!--------------------------------------------------------------------------------!
!                                                                                !
!     CALCULATING IION AND INTEGRATING CELL MODEL AT NODAL LEVEL                 !
!                                                                                !
!--------------------------------------------------------------------------------!
! -------------------------------------------------------------------------------
subroutine eq_unicel_nd (iam, lu_e, iopt, itm, t, dt, Vo, Vp, Iion, meshdata, stepdata)
! -------------------------------------------------------------------------------
! Arguments:
!. lu_e : unit number for writing currents
!. iopt : temporal optimization (=0 without temporal optimization, Iion is updated
!          at each time step; > 0 temporal optimization)
!  itm  : increment number
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  type(t_mesh), intent(in)       :: meshdata
  type(t_step), intent(in)       :: stepdata  
  integer(ip), intent(in)        :: iam, lu_e, iopt, itm
  real(rp),    intent(in)        :: t, dt,Vp(:)
  real(rp),intent(inout)         :: Vo(:), Iion(:)

!  print *,'eq_unicel_nd, iopt',iopt
  if (iopt == 0) then
    call PotQionSopt (iam, lu_e, itm, t, dt, Vo, Vp, Iion, meshdata, stepdata)
  else
    call PotQionCopt (iam, lu_e, itm, t, dt, Vo, Vp, Iion, meshdata, stepdata)
  end if
  return
end subroutine eq_unicel_nd
! -------------------------------------------------------------------------------
subroutine PotQionSopt (iam, lu_e, itm, t, dt, Vo, Vp, Iion, meshdata, stepdata)
! -------------------------------------------------------------------------------
!. iam  : process number
!. lu_e : unit number for writing currents
!  itm  : increment number
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current vector
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)       :: iam, lu_e, itm
  real(rp),     intent(in)       :: t, dt, Vp(:)
  type(t_mesh), intent(in)       :: meshdata
  type(t_step), intent(in)       :: stepdata  
  real(rp),     intent(inout)    :: Vo(:), Iion(:)
  !.
  integer(ip)                    :: dat_i(mx_p)
  real(rp)                       :: dat_r(mx_p), p_rm(mx_p)
  !.
  integer(ip)                    :: i,p_stm, t_cel, p_prm, np, n, flag_ndout, n_prop
  real(rp)                       :: ICm, Istm, Jion
  real(rp)                       :: v_cr(mx_cur)

  flag_ndout = get_ndout_flag (stepdata%nd_out)
  do i= 1,n_nod
    !.Retreive nodal stimulus, if exists
    Istm = 0.0; p_stm= mcel(i)%h_stm 
    if (p_stm > 0) Istm = get_stimulus( p_stm, t, stepdata%stmls)
    !.
    t_cel = mcel(i)%t_cel  !.Cell type.
    n_prop = get_node_prop(i, meshdata%node)
    ICm   = I_Cm(n_prop)
     !.---- Retreiving cell parameters ---------
    call get_parameter_nd (t_cel, p_rm, np)
    p_prm = mcel(i)%p_prm   !.Pointer to nodal parameter
    !.
    if (p_prm > 0) then
      call get_mask_value_nd ( p_prm, stepdata%prmnd, n, dat_i, dat_r)
      p_rm(dat_i(1:n)) = dat_r(1:n)
    end if
    !.
    call get_Iion (t_cel,dt,Istm,0.0_rp,Vo(i),Vp(i),Jion,p_rm(1:np),mcel(i)%v_me, v_cr)
    Jion  = Jion + Istm;  Iion(i) = Jion
    !.
    Vo(i) = Vo(i)- Jion*dt*ICm
    !.
    if(flag_ndout>0) then
      call currents_storage(i,v_cr)
    endif
  end do
  !.
  if(flag_ndout>0) then
    call currentsavewrite(iam, lu_e,meshdata, itm, t)
  endif
  return
end subroutine PotQionSopt
! -------------------------------------------------------------------------------
subroutine PotQionCopt (iam, lu_e, itm, t, dt, Vo, Vp, Iion, meshdata, stepdata)
! -------------------------------------------------------------------------------
!. iam  : process number
!. lu_e : unit number for writing currents
!  itm  : increment number
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current vector
!. mshdata : Model data (nodal coordinates, properties, conectivities, etc)
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)       :: iam, lu_e, itm
  real(rp),     intent(in)       :: t, dt, Vp(:)
  type(t_mesh), intent(in)       :: meshdata
  type(t_step), intent(in)       :: stepdata  
  real(rp),     intent(inout)    :: Vo(:), Iion(:)
  !.
  integer(ip)                    :: dat_i(mx_p)
  real(rp)                       :: dat_r(mx_p), p_rm(mx_p)
  !.
  logical                        :: flg_cal
  !.
  integer(ip)                    :: i, p_stm, n, t_cel, np, p_prm, n_prop, flag_ndout
  real(rp)                       :: ICm, Istm, Jion, dt_c,  Irel=0.0
  real(rp)                       :: v_cr(mx_cur)

 
  flag_ndout = get_ndout_flag (stepdata%nd_out)
  do i= 1,n_nod
    !.Retreive nodal stimulus, if exists
    Istm = 0.0; p_stm= mcel(i)%h_stm 
    if (p_stm > 0) Istm = get_stimulus( p_stm, t, stepdata%stmls)
    !.
    t_cel = mcel(i)%t_cel  !.Cell type
    n_prop = get_node_prop(i, meshdata%node)
    ICm   = I_Cm(n_prop)
    !.-------- Time optimization ----------------------------------------------
    if ((t_cel == 4).or.(t_cel == 5).or.(t_cel == 6)) Irel=mcel(i)%v_me(25)
    !.
    call flag_opttemp (t_cel, itm, Istm, Irel, Vp(i),dt,mcel(i)%stp,flg_cal,dt_c)
    !.
    if (flg_cal) then
     !.---- Retreiving cell parameters ---------
      call get_parameter_nd (t_cel, p_rm, np)
      p_prm = mcel(i)%p_prm   !.Pointer to nodal parameter
      !.
      if (p_prm > 0) then
        call get_mask_value_nd ( p_prm, stepdata%prmnd, n, dat_i, dat_r)
        p_rm(dat_i(1:n)) = dat_r(1:n)
      end if
      !.
      call get_Iion (t_cel,dt_c,Istm,0.0_rp,Vo(i),Vp(i),Jion,p_rm(1:np),mcel(i)%v_me, v_cr)
      !.
      Jion = Jion + Istm; Iion(i) = Jion
      !.
      if(flag_ndout>0) then
        call currents_storage(i, v_cr)
      endif
    else
      Jion = Iion(i)
    end if
    !.
    Vo(i)= Vo(i)- Jion*dt*ICm
    !.
  end do
  if(flag_ndout>0) then
    call currentsavewrite(iam, lu_e, meshdata, itm, t)
  endif
  return
end subroutine PotQionCopt
! -------------------------------------------------------------------------------
subroutine get_EM_nd (i, n_v ,val) 
! -------------------------------------------------------------------------------
! Soubroutine retreives current value of the cell model state variables
! i   : Node number
! n_v : Number of state variables
! val : vector containing the value of state variables
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: i
  !.
  integer(ip), intent(out)  :: n_v
  real(rp),    intent(out)  :: val(:)

  n_v = size(mcel(i)%v_me)
  val(1:n_v)= mcel(i)%v_me
  return
end subroutine get_EM_nd
! -------------------------------------------------------------------------------
subroutine set_EM_nd (i, n_v ,val) 
! -------------------------------------------------------------------------------
! Soubroutine updates the cell model state variables in mcel, a global variable 
!  to the module
! i   : Node number
! n_v : Number of state variables
! val : vector containing the value of state variables
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: i, n_v
  real(rp),    intent(in)  :: val(:)

  mcel(i)%v_me =  val(1:n_v)
  return
end subroutine set_EM_nd
! -------------------------------------------------------------------------------
end module mod_com_EM_nd
! -------------------------------------------------------------------------------
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
! -------------------------------------------------------------------------------
module mod_com_EM_elm
! ------------------------------------------------------------------------------
  use mod_precision
  use mod_error
  use mod_Iion
  use mod_meshdata
  use mod_stepdata
! ------------------------------------------------------------------------------
  implicit none
  !.
  integer(ip), parameter, private :: n_mc = 20 !.Max. Num. of cell models
  integer(ip), parameter, private :: mx_p = 100!.Max. Num. of par. of cell model
  integer(ip), parameter, private :: mx_cur=100!.Max. Num. of currents in cell model
  !.
  type ob_me_elm
    private
!    integer(ip)               :: h_stm     !.pointer to elemental stimulus
    integer(ip)               :: p_prm     !.pointer to elemental parameter
    integer(ip)               :: t_cel     !.cell type of the element
    real(rp),   allocatable   :: v_me(:,:) !.cell model state variable vector
    integer(ip),allocatable   :: stp(:)    !.count how many times the model
  end type ob_me_elm                       !.has not been updated 
  !.
  type(ob_me_elm), private, allocatable, save  :: mcel(:)
  real(rp),        private                     :: I_Cm(n_mc) = 0.0 !.Cm inverse
  integer(ip),     private,          save      :: n_elm
  !.
  public   :: Inic_me_elm,   eq_unicel_elm, get_EM_elm, set_EM_elm
  private  :: Dim_ModElect, fill_stm_param, PotQionSopt, PotQionCopt  
contains
!--------------------------------------------------------------------------------
subroutine Inic_me_elm (iopt, nin, meshdata, stepdata)
!--------------------------------------------------------------------------------
!. iopt : temporal optimization (=0 without temporal optimization, Iion is updated
!          at each time step; > 0 temporal optimization)
!. nin   : number of element internal nodes
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
!--------------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)   :: iopt, nin
  type(t_mesh)             :: meshdata
  type(t_step)             :: stepdata
  !.
  integer(ip)              :: i, ki, kf, t_cel, n_cel

  n_elm = get_nelm (meshdata%elem)  !.Number of elements
  !.
  call Dim_ModElect (iopt,nin, meshdata)
  !.
  call fill_stm_param(n_elm,stepdata)
  !.
  ki=minval(mcel(:)%t_cel)
  kf=maxval(mcel(:)%t_cel)
  do i=ki,kf
    t_cel = i
    n_cel = count(mcel(:)%t_cel == t_cel)
    if (n_cel > 0) call PrintNumCellEM ('###.ELEMENTS',t_cel,n_cel)
  end do
  return
end subroutine Inic_me_elm
! -------------------------------------------------------------------------------
subroutine Dim_ModElect (iopt,nin, meshdata)
! -------------------------------------------------------------------------------
!. iopt : temporal optimization (=0 without temporal optimization, Iion is updated
!          at each time step; > 0 temporal optimization)
!. nin   : number of element internal nodes
!. meshdata : Model data (nodal coordinates, properties, conectivities, etc)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in) :: iopt, nin
  type(t_mesh), intent(in) :: meshdata
  integer(ip)              :: ierr, i, j, n_var, t_cel, n_prop
  real(rp)                 :: ICm

  allocate(mcel(n_elm), stat=ierr)
  if (ierr /= 0 ) call w_error (63,'mcel(n_elm)',1)
  ! -- Dimensioning control structure for temporal optimization -------
  do i=1,n_elm
    call get_TcelMat_elm (meshdata, i, t_cel, ICm)
    n_var = get_numstatevar (t_cel) 
    n_prop= get_elemprop(i,meshdata%elem)
    allocate(mcel(i)%v_me(n_var,nin), stat=ierr)
    if (ierr /= 0 ) call w_error (64,'mcel(i)%v_me(n_var,nin)',1)
   ! mcel(i)%h_stm = 0     !.flag to stimulus
    mcel(i)%p_prm = 0     !.pointer to element parameter
    mcel(i)%t_cel = t_cel !.cell type
    I_Cm(n_prop)   = ICm
    !.
    do j=1,nin
      mcel(i)%v_me(:,j)  = get_ic_em(t_cel, n_var)  !.Initializing cell model
    end do
    if (iopt > 0) then   !.Temporal optimization
      allocate(mcel(i)%stp(nin),stat=ierr)
      if (ierr /= 0 ) call w_error (64,'mcel(i)%stp(nin)',1)
      mcel(i)%stp   = 0     
    end if
  end do
  !.
  return
end subroutine Dim_ModElect
! -------------------------------------------------------------------------------
subroutine fill_stm_param (nelm, stepdata)
! -------------------------------------------------------------------------------
! Retreives and fills pointer to elemental stimulus and parameters
! nelm     : number of elements
! stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in)  :: nelm
  type(t_step), intent(in)  :: stepdata
  !.
  integer(ip)               :: iaux(nelm)
  integer(ip)               :: n, i

  !.Filling pointer of elemental stimulus, NOT USED SO FAR
  !iaux=0;  call get_stmls_elm(stepdata%stmls, n, iaux )
  !if ( n > 0) then
  !  do i=1,nelm
  !    mcel(i)%h_stm = iaux(i)
  !  end do
  !end if
  !.
  !.Filling pointer to elemental parameter
  iaux=0; call get_parampointer_elm (stepdata%prmelm, n, iaux ) 
  if ( n > 0) then
    do i=1,nelm
      mcel(i)%p_prm = iaux(i)
    end do
  end if
  !.
  return
end subroutine fill_stm_param
! -------------------------------------------------------------------------------
!                                                                                !
!        CALCULATING IION AND INTEGRATING CELL MODEL AT ELEMENT LEVEL            !
!                                                                                !
!--------------------------------------------------------------------------------!
! -------------------------------------------------------------------------------
subroutine eq_unicel_elm (iam, iopt, itm, nin, meshdata, stepdata, t, dt, Vo, Vp, Iion)
! -------------------------------------------------------------------------------
! Arguments:
!.  iam : process number
!. iopt : temporal optimization (=0 without temporal optimization, Iion is updated
!          at each time step; > 0 temporal optimization)
!  itm  : increment number
!. nin  : number of element internal nodes
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)         :: iam, iopt, itm, nin
  real(rp),intent(in)             :: t, dt, Vp(:,:)
  real(rp),intent(inout)          :: Vo(:,:), Iion(:,:)
  type(t_step)                    :: stepdata
  type(t_mesh)                    :: meshdata

  if (iopt == 0) then
    call PotQionSopt (iam,       nin, t, dt, Vo, Vp, Iion, meshdata, stepdata)
  else
    call PotQionCopt (iam, itm,  nin, t, dt, Vo, Vp, Iion, meshdata, stepdata)
  end if
  return
end subroutine eq_unicel_elm
! -------------------------------------------------------------------------------
subroutine PotQionSopt ( iam, nin, t, dt, Vo, Vp, Iion, meshdata, stepdata)
! -------------------------------------------------------------------------------
!. iam  : process number
!. nin  : number of element internal nodes
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)         :: iam, nin
  real(rp),    intent(in)         :: t, dt, Vp(:,:)
  real(rp),    intent(inout)      :: Vo(:,:), Iion(:,:)
  type(t_step)                    :: stepdata
  type(t_mesh)                    :: meshdata
  !.
  integer(ip)                     :: dat_i(mx_p)
  real(rp)                        :: dat_r(mx_p), p_rm(mx_p)
  !.
  integer(ip)                     :: j, i, n, t_cel, np, p_prm, n_prop
  real(rp)                        :: ICm, Jion, Istm
  real(rp)                        :: v_cr(mx_cur)

  do i= 1,n_elm 
    !.Retreiving stimulation current for the element, if exists.
    Istm = 0.0; !p_stm= mcel(i)%h_stm 
    !if (p_stm > 0) Istm = get_stimulus( p_stm, t, stepdata%stmls)
    !.
    t_cel = mcel(i)%t_cel  !.Cell type.
    n_prop= get_elemprop(i,meshdata%elem)
    ICm   = I_Cm(n_prop)
    !.---- Retreiving cell model parameters ---------
    call get_parameter_nd (t_cel, p_rm, np)
    p_prm = mcel(i)%p_prm   !.Pointer to element parameters
    !.
    if (p_prm > 0) then
      call get_mask_value_elm ( p_prm, stepdata%prmelm, n, dat_i, dat_r)
      p_rm(dat_i(1:n)) = dat_r(1:n)
    end if
    !.
    do j = 1, nin  !.Calling cell model
      call get_Iion (t_cel,dt,Istm,0.0_rp,Vo(j,i),Vp(j,i),Jion,p_rm(1:np),mcel(i)%v_me(:,j),v_cr)
      Vo(j,i) = Vo(j,i)- Jion*dt*ICm
      Iion(j,i) = Jion
    end do
  end do
  return
end subroutine PotQionSopt
! -------------------------------------------------------------------------------
subroutine PotQionCopt (iam, itm, nin, t, dt, Vo, Vp, Iion, meshdata, stepdata)
! -------------------------------------------------------------------------------
!. iam  : process number 
!. itm  : increment number 
!. nin  : number of element internal nodes
!. t    : time 
!. dt   : time step
!. Vo   : Potential
!. Vp   : Potential time derivative
!. Iion : ionic current
!. stepdata : Simulation data (time step, solver, stimulation history, etc) 
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)         :: iam, itm, nin
  real(rp),    intent(in)         :: t, dt, Vp(:,:)
  real(rp),    intent(inout)      :: Vo(:,:), Iion(:,:)
  type(t_step)                    :: stepdata
  type(t_mesh)                    :: meshdata
  !.
  integer(ip)                     :: dat_i(mx_p)
  real(rp)                        :: dat_r(mx_p), p_rm(mx_p)
  !.
  logical                         :: flg_cal
  integer(ip)                     :: j, i, n, t_cel, np, p_prm, n_prop
  real(rp)                        :: ICm, Jion, Istm, dt_c, Irel=0.0
  real(rp)                        :: v_cr(mx_cur)

  do i= 1,n_elm 
    !.Retreiving stimulation current for the element, if exists.
    Istm = 0.0; !p_stm= mcel(i)%h_stm 
    !if (p_stm > 0) Istm = get_stimulus( p_stm, t, stepdata%stmls)
    !.
    t_cel = mcel(i)%t_cel  !.Cell type.
    n_prop= get_elemprop(i,meshdata%elem)
    ICm   = I_Cm(n_prop)
    !.---- Retreiving cell model parameters ---------
    call get_parameter_nd (t_cel, p_rm, np)
    p_prm = mcel(i)%p_prm   !.Pointer to element parameters
    !.
    if (p_prm > 0) then
      call get_mask_value_elm ( p_prm, stepdata%prmelm, n, dat_i, dat_r)
      p_rm(dat_i(1:n)) = dat_r(1:n)
    end if
    !.
    do j = 1, nin  !.Calling cell model
      !.Irel only for Luo Rudy model -------------------------------
      if ((t_cel == 4).or.(t_cel == 5).or.(t_cel == 6)) Irel=mcel(i)%v_me(25,i)
      !.
      call flag_opttemp (t_cel, itm, Istm, Irel, Vp(j,i),dt,mcel(i)%stp(j),flg_cal,dt_c)
      !.
      if (flg_cal) then
        call get_Iion (t_cel,dt_c,Istm,0.0_rp,Vo(j,i),Vp(j,i),Jion,p_rm(1:np),mcel(i)%v_me(:,j),v_cr)
        !Qion = Qion + Jion
        Iion(j,i) = Jion 
      else
        Jion = Iion(j,i)
      end if
      Vo(j,i) = Vo(j,i)- Jion*dt*ICm
     end do
  end do
  return
end subroutine PotQionCopt
! -------------------------------------------------------------------------------
subroutine get_EM_elm (i, j, n_v , val) 
! -------------------------------------------------------------------------------
! Soubroutine retreives current value of the cell model state variables
! i   : Element number
! j   : jth-internal node
! n_v : Number of state variables
! val : vector containing the value of state variables
! -------------------------------------------------------------------------------
  implicit none
  integer(ip)               :: i,j
  !.
  integer(ip), intent(out)  :: n_v
  real(rp),    intent(out)  :: val(:)

  n_v = size(mcel(i)%v_me(:,j))
  val(1:n_v)= mcel(i)%v_me(:,j)
  return
end subroutine get_EM_elm
! -------------------------------------------------------------------------------
subroutine set_EM_elm (i, j, n_v , val) 
! -------------------------------------------------------------------------------
! Soubroutine updates the cell model state variables in mcel, a global variable 
!  to the module
! i   : Element number
! j   : jth-internal node
! n_v : Number of state variables
! val : vector containing the value of state variables
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: i,j,n_v
  real(rp),    intent(in)  :: val(:)

  mcel(i)%v_me(:,j)= val(1:n_v)
  return
end subroutine set_EM_elm
!--------------------------------------------------------------------------------
end module mod_com_EM_elm
!--------------------------------------------------------------------------------
!
