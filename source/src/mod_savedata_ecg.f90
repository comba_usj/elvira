! -------------------------------------------------------------------------------
module mod_savedata_ecg
! -------------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_node
  implicit none
! -------------------------------------------------------------------------------
  contains
! -------------------------------------------------------------------------------
subroutine save_pot_ecg (fn_post, ifrc, iam, ite, node, t, V)
! -------------------------------------------------------------------------------
  implicit none
!  integer(ip), parameter          :: n_pt = 50 !.=> 50*0.02= 1 ms
  character (len=*), intent(in)   :: fn_post
  integer(ip),       intent(in)   :: ifrc, iam, ite
  type(t_obnd),      intent(in)   :: node
  real(rp),    intent(in)         :: t, V(:)
  !.
  integer(ip),       save         :: lu_e, n_nod
  !.
  character (len = 256)           :: fn_elec
  integer(ip)                     :: m, i

  if (ite == 0) then
    call a_unit(lu_e);
    m = len_trim(fn_post); fn_elec = fn_post(1:m)//'ecg_prc_'
    call file_name (fn_elec, '.bin', iam, fn_elec, m)
    open (unit = lu_e, file = fn_elec(1:m), status='unknown', form='unformatted')
    !.
    n_nod = get_nnod (node)  !.numero de nodos
    write (lu_e) n_nod
    do i =1, n_nod
      write(lu_e) get_nd_num_g (i, node)
    end do
  end if
  !.
  if (mod(ite, ifrc) == 0) write (lu_e) t, V(1:n_nod)
  !.
  return
end subroutine save_pot_ecg
! -------------------------------------------------------------------------------
end module mod_savedata_ecg
! -------------------------------------------------------------------------------
