!  -----------------------------------------------------------------------------
module mod_jacobiano
!  -----------------------------------------------------------------------------
use mod_precision
!  -----------------------------------------------------------------------------
implicit none
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine jacobiano (idim, npe, xn, df, djb, dcf, ierr)
!  -----------------------------------------------------------------------------
!  idim        : dimension del problema              ==> INPUT
!  npe         : Numero de nodos por elemento        ==> INPUT
!  xn(idim,npe): coordenadas de los nodos            ==> INPUT
!  df(idim,npe): derivada de las funciones de forma  ==> INPUT
!
!  djb         : determinante del jacobiano                    ==> OUTPUT
!  dcf         : derivada cartesiana de las funciones de forma ==> OUTPUT
!  ierr        : si ocurre un error ierr > 0
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)        :: idim, npe
  real(rp), intent(in)          :: xn(idim,npe),df(idim,npe)
  real(rp), intent(out)         :: djb,dcf(idim,npe)
  integer(ip),intent(out)       :: ierr
  !.

  select case (idim)
  case (1)
    call jacob_1d (npe, df, xn, djb, dcf, ierr)
  case (2)
    call jacob_2d (npe, df, xn, djb, dcf, ierr)
  case (3)
    call jacob_3d (npe, df, xn, djb, dcf, ierr)
  case default
    ierr = 1 
  end select
!   write (*,*)  'jacobiano dcf',dcf
  return
end subroutine jacobiano
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine jacob_1d (npe, df, xn, djb, dcf, ierr)
!  -----------------------------------------------------------------------------
!      Inputs:
!         xn(*)   - Nodal coordinates for element
!         df(*)   - Shape derivatives at point
!                   df(i) = dN_i/dx
!         npe     - Number of nodes attached to element
!      Outputs:
!         djb     - Jacobian determinant at point
!         dcf     - Cartesian derivatives   
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)    :: npe
  real(rp), intent(in)      :: df(npe),xn(npe)
  real(rp), intent(out)     :: djb,dcf(npe) 
  integer(ip),intent(out)   :: ierr
  !.
  real(rp)                  :: jc_i

  ierr = 0

  djb=dot_product(xn,df);  
  if (djb <= 0.0) ierr = 1
  !.
  jc_i = 1.0/djb
  dcf  = jc_i*df
  return
end subroutine jacob_1d
!  -----------------------------------------------------------------------------
subroutine jacob_2d (npe, df, xn, djb, dcf, ierr)
!  -----------------------------------------------------------------------------
!      Inputs:
!         xn(2,*) - Nodal coordinates for element
!         df(2,*) - Shape derivatives at point
!                     df(1,i) = dN_i/dx
!                     df(2,i) = dN_i/dy
!         npe     - Number of nodes attached to element
!      Outputs:
!         djb     - Jacobian determinant at point
!         dcf     - Cartesian derivatives   
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip),    intent(in)     :: npe
  real(rp), intent(in)           :: df(2, npe), xn(2, npe)
  real(rp), intent(out)          :: djb,dcf(2,npe)
  integer(ip),intent(out)        :: ierr
  !.
  real(rp)                       :: jc(2,2),jc_i(2,2),i_det

  ierr = 0
  !.
  jc=matmul(df,transpose(xn)) 
  !.
  djb = jc(1,1)*jc(2,2) - jc(1,2)*jc(2,1)
  !.
  if (djb <= 0.0) ierr = 1
  !.
  i_det= 1.0/djb;
  !.
  jc_i(1,1) =   jc(2,2)*i_det;  jc_i(1,2) = - jc(1,2)*i_det
  jc_i(2,1) = - jc(2,1)*i_det;  jc_i(2,2) =   jc(1,1)*i_det
  !.
  !.Derivadas cartesianas de la funcion de forma
  dcf=matmul(jc_i, df)
  return
end subroutine jacob_2d
!  -----------------------------------------------------------------------------
subroutine jacob_3d (npe, df, xn, djb, dcf, ierr)
!  -----------------------------------------------------------------------------
!      Inputs:
!         x,y,z    - Nodal coordinates for element
!         df(3,*) - Shape derivatives at point
!                     df(1,i) = dN_i/dx
!                     df(2,i) = dN_i/dy
!                     df(3,i) = dN_i/dz
!         npe      - Number of nodes attached to element
!      Outputs:
!         xsj       - Jacobian determinant at point
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)        :: npe
  real(rp), intent(in)           :: df(3,npe),xn(3,npe)
  real(rp), intent(out)          :: djb,dcf(3,npe) 
  integer(ip),intent(out)        :: ierr
  real(rp)                       :: jc(3,3),ad(3,3),djb1


  ierr = 0
! Calculo de la traspuesta de la matriz jacobiana
  jc=0.0
  jc(1,1)=sum(df(1,:)*xn(1,:)); jc(2,1)=sum(df(1,:)*xn(2,:)); jc(3,1)=sum(df(1,:)*xn(3,:))
  jc(1,2)=sum(df(2,:)*xn(1,:)); jc(2,2)=sum(df(2,:)*xn(2,:)); jc(3,2)=sum(df(2,:)*xn(3,:))
  jc(1,3)=sum(df(3,:)*xn(1,:)); jc(2,3)=sum(df(3,:)*xn(2,:)); jc(3,3)=sum(df(3,:)*xn(3,:))
! Calculo del adjunto ad(3,3)
  ad(1,1)=jc(2,2)*jc(3,3) - jc(2,3)*jc(3,2)
  ad(1,2)=jc(3,2)*jc(1,3) - jc(3,3)*jc(1,2)
  ad(1,3)=jc(1,2)*jc(2,3) - jc(1,3)*jc(2,2)
  
  ad(2,1)=jc(2,3)*jc(3,1) - jc(2,1)*jc(3,3)
  ad(2,2)=jc(3,3)*jc(1,1) - jc(3,1)*jc(1,3)
  ad(2,3)=jc(1,3)*jc(2,1) - jc(1,1)*jc(2,3)

  ad(3,1)=jc(2,1)*jc(3,2) - jc(2,2)*jc(3,1)
  ad(3,2)=jc(3,1)*jc(1,2) - jc(3,2)*jc(1,1)
  ad(3,3)=jc(1,1)*jc(2,2) - jc(1,2)*jc(2,1)
! Calculo de determinante
  djb  = jc(1,1)*ad(1,1) + jc(1,2)*ad(2,1) + jc(1,3)*ad(3,1)
!
  if (djb <= 0.0) ierr = 1
!
  djb1 = 1.0/djb
! Calculo de la inversa xs(3,3)
  jc= djb1*ad
! Calculo de las derivadas cartesianas de la funcion de forma
  dcf=matmul(transpose(jc),df)
  return
end subroutine jacob_3d
!  -----------------------------------------------------------------------------
end module mod_jacobiano
