!  -----------------------------------------------------------------------------
module mod_sysref
!  ----------------------------------------------------------------------------
  use mod_precision
  use mod_error
!  -------------------------- SISTEMAS DE REFERENCIA ---------------------------
!  Elvio Heidenreich => 25/05/2005
!  Definicion de los campos del objeto Sistema de referencia
!  Donde:
!       s_rf = puntero al sistema de Coordenadas al que esta referido
!       orig = Coordenadas del origen del sistema de referencia referidas al
!              sistema de referencia indicado por s_rf
!       matr = Matriz de cosenos direcotres que definen el sistema de referen-
!              cia expresadas en el sistema de referencia indicado por el
!              campo s_rf
!       nume = numero de usuario del sistema de referencia
!       tipo = Tipo de sistema de referencia
!            = 0 -Cartesiano Global
!            = 1 -Cilindrico Global
!            = 2 -Esferico Global
!  -----------------------------------------------------------------------------
  type t_srf
    private
    integer(ip)  :: n_srf 
    integer(ip)  :: tipo
    real(rp)     :: orig(3)
    real(rp)     :: matr(3,3)
  end type t_srf
!  -----------------------------------------------------------------------------
  type, public:: t_obsrf
    private
    integer(ip)               :: nsrf = 0
    type(t_srf), allocatable  :: srf(:)
  end type t_obsrf
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine lec_sysref(lu_m, sysrf)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obsrf)              :: sysrf
  !.
  integer(ip)                :: io_err, i_err, i, j 
  
  
  read (lu_m,*, iostat = io_err) sysrf%nsrf !.numero de sistemas de referencia
  if (io_err > 0) call w_error (19,'sysrf%nsrf',1)
  !.
  allocate (sysrf%srf(sysrf%nsrf),stat=i_err)
  if (i_err > 0 ) call w_error (20,'sysrf%srf(sysrf%nsrf)',1)
  !.
  do i=1,sysrf%nsrf
    read (lu_m,*, iostat = io_err)  sysrf%srf(i)%n_srf,  sysrf%srf(i)%tipo, &
                                   (sysrf%srf(i)%orig(j),   j=1,3),         &
                                   (sysrf%srf(i)%matr(1,j), j=1,3),         &
                                   (sysrf%srf(i)%matr(2,j), j=1,3),         &
                                   (sysrf%srf(i)%matr(3,j), j=1,3)
    if (io_err > 0) call w_error (21,'sysrf%srf(i)%()',1)
  end do
!
  write(*,10) sysrf%nsrf;  write(lu_log,10) sysrf%nsrf 
! 
  return
10 format ('###.Sistemas de referencia,leidos                 : ',I8)
end subroutine lec_sysref
!  -----------------------------------------------------------------------------
function deallocate_rysrf(sysrf) result(flg)
!  -----------------------------------------------------------------------------
  implicit none
  type(t_obsrf)              :: sysrf
  integer(ip)                :: flg
  !.
  integer(ip)                :: istat
  
  if (allocated(sysrf%srf)) then
    deallocate (sysrf%srf, stat=istat)
    if (istat == 0) flg = 0 
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_rysrf
!  -----------------------------------------------------------------------------
subroutine print_refsys (out, sysrf)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: out
  type(t_obsrf)              :: sysrf
  !.
  integer(ip)                :: i

  write (out,10)
  write (out,20) sysrf%nsrf
  !.
  do i=1,sysrf%nsrf
    write (out,80)
    write (out,30)  sysrf%srf(i)%n_srf
    write (out,40)  sysrf%srf(i)%tipo
    write (out,50)  sysrf%srf(i)%orig
    write (out,60)  sysrf%srf(i)%matr(1,:)
    write (out,70)  sysrf%srf(i)%matr(2,:)       
    write (out,70)  sysrf%srf(i)%matr(3,:)   
  end do
  write (out,10)
  return
10 format ('====================================================================')
20 format ('$$$.Number of reference systems     :    ',I8)
30 format ('>>>>>>>>>>>>>>>>>>>>>>>>> Number    :    ',I8)
40 format ('>>>>>>>>>>>>>>>>>>>>>>>>> Type      :    ',I8)
50 format ('>>>>>>>>>>>>>>>>>>>>>>>>> Origin    :    ',3(2X,F12.6))
60 format ('>>>>>>>>>>>>>>>>>>>>>>>>> Matrix    :    ',3(2X,F12.6))
70 format ('                                         ',3(2X,F12.6))
80 format ('....................................................................')
end subroutine print_refsys
!  -----------------------------------------------------------------------------
subroutine bin_write_rsys(lu_m, sysrf)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obsrf)              :: sysrf
  !.
  integer(ip)                :: i

  
  write (lu_m) '#REFSYSTEM', sysrf%nsrf
  if (sysrf%nsrf > 0) then
    do i=1,sysrf%nsrf
      write (lu_m)  sysrf%srf(i)%n_srf, sysrf%srf(i)%tipo, &
                    sysrf%srf(i)%orig,  sysrf%srf(i)%matr
    end do
  end if
  return
end subroutine bin_write_rsys
!  -----------------------------------------------------------------------------
subroutine ascii_write_rsys(lu_m, sysrf)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obsrf)              :: sysrf
  !.
  integer(ip)                :: i

  
  write (lu_m,10) '#REFSYSTEM', sysrf%nsrf
  if (sysrf%nsrf > 0) then
    do i=1,sysrf%nsrf
      write (lu_m,10)  sysrf%srf(i)%n_srf, sysrf%srf(i)%tipo, &
                    sysrf%srf(i)%orig,  sysrf%srf(i)%matr
    end do
  end if
  return
10 format (A,2X,I3)
20 format (I3,2X,I3,/,3(2X,F12.6),/,3(:,2X,F12.6))
end subroutine ascii_write_rsys



!  -----------------------------------------------------------------------------
subroutine bin_read_rsys(lu_m, sysrf)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)    :: lu_m
  type(t_obsrf)              :: sysrf
  !.
  character (len=10)         :: str
  integer(ip)                :: io_err, i_err, i
  

  read (lu_m,iostat=io_err) str,sysrf%nsrf !.numero de sistemas de referencia
  if (io_err > 0) call w_error (19,'sysrf%nsrf',1)
  !.
  allocate (sysrf%srf(sysrf%nsrf),stat=i_err)
  if (i_err > 0 ) call w_error (20,'sysrf%srf(sysrf%nsrf)',1)
  !.
  do i=1,sysrf%nsrf
    read (lu_m,iostat=io_err)  sysrf%srf(i)%n_srf, sysrf%srf(i)%tipo, &
                               sysrf%srf(i)%orig,  sysrf%srf(i)%matr
    if (io_err > 0) call w_error (21,'sysrf%srf(i)%()',1)
  end do
  !.
  return
end subroutine bin_read_rsys
!  -----------------------------------------------------------------------------
end module mod_sysref
