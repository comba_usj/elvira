! ------------------------------------------------------------------------------
module mod_psblas_solver 
! ------------------------------------------------------------------------------
  use mod_precision
!  use mpi_mod;
  use mod_node
  use psb_base_mod;   use psb_prec_mod
  use psb_krylov_mod; use psb_util_mod
  !.
  use mod_estsparse
  implicit none
  !.
  type, public:: t_sset
    character(len=9) :: cmethd  !.'CG','CGS','BICG','BICGSTAB','BICGSTABL','RGMRES'
    character(len=4) :: ptype   !.Preconditioner NONE  DIAG  BJAC
    character(len=5) :: afmt    !.Storage format CSR COO JAD 
    integer(ip)      :: istopc  !.Stopping criterion
    integer(ip)      :: itmax   !.MAXIT
    integer(ip)      :: itrace  !.> 0 writes info regarding convergence c/itrace iter.
    integer(ip)      :: irst    !.Restart for RGMRES  and BiCGSTABL
    real(rp)         :: eps     !.
  end type t_sset

!  -------------------------- Nodos a subdominio -------------------------------
!  This data tells to how many subdomains a node belongs to and which are them
  type, public :: t_dof_d
    private
    integer(ip), allocatable  :: nd_d(:)
!    integer(ip), pointer  :: nd_d(:)=>NULL()
  end type t_dof_d
!  -----------------------------------------------------------------------------
  type(t_dof_d), private, allocatable, save :: nd_sd(:)
!  type(t_dof_d), private, pointer, save :: nd_sd(:)

contains

! ------------------------------------------------------------------------------
subroutine fill_psb_parts(n_nod, np, node)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: n_nod, np
  type(t_obnd)              :: node
  !.
  integer(ip)               :: i_err, i, n_d
  integer(ip), allocatable  :: dom(:)


  allocate (nd_sd(n_nod), stat=i_err)
  if (i_err > 0) call w_error (97,'nd_sd',1)
  allocate (dom(np),stat=i_err)
  if (i_err > 0) call w_error (97,'dom',1)
  !.
  do i=1,n_nod
    call get_node_subdomain(i, n_d, dom, node)
    allocate (nd_sd(i)%nd_d(n_d), stat=i_err)
    if (i_err > 0) call w_error (97,'nd_sd(i)%nd_d',1)
    nd_sd(i)%nd_d=dom(1:n_d)-1
  end do
  return
end subroutine fill_psb_parts
! ------------------------------------------------------------------------------
subroutine parts_psb (g_indx, n, np, pv, nv)
! ------------------------------------------------------------------------------
  implicit none
  integer, intent(in)  :: g_indx, n, np
  integer, intent(out) :: nv
  integer, intent(out) :: pv(:)
  !.
  
  nv      = size(nd_sd(g_indx)%nd_d)
  pv(1:nv)= nd_sd(g_indx)%nd_d(1:nv)
  return
10 format (A,20(:,1X,I4))
end subroutine parts_psb
! ------------------------------------------------------------------------------
subroutine psblas_solver (iflg, isol,slvrset, node, Fg, Xg)
! ------------------------------------------------------------------------------
  implicit none
  character (len = 13)       :: r_name = 'psblas_solver'
  integer(ip), parameter     :: max_n=100
  integer(ip), parameter     :: max_dom=2048
  integer(ip), intent(in)    :: iflg, isol
  type(t_slvr)               :: slvrset
  type(t_obnd)               :: node
  real(rp),intent(in)        :: Fg(:)
  real(rp),intent(inout)     :: Xg(:)                   
  !.-- sparse matrix and preconditioner
  type(psb_dspmat_type), save :: A
  type(psb_dprec_type),  save :: prec
  !.-- descriptor
  type(psb_desc_type),   save :: desc_a
  !.-- dense matrices
  real(rp), allocatable ,save :: B(:), X(:)
  !.-- blacs setting
  type (t_sset),         save :: psb_p
  !.
  integer(ip),           save :: ictxt, iam, np
  integer(ip),           save :: ndof_l, ite, iter_c
  integer(ip)                 :: ngdl, nnz, n_vl, k, j
  !.
  integer(ip)                 :: dst, src, n_dat
  integer(ip)                 :: dom(max_dom),  nd_v
  integer(ip)                 :: row_g(max_n), col_g(max_n), row_i(max_n), col_i(max_n)
  real(rp)                    :: val_g(max_n), val_i(max_n), err

  integer(ip),allocatable,save:: r_ind(:), halo_i(:)
  integer(ip)                 :: i, iter,  err_act, info, i_err, id_dm
  integer(ip)                 :: d_dat, r_dat
  !.
  character(len=20)           :: name
  !.
  logical                     :: flg_e


  select case (iflg)
  case(1)
    !.
    psb_p = set_psblas_solver (isol) !.Recover solver parameters
    !.Initializing psblas libreries
    err_act = psb_act_abort_
    call psb_set_erraction(err_act)
    call psb_init(ictxt)
    call psb_info(ictxt, iam, np)
    call psb_barrier(ictxt)
    !.
    ndof_l = get_ndof_l_sprs_m(slvrset%sprs_m)
    !.
    ngdl = slvrset%ndof_g     !.Number of global degrees of freedom
    call psb_cdall( ictxt, desc_a, info, mg=ngdl, vg=slvrset%vg)
    if(info /= 0) then
      call psb_errpush(100, r_name=r_name); goto 999
    end if
    !.
    nnz  = get_nnz_sprs_m(slvrset%sprs_m) !.Number of non zeros in local matrix
    call psb_spall( A, desc_a, info, nnz = nnz)
    if(info /= 0)  then
      call psb_errpush(110, r_name=r_name); goto 999
    end if
    !.-- Allocate B and X
    call psb_geall(  B, desc_a, info); call psb_geall(  X, desc_a, info)
    if(info /= 0)  then
      call psb_errpush(120, r_name=r_name); goto 999
    end if
    !.
    call psb_barrier(ictxt)
    !.
    allocate (r_ind(ndof_l),  stat=i_err)
    if (i_err > 0) call w_error (98,'r_ind(ndof_l)',1)
    !.
    flg_e = .false.
    do i= 1, ndof_l
      call get_global_rowcol_sprs_m(i, slvrset%sprs_m, n_vl, row_g, col_g, val_g)
      call psb_spins(n_vl, row_g(1:n_vl), col_g(1:n_vl), val_g(1:n_vl),A,desc_a,info)
      if(info /= 0) flg_e =.true.
      r_ind(i) =  row_g(1)
      !.
      if (flg_e) exit
      call get_node_subdomain(i, nd_v, dom, node)
      if (nd_v > 1) then
        do j=1, nd_v
          dst = dom(j) -1
          if (dst /= iam) then
            d_dat=n_vl
            call psb_snd (ictxt, d_dat, dst)
            call psb_snd (ictxt, row_g, dst)
            call psb_snd (ictxt, col_g, dst) 
            call psb_snd (ictxt, val_g, dst)
            src = dst
            call psb_rcv (ictxt, r_dat, src); n_dat=r_dat
            call psb_rcv (ictxt, row_i, src)
            call psb_rcv (ictxt, col_i, src)
            call psb_rcv (ictxt, val_i, src)
            !.
            call psb_spins(n_dat, row_i(1:n_dat), col_i(1:n_dat), val_i(1:n_dat), A, desc_a, info)
            if(info /= 0)  flg_e =.true.
          end if
        end do
      end if
    end do
    !.
    if (flg_e) write (*,*) '##.Iam: ',iam, ' ##.Nodo: ',i
    !.
    call psb_barrier(ictxt)
    !.
    call psb_cdasb(desc_a,info)
    !.
    if(info /= 0) then
      info=4010
      write(*,*) '###.Error in psblas_solver (psb_cdasb) ', iam
      call psb_errpush(info,name,a_err='psblas_solver sub.')
      call psb_error(ictxt)
      goto 999
    end if
    !.
    !.call psb_cdasb( desc_a, info)
    !.
    call psb_barrier(ictxt)
    call psb_spasb( A, desc_a, info, dupl = psb_dupl_add_, afmt=psb_p%afmt);
    call psb_barrier(ictxt)
    !.
    if(info /= 0)  then
      call psb_errpush(130, r_name=r_name); goto 999
    end if
    !.
    call psb_precinit( prec,  psb_p%ptype, info)
    if(info /= 0)  then
      call psb_errpush(140, r_name=r_name); goto 999
    end if
    call psb_barrier ( ictxt)
    !.
    call psb_precbld ( A, desc_a, prec, info)
    if(info /= 0)  then
      call psb_errpush(150, r_name=r_name); goto 999
    end if
    !.
    ite = 0; iter_c = 0
    !.
    allocate (halo_i(ndof_l), stat=i_err)
    !.
    call psb_glob_to_loc(r_ind, halo_i, desc_a, info, iact='Ignore', owned=.false.)
    !.
    k = 0
    do i=1,ndof_l
      if (halo_i(i) < 0) then
        id_dm = slvrset%vg( r_ind(i) );
        write (iam+200,'(6(2X,I6))') i, halo_i(i), r_ind(i), iam, id_dm
        call get_node_subdomain (i, nd_v, dom, node)
        write (iam+200,'(A,6(2X,I6))') 'Nodal subdomains:',dom(1:nd_v)-1
        k=1
      end if
    end do
    call psb_barrier ( ictxt)
    if (k == 1) stop '####.Negative Halo indices'
    !.
  case (2)
    !.
    do i=1,ndof_l
      j=r_ind(i)
      k=slvrset%vg(j)
      if (k == iam) then
        call psb_geins(1,(/ j/), (/Fg(i)/), B, desc_a, info, psb_dupl_ovwrt_)
        call psb_geins(1,(/ j/), (/Xg(i)/), X, desc_a, info, psb_dupl_ovwrt_)
      end if
    end do
    !.
    if(info /= 0)  then
      call psb_errpush(160, r_name=r_name); goto 999
    end if
    !.
    call psb_barrier(ictxt); call psb_geasb( B, desc_a, info); 
    if(info /= 0)  then
      call psb_errpush(170, r_name=r_name); goto 999
    end if
    !.
    call psb_barrier(ictxt); call psb_geasb( X, desc_a, info)
    if(info /= 0)  then
      call psb_errpush(180, r_name=r_name); goto 999
    end if
    !.
    call psb_barrier(ictxt)
    call psb_krylov (psb_p%cmethd, A, prec, B, X, psb_p%eps, desc_a, info, &
                     itmax = psb_p%itmax,  iter=iter, err=err,             &
                     itrace= psb_p%itrace, istop=psb_p%istopc, irst=psb_p%irst)
    !.
    call psb_halo(X, desc_a, info)
    !.
    do i=1,ndof_l
      k = halo_i(i)
      Xg(i) = X(k)
    end do
    !.
    iter_c= iter + iter_c
    if (mod (ite, 500) == 0) then
      if ( iam ==    0  ) write (*,20) iam, ite, nint(iter_c/250.0), err
      if ( iam == (np-1)) write (*,20) iam, ite, nint(iter_c/250.0), err
      iter_c = 0
    end if
    !.
    ite =ite  + 1
    !if (ite == 10) stop
  case (3:10) !.cleanup storage and exit
    call psb_gefree(B, desc_a, info)
    call psb_gefree(X, desc_a, info)
    call psb_spfree(A, desc_a, info)
    call psb_precfree(prec,   info)
    call psb_cdfree  (desc_a, info) 
  case default
    write (*,10) iflg; stop 
  end select
  return
999 call psb_error(ictxt)
10 format ('##.Error in psblas_solver, wrong iflg:',I6)
20 format ('##.Iam: ', I4,' Step: ',I10,' Iterations: ', I6,' error: ',E10.3)
end subroutine psblas_solver
! ------------------------------------------------------------------------------
function set_psblas_solver (isol) result(sol_p)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)   :: isol
  type (t_sset) :: sol_p
  
  sol_p = t_sset('BICGSTAB', 'BJAC', 'CSR', 1, 1000, -1, 2, 1.d-8)  
  !.
  select case (isol)
  case (1);    sol_p%cmethd = 'CG'
  case (2);    sol_p%cmethd = 'CGS'
  case (3);    sol_p%cmethd = 'BICG'
  case (4);    sol_p%cmethd = 'BICGSTAB'
  case (5);    sol_p%cmethd = 'BICGSTABL'
  case (6);    sol_p%cmethd = 'RGMRES'
  case default;sol_p%cmethd = 'BICGSTAB'
  end select
  !.
  return
end function set_psblas_solver

! ------------------------------------------------------------------------------
end module mod_psblas_solver
! ------------------------------------------------------------------------------
