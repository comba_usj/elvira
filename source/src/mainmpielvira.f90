! -------------------------------------------------------------------------------
program mainmpielvira
! -------------------------------------------------------------------------------
  use mod_precision
  use mpi_mod
  use mod_timer
  use mod_problemsetting
  use mod_monodominio
  !.
  implicit none

  integer(ip)             :: ierr, nproc, myrank, rc
  type(t_prblm)           :: prblm
  type(timer)             :: time

  call mpi_init(ierr)
  if (ierr .ne. MPI_SUCCESS) then
     print *,'Error starting MPI program. Terminating.'
     call MPI_ABORT(MPI_COMM_WORLD, rc, ierr)
  end if
  call mpi_comm_size (mpi_comm_world, nproc, ierr)
  call mpi_comm_rank (mpi_comm_world, myrank,ierr)
  !.
  !. Initializaing seed for stochastic simulations
  !.
  call random_seed
  !
  call mpi_barrier(mpi_comm_world, ierr)
  call start_timer  (time)
  call problemsetting (.true., myrank, nproc, mpi_comm_world, prblm)
  if(myrank==0) then
    call write_n_str(6, 80,'='); write (*,10) myrank, elapsed_time(time); 
    call write_n_str(6, 80,'='); 
  endif
  !.
  call mpi_barrier(mpi_comm_world, ierr)
  call monodominio(mpi_comm_world, prblm)
  !.
!  if(myrank==0) then
!    write (*,20) myrank, elapsed_time(time); 
!    call write_n_str(6, 80,'|'); write (*,20) myrank, elapsed_time(time); 
!    call write_n_str(6, 80,'|');
!  endif
  !.
  if(myrank==0) then
    call write_n_str(6, 80,'='); write (*,20) myrank, elapsed_time(time); 
    call write_n_str(6, 80,'='); 
    close(lu_log)
  endif
  call mpi_finalize(ierr)

10 format ('###.MYRANK: ', I4,' problem setting time: ', G15.7)
20 format ('###.MYRANK: ', I4,' total time          : ', G15.7)
end program mainmpielvira
