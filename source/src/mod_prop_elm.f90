!  -----------------------------------------------------------------------------
module mod_prop_elm
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
!  -----------------------------------------------------------------------------
  implicit none

!  -------------------------- ELEMENT PROPERTIES -------------------------------
!  -----------------------------------------------------------------------------
  type t_prelm
     private
     integer(ip)              :: n_pel    ! Property number
     integer(ip)              :: p_mat    ! Pointer to element material
     integer(ip), allocatable :: dat_i(:) ! Integer values assoc. to property
     real(rp), allocatable    :: dat_r(:) ! Real values assoc. to property
  end type t_prelm;
!  -----------------------------------------------------------------------------
  type, public:: t_obpel
    private
    integer(ip)                :: n_prp = 0 ! Total number of element properties
    type(t_prelm), allocatable :: pr_el(:)
  end type t_obpel
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------    
!  -----------------------------------------------------------------------------    
subroutine lec_prelm (lu_m, arch_d, strout, propelm)
!  -----------------------------------------------------------------------------
! This subroutine reads the properties of the elements
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter       :: max=100
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d,strout
  type (t_obpel)               :: propelm
  !.
  integer(ip)                  :: lu_lec, io_err, i_err
  integer(ip)                  :: i, nprp, n_int, n_rea, dat_i(max)
  real(rp)                     :: dat_r(max)


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (28,'subroutine lec_prelm',1)
  !.
  read (lu_lec,*,iostat=io_err) propelm%n_prp
  if (io_err > 0) call w_error (29,'propelm%n_prp',1)
  !.
  allocate (propelm%pr_el(propelm%n_prp), stat=i_err)
  if (i_err > 0 ) call w_error (30,'propelm%pr_el(propelm%n_prp)',1)
  !.
  do i = 1, propelm%n_prp
    read (lu_lec, *, iostat = io_err) propelm%pr_el(i)%n_pel,               &
                                      propelm%pr_el(i)%p_mat, n_int, n_rea, &
                                      dat_i(1:n_int),dat_r(1:n_rea)
    if (io_err > 0) call w_error (31,'dat_i(1:n_int),dat_r(1:n_rea)',1)
    !.
    allocate (propelm%pr_el(i)%dat_i(n_int),stat=i_err) 
    if (i_err > 0 ) call w_error (30,'propelm%pr_el(i)%dat_i(n_int)',1)
    if (n_int > 0) then
      propelm%pr_el(i)%dat_i(1:n_int) = dat_i(1:n_int)
    end if
    !.
    allocate (propelm%pr_el(i)%dat_r(n_rea),stat=i_err) 
    if (i_err > 0 ) call w_error (30,'propelm%pr_el(i)%dat_r(n_rea)',1)
    if (n_rea > 0) then
      propelm%pr_el(i)%dat_r(1:n_rea) = dat_r(1:n_rea)
    end if
    !.
    if (io_err > 0) call w_error (31,'propelm%pr_el(i)%()',1)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !.
  write(*,10) propelm%n_prp
  write(lu_log,10) propelm%n_prp
  nprp=propelm%n_prp
  n_int=size(propelm%pr_el(1)%dat_i)
  n_rea=size(propelm%pr_el(1)%dat_r)
  if(n_rea > 0) then
    write (std_o, 20) propelm%pr_el(1)%n_pel,                &
                    propelm%pr_el(1)%p_mat, n_int, n_rea,    &
                    (propelm%pr_el(1)%dat_i(i),i=1,n_int)
    write (std_o, 30) (propelm%pr_el(1)%dat_r(i),i=1,n_rea) 
  else
    write (std_o, 21) propelm%pr_el(1)%n_pel,                &
                    propelm%pr_el(1)%p_mat, n_int, n_rea,    &
                    (propelm%pr_el(1)%dat_i(i),i=1,n_int)
  end if
  n_int=size(propelm%pr_el(nprp)%dat_i)
  n_rea=size(propelm%pr_el(nprp)%dat_r)
  if(n_rea > 0) then
    write (std_o, 20) propelm%pr_el(nprp)%n_pel,                &
                    propelm%pr_el(nprp)%p_mat, n_int, n_rea,    &
                    (propelm%pr_el(nprp)%dat_i(i),i=1,n_int)
    write (std_o, 30) (propelm%pr_el(nprp)%dat_r(i),i=1,n_rea) 
  else
    write (std_o, 21) propelm%pr_el(nprp)%n_pel,                &
                    propelm%pr_el(nprp)%p_mat, n_int, n_rea,    &
                    (propelm%pr_el(nprp)%dat_i(i),i=1,n_int)
  end if
  !.
  return
10 format ('###.Element properties read                       : ',I8)
20 format (2(I7,1X),2(I3,1X),10(I4,1X))
21 format (2(I7,1X),2(I3,1X),10(I4,1X))
30 format (10(E12.3))
end subroutine lec_prelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
function get_numpropelm(propelm) result(n_prp) 
!  -----------------------------------------------------------------------------
  implicit none
  type (t_obpel)               :: propelm
  integer(ip)                  :: n_prp

  n_prp = propelm%n_prp
  return
end function get_numpropelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
function get_nummatelm(i,prelm) result(p_mat) 
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: i
  type (t_obpel)               :: prelm
  integer(ip)                  :: p_mat

  p_mat = prelm%pr_el(i)%p_mat
  return
end function get_nummatelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
function get_propelm_tcel  (i, prelm) result(t_cel)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: i
  type (t_obpel)           :: prelm
  integer(ip)              :: t_cel

  t_cel = 0
  if(size(prelm%pr_el(i)%dat_i)>0) then
    t_cel =  prelm%pr_el(i)%dat_i(1)
  endif
  return
end function get_propelm_tcel
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine get_propelm_mat_tcel (i, prelm, t_cel, p_mat)
!  -----------------------------------------------------------------------------
  integer(ip),    intent(in)  :: i
  type (t_obpel), intent(in)  :: prelm
  !
  integer(ip), intent(out)    :: t_cel, p_mat

  t_cel = 0
  if(size(prelm%pr_el(i)%dat_i)>0) then
    t_cel = prelm%pr_el(i)%dat_i(1)
  endif
  p_mat = prelm%pr_el(i)%p_mat
  return
end subroutine get_propelm_mat_tcel
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine get_propelm(i, prelm, p_mat, n_rea, dat_r)
!  -----------------------------------------------------------------------------
  integer(ip),    intent(in)  :: i
  type (t_obpel), intent(in)  :: prelm
  !
  integer(ip), intent(out)    :: p_mat
  integer(ip), intent(out)    :: n_rea
  real(rp),    intent(out)    :: dat_r(:)


  p_mat = prelm%pr_el(i)%p_mat
  n_rea = size(prelm%pr_el(i)%dat_r)
  if (n_rea>0) then
     dat_r(1:n_rea) = prelm%pr_el(i)%dat_r(1:n_rea)
  end if
  return
end subroutine get_propelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine bin_write_prelm (lu_m, np_ed, lp_ed, propelm)
!  -----------------------------------------------------------------------------
! This subroutine writes the element properties (binary)
!       lu_m : logic unit
!       np_ed: number of elemental properties in domain
!       lp_ed: list with property number
!       propelm: element properties
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, np_ed, lp_ed(:)
  type (t_obpel)               :: propelm
  !.
  integer(ip)                  :: indx(propelm%n_prp)
  integer(ip)                  :: n_int, n_rea, i, j, k
!  -----------------------------------------------------------------------------


  write (lu_m) '#PROP_ELEM',np_ed !.Number of element properties
  !.
  if (np_ed > 0) then
    call quick_sort (propelm%n_prp,lp_ed,indx)
    !.
    do i = 1, propelm%n_prp
      k=indx(i); j = lp_ed(k)
      !.
      if ( j > 0 ) then
        n_int = size(propelm%pr_el(k)%dat_i)
        n_rea = size(propelm%pr_el(k)%dat_r)
        write (lu_m) j,propelm%pr_el(k)%p_mat, n_int, n_rea
        if (n_int > 0) write (lu_m) propelm%pr_el(k)%dat_i
        if (n_rea > 0) write (lu_m) propelm%pr_el(k)%dat_r
      end if
    end do
  end if
  return
end subroutine bin_write_prelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine ascii_write_prelm (lu_m, np_ed, lp_ed, propelm)
!  -----------------------------------------------------------------------------
! This subroutine writes the element properties (ascii)
!       lu_m : logic unit
!       np_ed: number of elemental properties in domain
!       lp_ed: list with property number
!       propelm: element properties
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m, np_ed, lp_ed(:)
  type (t_obpel)               :: propelm
  !.
  integer(ip)                  :: indx(propelm%n_prp)
  integer(ip)                  :: n_int, n_rea, i, j, k
!  -----------------------------------------------------------------------------


  write (lu_m,10) '#PROP_ELEM',np_ed !.Number of element properties
  !.
  if (np_ed > 0) then
    call quick_sort (propelm%n_prp,lp_ed,indx)
    !.
    do i = 1, propelm%n_prp
      k=indx(i); j = lp_ed(k)
      !.
      if ( j > 0 ) then
        n_int = size(propelm%pr_el(k)%dat_i)
        n_rea = size(propelm%pr_el(k)%dat_r)
        write (lu_m,20) j,propelm%pr_el(k)%p_mat, n_int, n_rea
        if (n_int > 0) write (lu_m,20) propelm%pr_el(k)%dat_i
        if (n_rea > 0) write (lu_m,30) propelm%pr_el(k)%dat_r
      end if
    end do
  end if
  return
10 format (A,2X,I3)
20 format (10(:,2X,I4))
30 format (10(:,2X,F12.6))
end subroutine ascii_write_prelm
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine bin_read_prelm (lu_m, propelm)
!  -----------------------------------------------------------------------------
! This subroutine reads the element properties
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type (t_obpel)               :: propelm
  !.
  character (len=10)           :: str
  integer(ip)                  :: io_err, i_err, n_int, n_rea ,i


  read (lu_m, iostat=io_err) str, propelm%n_prp !.Number of element properties
  if (io_err > 0) call w_error (29,'propelm%n_prp',1)
  !.
  if (propelm%n_prp > 0) then
    allocate (propelm%pr_el(propelm%n_prp), stat=i_err)
    if (i_err > 0 ) call w_error (30,'propelm%pr_el(propelm%n_prp)',1)
     !.
    do i = 1, propelm%n_prp
      read (lu_m, iostat = io_err) propelm%pr_el(i)%n_pel, &
                                      propelm%pr_el(i)%p_mat, n_int, n_rea
      if (io_err > 0) call w_error (31,'propelm%pr_el(i)%n_pel, %p_mat, n_int, n_rea',1)
      !.
      allocate (propelm%pr_el(i)%dat_i(n_int),stat=i_err) 
      if (i_err > 0 ) call w_error (30,'propelm%pr_el(i)%dat_i(n_int)',1)
      if (n_int > 0) then
        read (lu_m, iostat = io_err) propelm%pr_el(i)%dat_i
        if (io_err > 0) call w_error (31,'propelm%pr_el(i)%dat_i',1)
      end if
      !.
      allocate (propelm%pr_el(i)%dat_r(n_rea),stat=i_err) 
      if (i_err > 0 ) call w_error (30,'propelm%pr_el(i)%dat_r(n_rea)',1)
      if (n_rea > 0) then
        read (lu_m, iostat = io_err) propelm%pr_el(i)%dat_r
        if (io_err > 0) call w_error (31,'propelm%pr_el(i)%dat_r',1)
      end if
    end do
  end if
  return
end subroutine bin_read_prelm
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function deallocate_prelm (propelm) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type (t_obpel)               :: propelm
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i_err, i, istat


  if (allocated(propelm%pr_el)) then
    i_err = 0
    do i=1,propelm%n_prp
      deallocate (propelm%pr_el(i)%dat_i, stat=istat)
      if (istat /= 0) i_err = 1
      deallocate (propelm%pr_el(i)%dat_r, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (propelm%pr_el, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  return
end function deallocate_prelm
! ------------------------------------------------------------------------------
end module mod_prop_elm
! ------------------------------------------------------------------------------
