module mod_busqueda
! ------------------------------------------------------------------------------
! Este modulo contiene las siguiente subrutinas
!             #.subroutine i_swap (i,j)
!             #.subroutine quick_sort (n,arr,indx)
!             #.subroutine sort (n,arr)
!             #.subroutine  b_search (arr,ind,n,key,pos,fou)
!             #.function f_crst(ir,jc,row,col)
!             #.function bbveco (arr,n,key)
! ------------------------------------------------------------------------------
use mod_precision
! ------------------------------------------------------------------------------
contains
! ------------------------------------------------------------------------------
  subroutine i_swap (i,j)
! ------------------------------------------------------------------------------
! I_SWAP swaps two integer values.
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)       :: i,j,k

  k = i; i = j; j = k
  return
  end subroutine i_swap
! ------------------------------------------------------------------------------
  subroutine quick_sort (n,arr,indx)
! ------------------------------------------------------------------------------
! subrutina qsort
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter         :: m=10,nstack=50
  integer(ip)                    :: n,i,indxt,ir,j,jstack,k,l
  real(rp)                       :: a
  integer(ip),dimension (nstack) :: istack
  integer(ip),dimension (:)      :: indx
  integer(ip),dimension (:)      :: arr

  do j=1,n
     indx(j)=j
  end do
  jstack=0
  l=1
  ir=n
1 if (ir-l.lt.m) then
    do j=l+1,ir
      indxt=indx(j)
      a=arr(indxt)
      do i=j-1,1,-1
        if (arr(indx(i)).le.a) goto 2
        indx(i+1)=indx(i)
      end do
      i=0
2     indx(i+1)=indxt
    end do
    if(jstack.eq.0) return
    ir=istack(jstack)
    l=istack(jstack-1)
    jstack=jstack-2
  else
    k=(l+ir)/2
    call i_swap(indx(k),indx(l+1))
    if(arr(indx(l+1)).gt.arr(indx(ir)))then
      call i_swap (indx(l+1),indx(ir))
    end if
    if(arr(indx(l)).gt.arr(indx(ir)))then
    	call i_swap (indx(l),indx(ir))
    end if
    if(arr(indx(l+1)).gt.arr(indx(l)))then
    	call i_swap (indx(l+1),indx(l))
    end if
    i=l+1
    j=ir
    indxt=indx(l)
    a=arr(indxt)
3   continue
    i=i+1
    if (arr(indx(i)).lt.a) goto 3
4   continue
    j=j-1
    if (arr(indx(j)).gt.a) goto 4
    if (j.lt.i) goto 5
    call i_swap (indx(i),indx(j))
    goto 3
5   indx(l)=indx(j)
    indx(j)=indxt
    jstack=jstack+2
    if (jstack.gt.nstack) stop '##.NSTACK TOO SMALL IN QUICK_SORT '
    if (ir-i+1.ge.j-l) then
      istack(jstack)=ir
      istack(jstack-1)=i
      ir=j-1
    else
      istack(jstack)=j-1
      istack(jstack-1)=l
      l=i
    end if
  end if
  goto 1
end subroutine quick_sort
!  -----------------------------------------------------------------------------
subroutine sort (n,arr)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter         :: m=7,nstack=50
  integer(ip)                    :: n
  integer(ip), dimension(:)      :: arr
  integer(ip), dimension(nstack) :: istack
  integer(ip)                    :: i,ir,j,jstack,k,l,a

  jstack=0; l=1; ir=n
1 if(ir-l.lt.m)then
    do j=l+1,ir
      a=arr(j)
      do i=j-1,l,-1
        if(arr(i).le.a)goto 2
        arr(i+1)=arr(i)
      enddo
      i=l-1
2     arr(i+1)=a
    enddo
    if (jstack.eq.0) return
    ir=istack(jstack)
    l=istack(jstack-1)
    jstack=jstack-2
  else
    k=(l+ir)/2 
    call i_swap (arr(k),arr(l+1))
    if(arr(l).gt.arr(ir))then
      call i_swap (arr(l),arr(ir))
    endif
    if(arr(l+1).gt.arr(ir))then
      call i_swap (arr(l+1),arr(ir))
    endif
    if(arr(l).gt.arr(l+1))then
      call i_swap (arr(l),arr(l+1)) 
    endif
    i=l+1
    j=ir
    a=arr(l+1)
3   continue 
    i=i+1
    if (arr(i).lt.a) goto 3
4   continue
    j=j-1
    if (arr(j).gt.a) goto 4
    if (j.lt.i) goto 5
    call i_swap (arr(i),arr(j))
    goto 3
5   arr(l+1)=arr(j)
    arr(j)=a
    jstack=jstack+2
    if(jstack.gt.nstack) stop '##.NSTACK TOO SMALL IN SORT'
    if(ir-i+1.ge.j-l)then
      istack(jstack)=ir
      istack(jstack-1)=i
      ir=j-1
    else
      istack(jstack)=j-1
      istack(jstack-1)=l
      l=i
    endif
  endif
  goto 1
end subroutine sort
! ------------------------------------------------------------------------------
subroutine  b_search (arr,ind,n,key,pos,fou)
! ------------------------------------------------------------------------------
! busqueda binaria
! datos:
!       arr,ind: array de datos e indices
!       n    : dimension maxima del array a e ind
!       key  : elemento buscado
!       pos  : posicion del elemento en el array
!       fou  : variable logica, cierta se se ha encontrado el elemento
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), dimension (:),intent (in) :: arr,ind
  integer(ip), intent (in)               :: key
  integer(ip), intent (in)               :: n
  integer(ip), intent (out)              :: pos
  logical, intent (out)              :: fou
  integer(ip)                            :: l,r,mid,k
  
  fou =.false. 
  l = lbound(arr,1)
  if (n < l) then
    return
  end  if
  r = n
  if (key <= n) then
    if (key == arr(key)) then
      pos=key; fou=.true.
      return
    elseif (key > arr(key)) then
      l = key
    else
      r = key
    end if
  end if
! 
  do
    mid = (l+r)/2
    k   = ind(mid)
    if (key < arr(k))then
      if (l >= r) then  !.el elemento no fue encontrado
        exit
      end  if
      r = mid
    elseif (key > arr(k)) then 
      if (l >= r) then  !.el elemento no fue encontrado
        exit
      end  if
      l = mid+1
    else                 !.el elemento fue encontrado
      pos=  k  
      fou= .true.
      exit
    end  if
  end  do
  return
end  subroutine  b_search
! ------------------------------------------------------------------------------
function f_crst(ir,jc,row,col)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip)                            :: f_crst
  integer(ip), dimension (:),intent (in) :: row(:),col(:)
  integer(ip), intent (in)               :: ir,jc
  integer(ip)                            :: i,j,n

  i = row(ir); j = row(ir+1)-1; n=j-i+1
  f_crst=i+bbveco(col(i:j),n,jc)-1
  return
end function f_crst
! ------------------------------------------------------------------------------
function bbveco (arr,n,key)
! ------------------------------------------------------------------------------
! busqueda binaria en un vector ordenado
! datos:
!       arr  : array de datos
!       n    : dimension maxima del array
!       key  : elemento buscado
! ------------------------------------------------------------------------------
  implicit none
  integer(ip)                            :: bbveco
  integer(ip), dimension (:),intent (in) :: arr
  integer(ip), intent (in)               :: key
  integer(ip), intent (in)               :: n
  integer(ip)                            :: l,r,mid
  
  bbveco=0
  l = lbound(arr,1) 
  if (n < l) return
  r = n
! 
  do
    mid = (l+r)/2
    if (key < arr(mid))then
      if (l >= r) exit   !.el elemento no fue encontrado
      r = mid
    elseif( key > arr(mid)) then 
      if (l >= r) exit   !.el elemento no fue encontrado
      l = mid+1
    else                  !.el elemento fue encontrado
      bbveco= mid  
      exit
    end  if
  end  do
  return
end function bbveco
!  -----------------------------------------------------------------------------
end module mod_busqueda
