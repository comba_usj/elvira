! -------------------------------------------------------------------------------
module mod_problemsetting
! -------------------------------------------------------------------------------
  use mod_precision
  use mod_timer
  use mod_meshdata
  use mod_stepdata
  use mod_estsparse
  use mod_preprocesor
  use mpi_mod
! -------------------------------------------------------------------------------
  implicit none
  type, public :: t_prblm
    type(t_mesh)       :: mshdata
    type(t_step)       :: stpdata
    ! type(t_dof)        :: dof
    type(t_slvr)       :: slvrset
  end type t_prblm
! -------------------------------------------------------------------------------
contains
! -------------------------------------------------------------------------------
subroutine problemsetting (flg_read, myrank, nproc, comm, prblm)
! -------------------------------------------------------------------------------
  !use modPostProceso
  implicit none
  logical                 :: flg_read
  integer(ip), intent(in) :: myrank, nproc, comm
  type(t_prblm)           :: prblm
  !.
  type(timer)             :: tcpu(10)
  integer(ip)             :: lu_msh, n_nod, n_elm
  logical                 :: flg_fin =.false.
  !.
  character (len=256)     :: fname, fn_bin
  integer(ip)             :: lu_m, io_err, m, ierr, ndof_g!, n_nn, n_st(100),i
!  integer(ip)             :: dat_i, p_prop, pmat, i

  !.------- Opening log file --------
  !.Reading input data files and output file name (for postprocess)
  call fun_getarg (1,'-i',prblm%mshdata%fn_msh)  !.Input file name
  call fun_getarg (3,'-o',prblm%mshdata%fn_pst)  !.Output file name

  !
  fn_bin='domain_' !.binary file
  !.-----------------------------------------------------------------------------
  if ((myrank == 0).and.flg_read) then
    call file_name ('elvira_','.log',myrank,fname,m)
    open (unit=lu_log,file=fname(1:m),status='unknown',iostat=io_err)
    if (io_err > 0) call w_error(8, fname(1:m), 0)
    !. Reading mesh and step data -----------------------------------------------
    call start_timer (tcpu(1))
    call file_open   (lu_msh, prblm%mshdata%fn_msh)
    call get_meshdata(lu_msh, prblm%mshdata%fn_msh, prblm%mshdata)
    call get_stepdata(lu_msh, prblm%mshdata%fn_msh, prblm%stpdata,flg_fin)
    call close_unit  (lu_msh)
    call write_n_str(6, 80,'-'); write (*,20) elapsed_time(tcpu(1)); 
    call write_n_str(6, 80,'-'); write (*,10);
    !.---------------------------------------------------------------------------
    !.Graph construction [elem-nodos], [elem-elem] y [nodo-nodo]
    call start_timer  (tcpu(2))
    n_nod = get_nnod (prblm%mshdata%node); !.number of nodes 
    n_elm = get_nelm (prblm%mshdata%elem); !.number of elements
    call grafo_elmnode(n_nod, prblm%mshdata)
    call grafo_ElmElm (n_elm, prblm%mshdata)
    call grafo_NodNod (n_nod, prblm%mshdata)
    call write_n_str(6, 80,'-'); write (*,30) elapsed_time(tcpu(2)); 
    call write_n_str(6, 80,'-'); write (*,10); 
    !.-----------------------------------------------------------------------------
    !.Call to metis for domain partitioning
    call start_timer  (tcpu(3))
    call callmetis_partgraph(nproc, prblm%mshdata) !.partitioning elements and nodes
    call distribucion_NodInterface (nproc, n_nod, prblm%mshdata)
    !.
    call write_n_str(6, 80,'-'); write (*,40) elapsed_time(tcpu(3)); 
    call write_n_str(6, 80,'-'); write (*,10);
    !.-----------------------------------------------------------------------------
    !call start_timer  (tcpu(4))
    !call callmetis_fillreducing(mshdata)
    !call write_n_str(6, 80,'-'); write (*,50) elapsed_time(tcpu(4))
    !call write_n_str(6, 80,'-'); write (*,10)
    !.-----------------------------------------------------------------------------
    !call start_timer  (tcpu(5))
    !call abaqus_file(nproc, n_elm, n_nod,  prblm%mshdata,'abaqus_all.inp')
    !call write_n_str(6, 80,'-'); write (*,60) elapsed_time(tcpu(5)); 
    !call write_n_str(6, 80,'-'); write (*,10)
    !.-----------------------------------------------------------------------------
    !.Writing binary files
    call start_timer  (tcpu(6))
    call write_binfile( fn_bin, nproc, prblm%mshdata, prblm%stpdata)
    call write_n_str(6, 80,'-'); write (*,70) elapsed_time(tcpu(6))
    call write_n_str(6, 80,'-'); write (*,10) 
    !.-----------------------------------------------------------------------------
    !.PSBLAS - setting  
    ndof_g = n_nod; call allocate_slvr_vg (ndof_g, prblm%slvrset)
    call psbcdall_setting (prblm%mshdata, nproc, ndof_g, prblm%slvrset%vg)
    !.---------------------------------------------------------------------------
    !.freeing memoria
    call start_timer  (tcpu(7))
    call deallocate_mshdata  (prblm%mshdata)
    call deallocate_stepdata (prblm%stpdata)
    call write_n_str(6, 80,'-'); write (*,80) elapsed_time(tcpu(7)); 
    call write_n_str(6, 80,'-'); write (*,10)
    !.---------------------------------------------------------------------------
  end if
  !.
  call mpi_barrier(comm, ierr)
  !
  !.Recovering PSBLAS parameters
  call mpi_bcast(ndof_g, 1, mpi_integer, 0, comm, ierr)
  if (myrank /= 0) call allocate_slvr_vg (ndof_g, prblm%slvrset)
  call mpi_bcast(prblm%slvrset%vg(1:ndof_g), ndof_g, mpi_integer, 0, comm, ierr)
!  !.Reading binary files --------------------------------------------------------
  call start_timer  (tcpu(8))
  call a_unit(lu_m)   !.Unit number (file)
  call file_name (fn_bin,'.bin',myrank + 1,fname,m)
  open (unit=lu_m,file = fname(1:m),status='old',iostat=io_err,form='unformatted')
  if (io_err > 0) call w_error (8 ,fname(1:m),1)
  call bin_read_mshdata (lu_m, prblm%mshdata)
  call bin_read_stepdata(lu_m, prblm%stpdata)
  call close_unit (lu_m)
  call write_n_str(6, 80,'.'); write (*,110) myrank, elapsed_time(tcpu(8)); 
  call write_n_str(6, 80,'.'); write (*,10)
!  !.
!!!$  n_nod = get_nnod(prblm%mshdata%node); n_elm = get_nelm(prblm%mshdata%elem)
!!!$  call start_timer  (tcpu(9))
!!!$  call file_name ('abaqus_','.inp',myrank + 1,fname,m)
!!!$  call abaqus_file(nproc, n_elm, n_nod, prblm%mshdata,fname(1:m))
!!!$  call write_n_str(6, 80,'.'); write (*,120) myrank, elapsed_time(tcpu(9)); 
!!!$  call write_n_str(6, 80,'.'); write (*,10)
!!!$  !.
  call assign_dof_domain (myrank, prblm%mshdata,prblm%slvrset%sprs_m)
!!!$  call a_unit(lu_m)   !.Asignacion de numero de unidad 
!!!$  call file_name ('XXXX_','.dat',myrank + 1,fname,m)
!!!$  open (unit=lu_m,file = fname(1:m),status='unknown',iostat=io_err)
!!!$  call print_dof_domain(lu_m, myrank, prblm%slvrset%sprs_m)
!!!$  call close_unit (lu_m)
!  !.
!!!$  call a_unit(lu_m)   !.Asignacion de numero de unidad 
!!!$  call file_name ('VG_','.dat',myrank + 1,fname,m)
!!!$  open (unit=lu_m,file = fname(1:m),status='unknown',iostat=io_err)
!!!$  write (lu_m, 150) ndof_g, prblm%slvrset%vg(1:ndof_g)
!!!$  call close_unit (lu_m)
  return
10 format (/)
20 format ('###.Time for reading [meshdata-stepdata]          : ',F12.6,' [sec]')
30 format ('###.Time for graph construction E->N, E->E , N->N : ',F12.6,' [sec]')
40 format ('###.Time for Metis, nodes at interface            : ',F12.6,' [sec]')
50 format ('###.Time band width                               : ',F12.6,' [sec]')
60 format ('###.Time Abaqus file                              : ',F12.6,' [sec]')
70 format ('###.Time for writing binary files                 : ',F12.6,' [sec]')
80 format ('###.Time for freeing memory                       : ',F12.6,' [sec]')
100 format('###.Total time                                    : ',F12.6,' [sec]')
110 format('###.Process: ', I3,'  Time for reading [*.bin]    : ',F12.6,' [sec]')
120 format('###.Process: ', I3,'  Time for Abaqus file        : ',F12.6,' [sec]')
150 format('###.ndof_g : 'I8,/,20(:,1X,I5))
end subroutine problemsetting
! -------------------------------------------------------------------------------


end module mod_problemsetting
