! ------------------------------------------------------------------------------ 
module mod_filename
! ------------------------------------------------------------------------------ 
  use mod_precision; use mod_UnitUtil
  implicit none
  public  :: unitnumber ,FileNameRelPath
 ! private :: FileNameRelPath
! ------------------------------------------------------------------------------ 
contains
! ------------------------------------------------------------------------------ 
subroutine unitnumber (pathfn,strout,lu_m,io_err)
! ------------------------------------------------------------------------------
! Esta rutina
! Donde:
!       pathfn : path y nombre del archivo de "configuracion"
!       strout : cadena de caracteres en donde se va a buscar la subcadena FILE
!       lu_m   : numero de unidad asignada, si los datos se encuentran en otro 
!                archivo
!       io_err : si el archivo pudo abrirse sin problema io_err = 0, sino > 0
! ------------------------------------------------------------------------------
  implicit none
  character(len=*),intent(in)  :: pathfn,strout
  integer(ip), intent(inout)   :: lu_m
  integer(ip), intent(out)     :: io_err
  !.
  integer(ip)                  :: lu_aux,m
  character (len=len(pathfn))  :: arch_n

  io_err = 0
  if (index(strout,'FILE') > 0) then        
    arch_n = FileNameRelPath (pathfn,strout); !.Recupera el Nombre del archivo
    m=len_trim(arch_n) 
    call a_unit(lu_aux)   !.Asignacion de numero de unidad 
    !.  
    open (unit=lu_aux,file=arch_n(1:m),status='old',iostat=io_err)
    !.
    lu_m = lu_aux
  end if
  !.
  return
end subroutine unitnumber
! ------------------------------------------------------------------------------ 
function FileNameRelPath(pathfn,strout) result(arch_n)
! ------------------------------------------------------------------------------
! Esta funcion recupera el Path de la cadena pathfn  
  implicit none
  character (len=*), intent(in) :: pathfn,strout
  integer(ip)                   :: jf,ki,kf,m
  character (len=len(pathfn))   :: arch_n


  jf=scan(pathfn,'/',back=.true.)
  ki=scan(strout,'"')+1; kf=scan(strout,'"',back=.true.)-1
  m=jf+(kf-ki)+1
  arch_n = pathfn(1:jf)//strout(ki:kf)
  return
end function FileNameRelPath
! ------------------------------------------------------------------------------ 
function FileNameRelPath_WR (string, s_str, pathfn) result(arch_n)
! ------------------------------------------------------------------------------
! Esta funcion recupera el Path de la cadena pathfn
! ------------------------------------------------------------------------------  
  implicit none
  character (len=*), intent(in) :: string,s_str,pathfn
  integer(ip)                   :: l1, pi, jf, ki, kf
  character (len=len(pathfn))   :: arch_n


  l1 = len_trim(string)
  !.
  pi = index(string, s_str)  !.FILE_R    FILE_W
  !.
  ki = pi +  scan(string(pi:l1),'"')    ; !print *, 'ki:', ki
  kf = ki +  scan(string(ki:l1),'"') - 2; !print *, 'kf:', kf 
  !.
  jf=scan(pathfn,'/',back=.true.)
  !.
  arch_n = pathfn(1:jf)//string(ki:kf)
  !.
  return
end function FileNameRelPath_WR

! ------------------------------------------------------------------------------ 
end module mod_filename
! ------------------------------------------------------------------------------ 
