!  -----------------------------------------------------------------------------
module mod_error
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_UnitUtil
  use mod_busqueda
  !.
  implicit none
  !.

  !.
  type t_cerr
    integer(4)             :: cd_e !.Numero del error
    character(len=80)      :: tx_e !.Error
  end type t_cerr

  include 'errores.inc'

  integer(ip),   private                :: lu_er

!  -----------------------------------------------------------------------------
  interface w_error !.Generic name for interface
    module procedure  w_error_c
    module procedure  w_error_i
    module procedure  w_error_r
  end interface
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine detener
!  -----------------------------------------------------------------------------
  implicit none

  write (std_o, 30)
  write (std_o, 10) '>>>>>>>>>>.ERROR, READ FILE elvira_0.log <<<<<<<<<<<<'
  write (std_o, 30)
  write (std_o, '(A256)') '  '

  write (lu_log, 20)
  write (lu_log, 10) '!>*>*>*>.ERROR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!'
  write (lu_log, 10) '!>*>*>*>*>*>*>.ERROR <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<!'
  write (lu_log, 10) '!>*>*>*>*>*>*>*>*>*>.ERROR <<<<<<<<<<<<<<<<<<<<<<<<<!'
  write (lu_log, 10) '!>*>*>*>*>*>*>*>*>*>*>*>*>.ERROR <<<<<<<<<<<<<<<<<<<!'
  write (lu_log, 20)
  call closeall_unit; stop 
  return
10 format(A)
20 format ('!****************************************************!')
30 format ('------------------------------------------------------')
end subroutine detener
!  -----------------------------------------------------------------------------
subroutine read_error_file
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip)         :: j, io_err, i_err
  
!  call a_unit(lu_er)
!  open (unit=lu_er,file='../conf/errores.conf',status='old',iostat=io_err)
  !.
!  if ( io_err > 0 ) then
!    write (lu_log,'(A,I3)')'###.ERROR ABRIENDO ARCHIVO: errores.conf; UNIDAD:',lu_er
!    call detener
!  end if
  !.
!  read (lu_er,*)  error%n_e
!  allocate(error%c_err(error%n_e), error%ind_e(error%n_e), stat=i_err)
  !.
!  if (i_err > 0 ) then
!    write(lu_log,'(A)')'###.ERROR ASIGNANDO MEMORIA EN read_error_file'
!    call detener
!  end if
  !.
!  read (lu_er,'(I4,A80)') (error%c_err(j),j=1,error%n_e)
  !.
  call quick_sort(n_e,c_err(:)%cd_e,ind_e)
!
!  call close_unit (lu_er)
!
  return
end subroutine read_error_file
!  -----------------------------------------------------------------------------
subroutine  w_error_i (ide,cir,istop)
!  -----------------------------------------------------------------------------
!  Where:
!        ide     : error code
!        cir     : value written in the logic unit besides the error message
!                  hoping it helps finding the error!!!!
!        istop   : = 1 stop the program
!  -----------------------------------------------------------------------------
  implicit none
  integer(4),intent(in)  :: ide
  integer(ip),intent(in) :: cir,istop
  integer(ip)            :: pos
  logical                :: fou

!  if (.not. allocated(error%c_err)) call read_error_file
!
  call b_search (c_err(:)%cd_e,ind_e,n_e,ide,pos,fou)
!
  if (fou) then
    write (lu_log,'(A80,I8)')    c_err(pos)%tx_e,cir
  else
      write (lu_log,10) '###.ERROR CODE DOES NOT EXIST:',ide,' NOT DEFINED'
  endif
  if (istop == 1) call detener 
  return
10 format (A,2X,I6,A)
end subroutine w_error_i
!  -----------------------------------------------------------------------------
subroutine  w_error_r (ide,cir,istop)
!  -----------------------------------------------------------------------------
!  Where:
!        ide     : error code
!        cir     : value written in the logic unit besides the error message
!                  hoping it helps finding the error!!!!
!        istop   : = 1 stop the program
!  -----------------------------------------------------------------------------
  implicit none
  integer(4),intent(in)  :: ide
  integer(ip),intent(in) :: istop
  real(rp),   intent(in) :: cir
  integer(ip)            :: pos
  logical                :: fou
  
!  if (.not. allocated(error%c_err)) call read_error_file
!
  call b_search (c_err(:)%cd_e,ind_e,n_e,ide,pos,fou)
!
  if (fou) then
    write (lu_log,'(A80,f16.6)') c_err(pos)%tx_e,cir
  else
    write (lu_log,10) '###.ERROR CODE DOES NOT EXIST:',ide,' NOT DEFINED'
  endif
  if (istop == 1) call detener
  return
10 format (A,2X,I6,A)
end subroutine w_error_r
!  -----------------------------------------------------------------------------
subroutine  w_error_c (ide,cir,istop)
!  -----------------------------------------------------------------------------
!  Where:
!        ide     : error code
!        cir     : value written in the logic unit besides the error message
!                  hoping it helps finding the error!!!!
!        istop   : = 1 stop the program
!  -----------------------------------------------------------------------------
  implicit none
  integer(4),intent(in)  :: ide
  integer(ip),intent(in) :: istop
  character (len=*),intent(in) :: cir
  integer(ip)                  :: pos
  logical                      :: fou

!  if (.not. allocated(error%c_err)) call read_error_file
!
  call b_search (c_err(:)%cd_e,ind_e,n_e,ide,pos,fou)
!
  if (fou) then
    write (lu_log,'(A80,A)')  c_err(pos)%tx_e,cir
  else
    write (lu_log,10) '###.ERROR CODE DOES NOT EXIST:',ide,' NOT DEFINED'
  endif
  if (istop == 1) call detener
  return
10 format (A,2X,I6,A)
end subroutine w_error_c
!  -----------------------------------------------------------------------------
subroutine write_n_str(lu_o, n, str)
!  -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: lu_o, n
  character (len=1)       :: str
  !.
  integer(ip)             :: i

  do i=1,n-1
    write (lu_o,advance='no',fmt='(A1)') str
  end do
  write (lu_o,advance='yes',fmt='(A1)') str
  return
10 format (A1)
end subroutine write_n_str
end module mod_error
!  -----------------------------------------------------------------------------


