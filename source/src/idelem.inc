!.m_elmt: Total number of defined finite elements
! --------------------- Element specific data  ---------------------------- !
!   name: element name (max 12 characters)                                  !
!   type: element number                                                    !
!   ndim: element dimension                                                 !
!   ngdl: number of degrees of freedom per node                             !
!   nlib: number of degrees of freedom of the element                       !
!   n_pe: number of nodes per element                                       !
!   n_cv: number of directions of the element volumentric load              !
!   n_te: number of stress components per integration point or node         !
!   n_qp: number of Gauss points per edge of the element                    !
!   n_qp: number of Gauss points per edge of the elemento                   !
!   ntqp: total number of Gauss points per element                          !
!   prme: estra parameter to pass information                               !
! ------------------------------------------------------------------------- !
  integer(ip), parameter, private    :: m_elmt = 43
  type(t_idel),private               :: id_elm(1:m_elmt) = (/     &
! ---------- ELEMENTS FOR PROBLEMS OF A SCALAR VARIABLE ------------------- !
! --------------------- ELEMENTS 1D --------------------------------------- !
!        name          type  ndim  ngdl  nlib  n_pe  n_cv  n_te  n_qp  ntqp  prme
! ------ LINEAS
t_idel('HT1DL02',        1,    1,    1,    2,    2,    1,    1,    2,    2,    1), &
t_idel('HT1DL03',        2,    1,    1,    3,    3,    1,    1,    2,    2,    1), &
! -------------------- ELEMENTS 2D --------------------------------------- !
! ------ TRIANGLES
t_idel('HT2DT03',        3,    2,    1,    3,    3,    1,    2,    0,    1,    1), &
t_idel('HT2DT06',        4,    2,    1,    6,    6,    1,    2,    0,    6,    1), &
! ------ QUADRILATERALS
t_idel('HT2DQ04',        5,    2,    1,    4,    4,    1,    2,    2,    4,    1), &
t_idel('HT2DQ08',        6,    2,    1,    8,    8,    1,    2,    3,    3,    1), &
! -------------------- ELEMENTS 3D --------------------------------------- !
! ------ TETRAHEDRALS
t_idel('HT3DT04',        7,    3,    1,    4,    4,    1,    3,    0,    4,    1), &
t_idel('HT3DT10',        8,    3,    1,   10,   10,    1,    3,    0,    4,    1), &
! ------ HEXAEDRALS
t_idel('HT3DH08',        9,    3,    1,    8,    8,    1,    3,    2,    8,    1), &
t_idel('HT3DH20',       10,    3,    1,   20,   20,    1,    3,    3,   27,    1), &
! -------------------- ELEMENTS 1D WITH BUBBLE --------------------------- !
t_idel('HT1DL02B',      15,    1,    1,    3,    2,    1,    1,    3,    3,    1), &
! -------------------- ELEMENTS 2D WITH BUBBLE --------------------------- !
t_idel('HT2DT03B',      16,    2,    1,    4,    3,    1,    2,    0,    3,    1), &
t_idel('HT2DQ04B',      17,    2,    1,    5,    4,    1,    2,    3,    9,    1), &
! -------------------- ELEMENTS 3D WITH BUBBLE --------------------------- !
t_idel('HT3DT04B',      18,    3,    1,    5,    4,    1,    3,    0,    5,    1), &
t_idel('HT3DH08B',      19,    3,    1,    9,    8,    1,    3,    3,   27,    1), &
! -------------- MACRO ELEMENTS FOR ELECTROFISIOLOGY --------------------- !
! Macro elements have inner element data that will be generated during the assembly
! and that will be updated during the computations. The number of inner data points
! are given by prme.
! -------------------- ELEMENTS 1D --------------------------------------- !
!        name          type  ndim  ngdl  nlib  n_pe  n_cv  n_te  n_qp  ntqp  prme
t_idel('ML02HT1DL02',  020,    1,    1,    3,    2,    1,    1,    2,    2,    2), &
t_idel('ML03HT1DL02',  021,    1,    1,    4,    2,    1,    1,    2,    2,    3), &
t_idel('ML02HT1DL02B', 022,    1,    1,    5,    2,    1,    1,    3,    3,    2), &
! -------------------- ELEMENTS 2D --------------------------------------- !
t_idel('MT03HT2DT03',  023,    2,    1,    4,    3,    1,    1,    0,    1,    3), &
t_idel('MT03HT2DT03B', 024,    2,    1,    7,    3,    1,    1,    0,    1,    3), &
t_idel('MQ04HT2DT03',  025,    2,    1,    5,    4,    1,    1,    0,    1,    4), &
t_idel('MQ04HT2DT03B', 026,    2,    1,    9,    4,    1,    1,    0,    3,    4), &
t_idel('MQ06HT2DT03',  027,    2,    1,    6,    4,    1,    1,    0,    2,    6), &
t_idel('MQ06HT2DT03B', 028,    2,    1,   12,    4,    1,    1,    0,    2,    6), &
t_idel('MQ08HT2DT03',  029,    2,    1,    7,    4,    1,    1,    0,    2,    8), &
t_idel('MQ08HT2DT03B', 030,    2,    1,   15,    4,    1,    1,    0,    2,    8), &
t_idel('MQ05HT2DQ04',  031,    2,    1,    8,    4,    1,    1,    2,    2,    5), &
t_idel('MQ05HT2DQ04B', 032,    2,    1,   13,    4,    1,    1,    2,    2,    5), &
t_idel('MQ09HT2DQ04',  033,    2,    1,   12,    4,    1,    1,    2,    2,    9), &
t_idel('MQ09HT2DQ04B', 034,    2,    1,   21,    4,    1,    1,    2,    2,    9), &
! -------------------- ELEMENTS 3D --------------------------------------  !
t_idel('MT04HT3DT04',  035,    3,    1,    5,    4,    1,    1,    0,    2,    4), &
t_idel('MT04HT3DT04B', 036,    3,    1,    9,    4,    1,    1,    0,    2,    4), &
t_idel('MH07HT3DH08',  037,    3,    1,   16,    8,    1,    1,    2,    2,    7), &
t_idel('MH07HT3DH08B', 038,    3,    1,   23,    8,    1,    1,    2,    2,    7), &
t_idel('MH13HT3DH08',  039,    3,    1,   27,    8,    1,    1,    2,    2,   13), &
t_idel('MH13HT3DH08B', 040,    3,    1,   40,    8,    1,    1,    3,   12,   13), & 
! -------------------- ELEMENTS 1D IN 3D SPACE  -------------------------- ! 
t_idel('HT3DL02',      061,    3,    1,    2,    2,    1,    1,    2,    2,    1), &
t_idel('HT3DL03',      062,    3,    1,    3,    3,    1,    1,    2,    2,    1), &
! -------------------- ELEMENTO 1D WITH BUBBLE IN 3D SPACE --------------- !
t_idel('HT3DL02B',     063,    3,    1,    3,    2,    1,    1,    3,    3,    1), &
! ------------- MACRO ELEMENTS 1D IN 3D SPACE ---------------------------- !
t_idel('ML02HT3DL02',  064,    3,    1,    3,    2,    1,    1,    2,    2,    2), &
t_idel('ML03HT3DL02',  065,    3,    1,    4,    2,    1,    1,    2,    2,    3), &
t_idel('ML02HT3DL02B', 066,    3,    1,    5,    2,    1,    1,    3,    3,    2), &
! ------------------- CONNECTING ELEMENT 0 D -------------------------------
t_idel('HT0DC',        070,    0,    1,    2,    2,    0,    0,    0,    0,    0)/)     
