!     Last change:  EAH  25 Mar 2007    9:51 pm
! -------------------------------------------------------------------------------
module mod_monodominio
! -------------------------------------------------------------------------------
  use mod_precision
  use mod_timer
  use mod_problemsetting
  use mod_com_EM_nd
  use mod_assembly 
  use mod_USER_monodomain
  use mod_psblas_solver 
  use modPostProceso
  use mod_savedata_ecg
  use mod_mpisndrcv
  use mod_sndrcv_xg
  use mod_write_restart
  use savestorCurrent
  !.
  implicit none
! -------------------------------------------------------------------------------
contains
! -------------------------------------------------------------------------------
subroutine monodominio(comm, prblm)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: comm
  type(t_prblm)                :: prblm
  type(t_sdrc), save           :: sndrcv
  !.
  type(timer)                  :: tim_q, tim_s, tim_b1, tim_b2, tim_d, tim_p, tim_e
  real(rp)                     :: sm_tq, sm_ts, sm_tb1, sm_tb2, sm_td, sm_tp, sm_tot
  real(rp)                     :: dt, tmax, t, i_dt
  integer(ip)                  :: ni_tmp, it_exq, i_sol, iopt, n_nod, n_elm
  integer(ip)                  :: tipo, n_pe, nlib, nin,  tipo_p, ite
  integer(ip)                  :: lu_t, lu_c
  integer(ip)                  :: myrank, ierr, nproc, ndof_l, ndof_g
  character (len=256)          :: fname
  integer(ip)                  :: m, flg_bin, frc_bin, flg_ndout
  integer(ip)                  :: t_str, n_str
  logical                      :: flg_ei

  call mpi_comm_size (comm, nproc, ierr)
  call mpi_comm_rank (comm, myrank, ierr)
  ! -----------------------------------------------------------------------------
  ! dt    : time step                    | tmax  : simulation time
  !.ni_tmp: number of time increments    | i_dt  : 1/dt
  call get_stpdata_time_inc (prblm%stpdata%timeinc, dtini = dt, tmax = tmax  ) !.Time step 
  i_dt  = 1.0_rp/dt;  ni_tmp=int(tmax/dt);   t = 0.0_rp; 
  ! -----------------------------------------------------------------------------
  it_exq = get_int_temp  (prblm%stpdata)       !.Time integraton scheme
  i_sol  = get_solvertype(prblm%stpdata)       !.Solver to be used
  tipo_p = prblm%mshdata%atype                 !.problem type
  !.
  iopt = optTemporal(it_exq)             !.Time optimization flag
  !.
  n_nod = get_nnod (prblm%mshdata%node)  !.number of nodes 
  n_elm = get_nelm (prblm%mshdata%elem)  !.number of elements
  !.
  tipo  = get_elemtipo(1,prblm%mshdata%elem)      !.element type
  call id_elem_tip (tipo, n_pe=n_pe, nlib = nlib) !.numb of nodes per elm and dof per elm
  nin = nlib - n_pe                               !.number of internal nodes
  !.nin = 0 => linear element
  !.nin = 1 => element with bubble
  !.nin > 1 => macro element
  !.
  !- Allocates memory of required variables for calculations -------------------- 
  !.
  flg_ei = .false.; if ((it_exq == 0).or.(it_exq == 1)) flg_ei = .true.
  call allocatememory (flg_ei, n_nod, n_elm, nlib, n_pe)
  !.
  !- Initializing potential according to electric model -------------------------
  !.
  call assigninitialconditions(n_nod, n_elm, nin, prblm%mshdata, vnd, velm)
  !.
  !- Initializing electric model -----------------------------------------------
  !.
  call Inic_me_nd (prblm%mshdata, prblm%stpdata)
  if (nin > 0) call Inic_me_elm (iopt, nin, prblm%mshdata, prblm%stpdata)
  !.
  !- Calculating/Assembling stiffness matrix -----------------------------------
  !.
  call start_timer  (tim_e)
  call assemblymatrix (userelem_monodomain, prblm)
!  call write_n_str(6, 80,'&'); 
  write (*,10) myrank, elapsed_time(tim_e); 
!  call write_n_str(6, 80,'&');
  !.
  !.---------------- Initial Files for Postprocessing --------------------------
  call a_unit(lu_t)
  call file_name ('times_','.dat',myrank + 1,fname,m)
  open (unit=lu_t,file = fname(1:m),status='unknown')!,iostat=io_err)
  !.
  ! ------------ Reading restart file ------------------------------------------
  call get_t_rstrt(prblm%stpdata%rstrt, t_str, n_str) !.restart type, and No of files to write
  !.
  if (t_str > -1 ) call ReadReStartVar (t_str, myrank, nin, vnd, velm, prblm)
  ! -----------------------------------------------------------------------------
  !.
  ! -----------------------------------------------------------------------------
  !. Initializing times
  sm_tq = 0.0; sm_ts = 0.0;  sm_tb1= 0.0; sm_tb2= 0.0; sm_td= 0.0; sm_tp= 0.0
  !.
  call PostProceso (myrank, 0, prblm, vnd%Vn_n, vnd%Qi_n, t)
  !.
  call get_output_var ( 2, prblm%stpdata%g_out, flg_bin, frc_bin)
  !.
  if (flg_bin == 1) call save_pot_ecg (prblm%mshdata%fn_pst, frc_bin, myrank, 0,  &
                                       prblm%mshdata%node, t, vnd%Vn_n)
  !.
  flg_ndout = get_ndout_flag(prblm%stpdata%nd_out)
  if(flg_ndout.eq.1) then
    call setcurrentpost (myrank, 0, prblm)
    call a_unit(lu_c); !.Unit for writing currents
  end if
  t=dt; ite=1
  !.
  call mpi_barrier(comm, ierr)
  !.
  ndof_l = get_ndof_l_sprs_m( prblm%slvrset%sprs_m )
  !.
  ndof_g = prblm%slvrset%ndof_g 
  !.
! -------------------------------------------------------------------------------
  if (nin > 0) then
    !.Communication structure for right hand side
    call dof_sndrcv ( ndof_g,   comm, myrank, nproc,  prblm%mshdata%node, sndrcv)
    !.
    if (flg_ei) then
      !.Filling terms in mass matrix
      call mm_completing( myrank, comm,  n_nod, sndrcv, prblm%slvrset%sprs_m)
      write (lu_t,20) '%Iteration','t (Simulation)',' Ionic Model  ','Linear System ',&
                      '   RHS    ','Vm time deriv.','  Postprocess '
      write (*,50) 'Explicit'
      
      do while ( ite <= ni_tmp )
        !.Calculate Potential and Iion in the node and in the macro element-
        call start_timer   (tim_q)
        call eq_unicel_nd  (myrank,lu_c, iopt,ite,t,dt,vnd%Vo_n,vnd%Vp_n,vnd%Qi_n,prblm%mshdata,prblm%stpdata)
        call eq_unicel_elm (myrank,iopt,ite,nin,prblm%mshdata,prblm%stpdata,t,dt,velm%Vo_e,velm%Vp_e,velm%Qi_e)
        sm_tq = sm_tq + elapsed_time(tim_q); !.Cummulated time for Jion calculation
        !.
        !.----Right hand side ----------------------------------------------------
!        write (200+myrank,'(10(1X,F12.6))') velm%Vn_e 
        call start_timer (tim_b1)
        !    Rhs_Bubble_exp (n_elm,              elem,    Vo_n,    Vn_n,     Vo_e,     Vn_e,    Rhs)
        call Rhs_Bubble_exp (n_elm,prblm%mshdata%elem,vnd%Vo_n,vnd%Vn_n,velm%Vo_e,velm%Vn_e,vnd%Rhs)
        call rhs_sndrcv (myrank, comm, vnd%Rhs, sndrcv)
        sm_tb1= sm_tb1 + elapsed_time(tim_b1) !.Cummulated time for RHS 
        !.
!        write (100+myrank,'(10(1X,F12.6))') vnd%Rhs
        !.----Solving the system --------------------------------------------------
        call start_timer (tim_s)
        vnd%Vn_n = implicit_rhs ( prblm%slvrset%sprs_m, vnd%Rhs )
        sm_ts = sm_ts + elapsed_time(tim_s) !.Cummulated time for solver
!        write (500+myrank,'(10(1X,F9.3))') vnd%Vn_n
        !.
        !.----Potential time derivative update ------------------------------------
        call start_timer (tim_d)
        call DerTempReasig_Node(n_nod,i_dt,vnd%Vo_n,vnd%Vn_n,vnd%Va_n,vnd%Vp_n)
        call DerTempReasig_Elm (nin,n_elm,i_dt,velm%Vo_e,velm%Vn_e,velm%Va_e,velm%Vp_e)
        sm_td = sm_td + elapsed_time(tim_d) !. Cummulated time for Vm time derivative
        !.
        !.----Postprocessing -----------------------------------------------------
        call start_timer (tim_p)
        !.----Writing postprocessing variables for nodes --------------------------
        call PostProceso (myrank, ite, prblm, vnd%Vn_n, vnd%Qi_n, t)
        !.---- Saving data of the entire domain for postprocessing ----------------
        if (flg_bin == 1) call save_pot_ecg (prblm%mshdata%fn_pst, frc_bin, myrank, &
             ite, prblm%mshdata%node, t, vnd%Vn_n)
        !.---- Writing restart file -----------------------------------------------
        if (t_str > 0) call WriteReStartVar (t_str,n_str,myrank,ite,nin,vnd,velm,prblm)
        sm_tp= sm_tp + elapsed_time(tim_p)  !.Cummulated time for postprocessing
        !.
        !.-------------------------------------------------------------------------
        !.
        if (mod(ite,500) == 0)  then
          write (lu_t,30) ite,t,sm_tq, sm_ts, sm_tb1, sm_td, sm_tp
          if (myrank == 0 ) then
            sm_tot= sm_tq + sm_ts + sm_tb1 + sm_tb2 + sm_td + sm_tp
            write (*,   40) ite, sm_tq, sm_ts, sm_tot
          end if
        end if
        !.
        write(*,70) t,ite
        !.
        !.---- Increments --------------------------------------------------------
        t= t + dt; ite = ite + 1
      end do
    else
    !.Call and setting of psblas solver  
      call psblas_solver (1, i_sol, prblm%slvrset, prblm%mshdata%node, vnd%Rhs,vnd%Rhs)
      !.
      write (lu_t,20) '%Iteration','t (Simulation)',' Ionic Model  ','Linear System ',&
                      '   RHS    ','Add Elem Oper ','Vm time deriv.','  Postprocess '
      !.
      write (*,50) 'Implicit'
      do while ( ite <= ni_tmp )
        !.Calculate Potential and Iion in the node and in the macro element-
        call start_timer   (tim_q)
        call eq_unicel_nd  (myrank,lu_c, iopt,ite,t,dt,vnd%Vo_n,vnd%Vp_n,vnd%Qi_n,prblm%mshdata,prblm%stpdata)
        call eq_unicel_elm (myrank,iopt,ite,nin,prblm%mshdata,prblm%stpdata,t,dt,velm%Vo_e,velm%Vp_e,velm%Qi_e)
        sm_tq = sm_tq + elapsed_time(tim_q) !.Cummulated time for Jion calculation 
        !.
        !.----Right hand side ----------------------------------------------------
        call start_timer (tim_b1)
        !.call Rhs_Bubble (n_elm, prblm%mshdata%elem, vnd%Vo_n, velm%Vo_e, vnd%Rhs)
        call Rhs_Bubble02 (n_elm, prblm%mshdata%elem, vnd%Vo_n, velm%Vo_e, vnd%Rhs)
        call rhs_sndrcv (myrank, comm, vnd%Rhs, sndrcv)
        sm_tb1= sm_tb1 + elapsed_time(tim_b1) !.Cummulated time for element operations
        !.
        !.----Solving the system ------------------------------------------------
        call start_timer (tim_s)
        call psblas_solver (2, i_sol, prblm%slvrset, prblm%mshdata%node, vnd%Rhs,vnd%Vn_n)
        sm_ts = sm_ts + elapsed_time(tim_s) !.Cummulated time for solver
        !.
        !.----Calculating potential in the interior of elements -----------------
        call start_timer (tim_b2)
        !call Vn_Elem_Bubble (n_elm, prblm%mshdata%elem, vnd%Vn_n, velm%Vn_e)
        call Vn_Elem_Bubble02 (n_elm, prblm%mshdata%elem, vnd%Vn_n, velm%Vn_e)
        sm_tb2= sm_tb2 + elapsed_time(tim_b2) !.Cummulated time for elem operation (Vm)
        !.
        !.----Potential time derivative update -----------------------------------
        call start_timer (tim_d)
        call DerTempReasig_Node(n_nod,i_dt,vnd%Vo_n,vnd%Vn_n,vnd%Va_n,vnd%Vp_n)
        call DerTempReasig_Elm (nin,n_elm,i_dt,velm%Vo_e,velm%Vn_e,velm%Va_e,velm%Vp_e)
        sm_td = sm_td + elapsed_time(tim_d) !. Cummulated time for Vm time derivative
        !.
        !.----Postprocessing -----------------------------------------------------
        call start_timer (tim_p)
        !.----Writing postprocessing variables for nodes -------------------------
        call PostProceso (myrank, ite, prblm, vnd%Vn_n, vnd%Qi_n, t)
        !.---- Saving data of the entire domain for postprocessing ----------------
        if (flg_bin == 1) call save_pot_ecg (prblm%mshdata%fn_pst, frc_bin, myrank, &
             ite, prblm%mshdata%node, t, vnd%Vn_n)
        !.---- Writing restart file  ---------------------------------------------
        if (t_str > 0) call WriteReStartVar (t_str,n_str,myrank,ite,nin,vnd,velm,prblm)
        sm_tp= sm_tp + elapsed_time(tim_p)  !.Cummulated time for postprocessing
        !.
        !.-------------------------------------------------------------------------
        !.
        if (mod(ite,500) == 0)  then
          write (lu_t,30) ite,t,sm_tq, sm_ts, sm_tb1, sm_tb2, sm_td, sm_tp
          if (myrank == 0 ) then
            sm_tot= sm_tq + sm_ts + sm_tb1 + sm_tb2 + sm_td + sm_tp
            write (*,   40) ite, sm_tq, sm_ts, sm_tot
          end if
        end if
        !.
        !.
        !.---- Increments --------------------------------------------------------
        t= t + dt; ite = ite + 1
      end do
    end if
  else
    select case (it_exq)  !.NOT READY YET --------------------------------------

    case(0:1) !.Explicit -------------------------------------------------------
      if (it_exq.eq.0) then 
          write(*,*) 'Solving explicit scheme with constant time step. Standard Element'
      else 
          write(*,*) 'Solving explicit scheme with adaptive time step. Standard Element'
      end if
      call calculo_sprs_kr_exp (dt, prblm%slvrset%sprs_m)
      !.
      write (lu_t,20) '%Iteration','t (Simulation)',' Ionic Model  ','Linear System ',&
                      'Vm time deriv.','  Postprocess '
      do while ( ite <= ni_tmp )
        !.Calculate Potential and Iion in the node  ----------------------------
        call start_timer  (tim_q)
        call eq_unicel_nd  (myrank,lu_c, iopt,ite,t,dt,vnd%Vo_n,vnd%Vp_n,vnd%Qi_n,prblm%mshdata,prblm%stpdata)
        sm_tq = sm_tq + elapsed_time(tim_q); !.Cummulated time for Jion calculation 
        !.
        !.----Calculate Potential in the node ----------------------------------
        call start_timer (tim_s)
        vnd%Vn_n = calculo_vn_exp (prblm%slvrset%sprs_m, vnd%Vo_n, vnd%Va_n)
        sm_ts = sm_ts + elapsed_time(tim_s) !.Cummulated time for solver
        !.
        !.---- Postprocessing -----------------------------------------------------
        call start_timer (tim_p)
        !.----Writing postprocessing variables for nodes----------------------------
        call PostProceso (myrank, ite, prblm, vnd%Vn_n, vnd%Qi_n, t)
        !.---- Saving data of the entire domain for postprocessing ---------------
        if (flg_bin == 1) call save_pot_ecg (prblm%mshdata%fn_pst, frc_bin, myrank, &
                                           ite, prblm%mshdata%node, t, vnd%Vn_n)
        !.---- Writing restart file -----------------------------------------------
        if (t_str > 0) call WriteReStartVar (t_str,n_str,myrank,ite,nin,vnd,velm,prblm)
        sm_tp= sm_tp + elapsed_time(tim_p)  !.Cummulated time for postprocessing
        !.
        !.--------- Potential time derivative update -------------------------------
        call start_timer (tim_d)
        call DerTempReasig_Node(n_nod,i_dt,vnd%Vo_n,vnd%Vn_n,vnd%Va_n,vnd%Vp_n)
        sm_td = sm_td + elapsed_time(tim_d) !. Cummulated time for Vm time derivative
        !.-------------------------------------------------------------------------
        if (mod(ite,500) == 0)  then
          write (lu_t,30) ite,t,sm_tq, sm_ts, sm_td, sm_tp
          if (myrank == 0 ) then
            sm_tot= sm_tq + sm_ts + sm_td + sm_tp
            write (*,   40) ite, sm_tq, sm_ts, sm_tot
          end if
        end if
        !.---- Increments ------------------------------------------------------
        t= t + dt; ite = ite + 1
      end do
    case (2:3) !.Implicit ------>> Full ----------------------------------------
      !.
      !.Diagonal mass matrix
      if (it_exq.eq.2) then 
          write(*,*) 'Solving implicit scheme with constant time step. Standard Element'
      else 
          write(*,*) 'Solving implicit scheme with adaptive time step. Standard Element'
      end if
      call mm_plus_dt_x_kr (dt, prblm%slvrset%sprs_m)
      !.
      call dof_sndrcv ( ndof_g,   comm, myrank, nproc,  prblm%mshdata%node, sndrcv)
      call mm_completing( myrank, comm,  n_nod, sndrcv, prblm%slvrset%sprs_m)
      !.
      call psblas_solver (1, i_sol, prblm%slvrset,prblm%mshdata%node, vnd%Rhs,vnd%Rhs)
      !. 
      write (lu_t,20) '%Iteration','t (Simulation)',' Ionic Model  ','Linear System ',&
                      '   RHS    ','Vm time deriv.','  Postprocess '
      !.
      do while ( ite <= ni_tmp )
        !.Calculate Potential and Iion in the node
        call start_timer  (tim_q)
        call eq_unicel_nd  (myrank,lu_c, iopt,ite,t,dt,vnd%Vo_n,vnd%Vp_n,vnd%Qi_n,prblm%mshdata,prblm%stpdata)
        sm_tq = sm_tq + elapsed_time(tim_q); !.Cummulated time for Jion calculation 
        !.
        !.-Calculating right hand side for diagonal mass matrix or consistent
        call start_timer (tim_b1)
        vnd%rhs = implicit_rhs ( prblm%slvrset%sprs_m, vnd%Vo_n)
        sm_tb1 = sm_tb1 + elapsed_time(tim_b1) !.Cummulated time for RHS
        !.
        !.---- Solving the system --------------------------------------------
        call start_timer (tim_s)
        call psblas_solver (2, i_sol, prblm%slvrset,prblm%mshdata%node, vnd%Rhs,vnd%Vn_n)
        sm_ts = sm_ts + elapsed_time(tim_s) !.Cummulated time for solver
        !.
        !.--- Postprocess ------------------------------------------------------
        call start_timer (tim_p)
        !.----Writing postprocessing variables for nodes----------------------------
        call PostProceso (myrank, ite, prblm, vnd%Vn_n, vnd%Qi_n, t)
        !.---- Saving data of the entire domain for postprocessing ---------------
        if (flg_bin == 1) call save_pot_ecg (prblm%mshdata%fn_pst, frc_bin, myrank, &
                                           ite, prblm%mshdata%node, t, vnd%Vn_n)
        !.---- Writing restart files ------------------------------------------
        if (t_str > 0) call WriteReStartVar (t_str,n_str,myrank,ite,nin,vnd,velm,prblm)
        sm_tp= sm_tp + elapsed_time(tim_p)  !.Cummulated time for postprocessing
        !.      
        !.----Potential time derivative updates -------------------------------
        call start_timer (tim_d)
        call DerTempReasig_Node(n_nod,i_dt,vnd%Vo_n,vnd%Vn_n,vnd%Va_n,vnd%Vp_n)
        sm_td = sm_td +  elapsed_time(tim_d)!. Cummulated time for Vm time derivative
        !.
        !.
        if (mod(ite,500) == 0)  then
          write (lu_t,30) ite,t,sm_tq, sm_ts, sm_td, sm_tp
          if (myrank == 0 ) then
            sm_tot= sm_tq + sm_ts + sm_td + sm_tp
            write (*,   40) ite, sm_tq, sm_ts, sm_tot
          end if
        end if
        !.
        !.---- Increments ------------------------------------------------------
        t=t+dt; ite=ite+1
      end do
    case default
     ! write (*,999); stop
    end select
   end if
   ! -------------------------------------------------------------------------------
   call psblas_solver (5,i_sol,prblm%slvrset,prblm%mshdata%node, vnd%Rhs,vnd%Vn_n)
   ! -------------------------------------------------------------------------------
  call closeall_unit
 return
10 format('###.MYRANK: ',I4,' Assembling time:' G14.6)
20 format(A10,7(1X,A14))
30 format(I10,7(1X,G14.6))
40 format('###.ITE: ',I10,' JION : ',G14.6,' SOLVER: ',G14.6,' TOTAL: ',G14.6,' [sec]')
50 format('###.Time Integration Scheme:',A,' with Macroelements')
60 format('###.Time Integration Scheme:',A,' with Linear Elements')
70 format('SIM TIME: ',G14.6,' ITERATION: ',I10)
end subroutine Monodominio
! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
function optTemporal(jopt) result(iopt)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: jopt
  integer(ip)              :: iopt

  if ((jopt == 1).or.(jopt == 3).or.(jopt == 5)) then
    iopt = 1
  else
    iopt = 0
  end if
  return
end function optTemporal
! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
subroutine assigninitialconditions (n_nod, n_elm, n_int, mshdata, vnd, velm)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip),  intent(in) :: n_nod, n_elm, n_int
  type(t_mesh), intent(in) :: mshdata
  type(t_vnd)              :: vnd
  type(t_velm)             :: velm
  !.
  integer(ip)              :: i, p_prop, pmat, t_cel


  do i=1,n_nod
    p_prop = get_node_prop( i, mshdata%node)
    call get_prop_nd (p_prop, mshdata%prnd, t_cel, pmat)
    vnd%Vo_n(i) = initcond_elecmodel (t_cel)
  end do
  vnd%Vn_n = vnd%Vo_n
  vnd%Va_n = vnd%Vo_n
  !.
  if (n_int > 0) then
    do i=1,n_elm
      p_prop         = get_elemprop      (i, mshdata%elem)
      t_cel          = get_propelm_tcel  (p_prop, mshdata%prel)
      velm%Vo_e(:,i) = initcond_elecmodel(t_cel)
    end do
    velm%Vn_e = velm%Vo_e
    velm%Va_e = velm%Vo_e
  end if
  !.
  return
end subroutine assigninitialconditions
! ------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
subroutine set_edat (iel, n, nr, Ke, Me )
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)   :: iel, n, nr
  real(rp),    intent(in)   :: Ke(:,:), Me(:)
  !.
  integer(ip)               :: m, l

  m= n - nr; l= nr+1
  edat(iel)%Me   (1:n)     = Me(1:n)
  edat(iel)%Ke21 (1:m,1:nr)= Ke(l:n,1:nr)
  edat(iel)%Ke22i(1:m,1:m) = Ke(l:n,l:n)
  return
end subroutine set_edat
! -------------------------------------------------------------------------------
! &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
! -------------------------------------------------------------------------------
end module mod_monodominio
