module mod_genmesh
!  -----------------------------------------------------------------------------
  use mod_precision;
  use mod_fforma
  public  :: malla_me
  private :: MT03HT2DT03, MT03HT2DT03B, MQ04HT2DT03,  MQ04HT2DT03B, MQ12HT2DT03
  private :: MQ05HT2DQ04, MQ05HT2DQ04B, MT04HT3DT04,  MT04HT3DT04B
  private :: MH07HT3DH08, MH07HT3DH08B
  !.
contains
! ------------------------------------------------------------------------------
subroutine  malla_me (ndim,n_pe,xn_me,flg_b,nlib,nnod,nelm,xn_mme,cn_mme)
! malla_me (elm%ndim,m_e%n_pe,xn_me,elm%flg_b, elm%nlib,  m_e%nnod,        &
!                 m_e%nelm,xn_mme,cn_mme)

! ------------------------------------------------------------------------------
! ndim             : dimension (1, 2 o 3)
! n_pe             : numero de nodos que tiene el elemento macro
! xn_me(ndim,n_pe) : coordenadas de los nodos del elemento macro
! cn_me(n_pe)      : conectividad del elemento macro
! nlib             : numero de nodos de cada elmento de la malla macro
! nnod             : numero de nodos de la malla del elemento
! nelm             : numero de elementos de la malla micro
! xn_mme(ndim,nnod): coordenadas de la malla micro
! cn_mme(nlib,nelm): conectividad de la malla micro
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter               :: u_3 = 1.0/3.0
  integer(ip), intent(in)           :: ndim, n_pe, nlib, nnod, nelm 
  logical                           :: flg_b
  real(rp),    intent(in)           :: xn_me(ndim,n_pe) 
  !------------------------------------------------------------------------------
  real(rp),    intent(out)          :: xn_mme(ndim,nnod)
  integer(ip), intent(out)          :: cn_mme(nlib,nelm)
  !-- Malla en el macro elemento

  select case(ndim)
  case (1)
    xn_mme(1,1:2) = xn_me(1,1:2)
    if (flg_b) then
      xn_mme(1,3) = 0.5*sum(xn_me (1,1:2))
      xn_mme(1,4) = 0.5*sum(xn_mme(1,(/1,3/)))
      xn_mme(1,5) = 0.5*sum(xn_mme(1,(/2,3/)))
      !.
      cn_mme(1:3,1) = (/1, 3, 4/); cn_mme(1:3,2) = (/3, 2, 5/)
    else
      select case(nelm)
      case (2)
        xn_mme(1,3) = 0.5*sum(xn_me(1,1:2))
        !.
        cn_mme(1:2,1) = (/1, 3/);  cn_mme(1:2,2) = (/3, 2/)
      case (3)
        xn_mme(1,3) = xn_me(1,1) +     u_3*(xn_me(1,2) - xn_me(1,1))
        xn_mme(1,4) = xn_me(1,1) + 2.0*u_3*(xn_me(1,2) - xn_me(1,1))
        !.
        cn_mme(1:2,1) = (/1, 3/);  cn_mme(1:2,2) = (/3, 4/)
        cn_mme(1:2,3) = (/4, 2/)
      case default
        stop 'Error malla_me 1D: 1'
      end select
    end if
  case (2) 
    xn_mme(1:2,1:n_pe) = xn_me(1:2,1:n_pe)
    select case(n_pe)
    case (3)  !.Malla original de triangulos
      !print *,'nlib',nlib,'nnod',nnod,'nelm',nelm
      select case(nlib)
      case (3); call MT03HT2DT03  (xn_mme,cn_mme) !.3 Triangulos lineales
      case (4); call MT03HT2DT03B (xn_mme,cn_mme) !.3 Triangulos con burbuja
      case default
        stop 'Error malla_me 2D: macro triangulo'
      end select
    case (4)  !.Malla original de cuadrilateros
      if (flg_b) then  !.Elementos con burbuja
        select case(nlib)
        case ( 4); call MQ04HT2DT03B (xn_mme, cn_mme)!.4 Triangulos con burbuja
        case ( 5); call MQ05HT2DQ04B (xn_mme, cn_mme)!.5 Cuadrilateros con burbuja
        case default
          stop 'Error malla_me 2D: macro cuadrilatero 1'
        end select
      else
        !print *,'nlib',nlib, nnod
        !pause
        select case(nnod)
        case ( 5); call MQ04HT2DT03 (xn_mme, cn_mme)   !. 4 Triangulos lineales
        case ( 8); call MQ05HT2DQ04 (xn_mme, cn_mme)   !. 5 Cuadrilateros lineales
        case ( 9); call MQ12HT2DT03 (xn_mme, cn_mme);  !.12 Triangulos lineales'
        case default
          stop 'Error malla_me 2D: macro cuadrilatero 2'
        end select
      end if
    case (8)
      select case(nelm)
      case ( 4); call MQ04HT2DQ04B (xn_mme, cn_mme);! print *,' !.4 cuadrilateros con burbuja'
      case (16); call MQ16HT2DT03  (xn_mme, cn_mme);! print *,' !.16 Triangulos lineales '
      case default
        stop 'Error malla_me 2D: macro cuadrilatero 3'
      end select
    case default
      stop 'Error malla_me 2D'
    end select
  case (3)
    xn_mme(1:3,1:n_pe) = xn_me(1:3,1:n_pe)
    select case(n_pe)
    case (4)  !.Malla original de tetraedros
      select case(nlib)
      case ( 4); call MT04HT3DT04  (xn_mme, cn_mme) !.4 Tetraedros lineales
      case ( 5); call MT04HT3DT04B (xn_mme, cn_mme) !.4 Tetraedros con burbuja
      case default
        stop 'Error malla_me 3D: tetraedros '
      end select
    case (8)  !.Malla original de hexaedros
      select case(nlib)
      case (8); call MH07HT3DH08  (xn_mme, cn_mme);! print *,'!.7 Hexaedros lineales'
      case (9); call MH07HT3DH08B (xn_mme, cn_mme);! print *,'!.7 Hexaedros con burbuja'
      case default
        stop 'Error malla_me 3D: hexaedros '
      end select
    case default
      stop 'Error malla_me 3D'
    end select
  case default
    stop 'Error malla_me => dimension '
  end select
  !.
  return
end subroutine malla_me
! ------------------------------------------------------------------------------
subroutine MT03HT2DT03(xn,cn)
! ------------------------------------------------------------------------------
!. 3 triangulos lineales
  implicit none
  real(rp), parameter           :: u_3 = 1.0/3.0
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)

  xn(1,4) = u_3*sum(xn(1,1:3));  xn(2,4) = u_3*sum(xn(2,1:3))
  !.
  cn(1:3,1) = (/1, 2, 4/);  cn(1:3,2) = (/2, 3, 4/);  cn(1:3,3) = (/3, 1, 4/)
  return
end subroutine MT03HT2DT03
! ------------------------------------------------------------------------------
subroutine MT03HT2DT03B(xn,cn)
! ------------------------------------------------------------------------------
!. 3 triangulos lineales
  implicit none
  real(rp), parameter           :: u_3 = 1.0/3.0
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)

  xn(1,4) = u_3*sum(xn(1,1:3));       xn(2,4) = u_3*sum(xn(2,1:3))
  !.
  xn(1,5) = u_3*sum(xn(1,(/1,2,4/))); xn(2,5) = u_3*sum(xn(2,(/1,2,4/)))
  !.
  xn(1,6) = u_3*sum(xn(1,(/2,3,4/))); xn(2,6) = u_3*sum(xn(2,(/2,3,4/)))
  !.
  xn(1,7) = u_3*sum(xn(1,(/3,1,4/))); xn(2,7) = u_3*sum(xn(2,(/3,1,4/)))
  !.
  cn(1:4,1) = (/1, 2, 4, 5/)
  cn(1:4,2) = (/2, 3, 4, 6/)
  cn(1:4,3) = (/3, 1, 4, 7/)
  return
end subroutine MT03HT2DT03B
! ------------------------------------------------------------------------------
subroutine MQ04HT2DT03(xn,cn)
! ------------------------------------------------------------------------------
!. 4 Triangulos lineales
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: u_4 = 0.25
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)


  xn(1,5) = u_4*sum(xn(1,1:4));        xn(2,5) = u_4*sum(xn(2,1:4))
  !.
  cn(1:3,1) = (/1, 2, 5/);  cn(1:3,2) = (/2, 3, 5/)
  cn(1:3,3) = (/3, 4, 5/);  cn(1:3,4) = (/4, 1, 5/)
  return
end subroutine MQ04HT2DT03
! ------------------------------------------------------------------------------
subroutine MQ12HT2DT03 (xn,cn)
! ------------------------------------------------------------------------------
!. 12 Triangulos lineales
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi= 0.5, cer=0.0
  real(rp), parameter           :: pt1(1:2)=(/ cer,-psi/);
  real(rp), parameter           :: pt2(1:2)=(/ psi, cer/);
  real(rp), parameter           :: pt3(1:2)=(/ cer, psi/);
  real(rp), parameter           :: pt4(1:2)=(/-psi, cer/);
  real(rp), parameter           :: pt5(1:2)=(/ cer, cer/);

  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  real(rp)                      :: ff(4)
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=4
  !.
  call fforma2d(flg,npe,pt1,ff=ff); xn(1:2,5)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt2,ff=ff); xn(1:2,6)=dot_ff2d(ff,(xn(1:2,1:4)))
  !.
  call fforma2d(flg,npe,pt3,ff=ff); xn(1:2,7)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt4,ff=ff); xn(1:2,8)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt5,ff=ff); xn(1:2,9)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  cn(1:3, 1) = (/1, 2, 5/);  cn(1:3, 2) = (/2, 3, 6/); cn(1:3, 3) = (/3, 4, 7/);  

  cn(1:3, 4) = (/4, 1, 8/);  cn(1:3, 5) = (/1, 5, 8/); cn(1:3, 6) = (/2, 6, 5/); 
 
  cn(1:3, 7) = (/3, 7, 6/);  cn(1:3, 8) = (/4, 8, 7/); cn(1:3, 9) = (/5, 9, 8/);

  cn(1:3,10) = (/5, 6, 9/);  cn(1:3,11) = (/6, 7, 9/); cn(1:3,12) = (/7, 8, 9/);
  return
end subroutine MQ12HT2DT03
! ------------------------------------------------------------------------------
subroutine MQ16HT2DT03 (xn, cn);  
! ------------------------------------------------------------------------------
!. 16 Triangulos lineales
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi= 0.5, cer=0.0
  real(rp), parameter           :: pt1(1:2)=(/ cer, cer/);  !.pto. 9 
  real(rp), parameter           :: pt2(1:2)=(/-psi,-psi/);  !.pto.10 
  real(rp), parameter           :: pt3(1:2)=(/ psi,-psi/);  !.pto.11 
  real(rp), parameter           :: pt4(1:2)=(/ psi, psi/);  !.pto.12 
  real(rp), parameter           :: pt5(1:2)=(/-psi, psi/);  !.pto.13 

  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  real(rp)                      :: ff(4)
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=4
  !.
  call fforma2d(flg,npe,pt1,ff=ff); xn(1:2, 9)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt2,ff=ff); xn(1:2,10)=dot_ff2d(ff,(xn(1:2,1:4)))
  !.
  call fforma2d(flg,npe,pt3,ff=ff); xn(1:2,11)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt4,ff=ff); xn(1:2,12)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt5,ff=ff); xn(1:2,13)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  cn(1:3, 1) = (/ 1, 5,10/);  cn(1:3, 2) = (/ 5, 2,11/); 
  cn(1:3, 3) = (/ 1,10, 8/);  cn(1:3, 4) = (/ 5, 9,10/);  
  cn(1:3, 5) = (/ 5,11, 9/);  cn(1:3, 6) = (/ 2, 6,11/); 
  cn(1:3, 7) = (/10, 9, 8/);  cn(1:3, 8) = (/11, 6, 9/); 
  !.
  cn(1:3, 9) = (/ 8, 9,13/);  cn(1:3,10) = (/ 9, 6,12/); 
  cn(1:3,11) = (/ 8,13, 4/);  cn(1:3,12) = (/ 9, 7,13/);  
  cn(1:3,13) = (/ 9,12, 7/);  cn(1:3,14) = (/ 6, 3,12/); 
  cn(1:3,15) = (/13, 7, 4/);  cn(1:3,16) = (/12, 3, 7/); 
  !.
  return
end subroutine MQ16HT2DT03
! ------------------------------------------------------------------------------
subroutine MQ04HT2DQ04B (xn, cn)
! ------------------------------------------------------------------------------
!. 4 cuadrilateros con burbuja
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi= 0.5, cer=0.0
  real(rp), parameter           :: pt1(1:2)=(/ cer, cer/);  !.pto. 9 
  real(rp), parameter           :: pt2(1:2)=(/-psi,-psi/);  !.pto.10 
  real(rp), parameter           :: pt3(1:2)=(/ psi,-psi/);  !.pto.11 
  real(rp), parameter           :: pt4(1:2)=(/ psi, psi/);  !.pto.12 
  real(rp), parameter           :: pt5(1:2)=(/-psi, psi/);  !.pto.13 

  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  real(rp)                      :: ff(4)
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=4
  !.
  call fforma2d(flg,npe,pt1,ff=ff); xn(1:2, 9)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt2,ff=ff); xn(1:2,10)=dot_ff2d(ff,(xn(1:2,1:4)))
  !.
  call fforma2d(flg,npe,pt3,ff=ff); xn(1:2,11)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt4,ff=ff); xn(1:2,12)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt5,ff=ff); xn(1:2,13)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  cn(1:5, 1) = (/ 1, 5, 9, 8, 10/);  cn(1:5, 2) = (/ 5, 2, 6, 9, 11/); 
  cn(1:5, 3) = (/ 8, 9, 7, 4, 13/);  cn(1:5, 4) = (/ 9, 6, 3, 7, 12/);  

  return
end subroutine MQ04HT2DQ04B
! ------------------------------------------------------------------------------
subroutine MQ04HT2DT03B(xn,cn)
! ------------------------------------------------------------------------------
!. 4 Triangulos con burbuja
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: u_4 = 0.25, u_3 = 1.0/3.0
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)


  xn(1,5) = u_4*sum(xn(1,1:4));        xn(2,5) = u_4*sum(xn(2,1:4))
  !.
  xn(1,6) = u_3*sum(xn(1,(/1,2,5/)));  xn(2,6) = u_3*sum(xn(2,(/1,2,5/)))
  !.
  xn(1,7) = u_3*sum(xn(1,(/2,3,5/)));  xn(2,7) = u_3*sum(xn(2,(/2,3,5/)))
  !.
  xn(1,8) = u_3*sum(xn(1,(/3,4,5/)));  xn(2,8) = u_3*sum(xn(2,(/3,4,5/)))
  !.
  xn(1,9) = u_3*sum(xn(1,(/4,1,5/)));  xn(2,9) = u_3*sum(xn(2,(/4,1,5/)))
  !
  cn(1:4,1) = (/1, 2, 5, 6/);  cn(1:4,2) = (/2, 3, 5, 7/)
  cn(1:4,3) = (/3, 4, 5, 8/);  cn(1:4,4) = (/4, 1, 5, 9/)
  return
end subroutine MQ04HT2DT03B
! ------------------------------------------------------------------------------
function dot_ff2d(ff,xn)  result (XY)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)   :: ff(4),xn(2,4)
  real(rp)               :: XY(2) 
 
  XY(1) = dot_product(xn(1,:),ff); XY(2) = dot_product(xn(2,:),ff)
  return
end function dot_ff2d
! ------------------------------------------------------------------------------
function quad_varic(Xn) result (XY)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter        :: u_4 = 0.25
  real(rp),intent(in)        :: Xn(2,4)
  real(rp)                   :: XY(2)

  XY(1)=u_4*sum(Xn(1,1:4));  XY(2)=u_4*sum(Xn(2,1:4)) 
  return
end function quad_varic
! ------------------------------------------------------------------------------
subroutine MQ05HT2DQ04 (xn, cn)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi= sqrt(2.0)/(2.0+sqrt(2.0)) !.Opcion 1
 ! real(rp), parameter           :: psi= 1.0/sqrt(5.0)             !.Opcion 2
 ! real(rp), parameter           :: psi= 1.0/3.0                   !.Opcion 3
  !.
  real(rp), parameter           :: pt1(1:2)=(/-psi,-psi/);
  real(rp), parameter           :: pt2(1:2)=(/ psi,-psi/);
  real(rp), parameter           :: pt3(1:2)=(/ psi, psi/);
  real(rp), parameter           :: pt4(1:2)=(/-psi, psi/);
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  real(rp)                      :: ff(4)
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=4
  !.
  call fforma2d(flg,npe,pt1,ff=ff); xn(1:2,5)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt2,ff=ff); xn(1:2,6)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt3,ff=ff); xn(1:2,7)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt4,ff=ff); xn(1:2,8)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  cn(1:4,1) = (/ 1, 2, 6, 5/);  cn(1:4,2) = (/ 2, 3, 7, 6/)
  cn(1:4,3) = (/ 3, 4, 8, 7/);  cn(1:4,4) = (/ 4, 1, 5, 8/)
  cn(1:4,5) = (/ 5, 6, 7, 8/)
  return
end subroutine MQ05HT2DQ04
! ------------------------------------------------------------------------------
subroutine MQ05HT2DQ04B(xn,cn)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi= sqrt(2.0)/(2.0+sqrt(2.0)) 
  !real(rp), parameter           :: psi= 1.0/sqrt(5.0)
  real(rp), parameter           :: pt1(1:2)=(/-psi,-psi/);
  real(rp), parameter           :: pt2(1:2)=(/ psi,-psi/);
  real(rp), parameter           :: pt3(1:2)=(/ psi, psi/);
  real(rp), parameter           :: pt4(1:2)=(/-psi, psi/);
  !.
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  real(rp)                      :: ff(4)
  logical                       :: flg
  integer(ip)                   :: npe


  flg=.false.; npe=4
  !.
  call fforma2d(flg,npe,pt1,ff=ff); xn(1:2,5)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt2,ff=ff); xn(1:2,6)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt3,ff=ff); xn(1:2,7)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  call fforma2d(flg,npe,pt4,ff=ff); xn(1:2,8)=dot_ff2d(ff,(xn(1:2,1:4))) 
  !.
  cn(1:5,1) = (/ 1, 2, 6, 5, 9/);  cn(1:5,2) = (/ 2, 3, 7, 6,10/)
  cn(1:5,3) = (/ 3, 4, 8, 7,11/);  cn(1:5,4) = (/ 4, 1, 5, 8,12/)
  cn(1:5,5) = (/ 5, 6, 7, 8,13/)
  !.
  xn(1:2, 9) = quad_varic( xn(1:2, cn(1:4,1)))
  !.
  xn(1:2,10) = quad_varic( xn(1:2, cn(1:4,2)))
  !.
  xn(1:2,11) = quad_varic( xn(1:2, cn(1:4,3)))
  !.
  xn(1:2,12) = quad_varic( xn(1:2, cn(1:4,4)))
  !.
  xn(1:2,13) = quad_varic( xn(1:2, cn(1:4,5)))
  !.
  return
end subroutine MQ05HT2DQ04B
! ------------------------------------------------------------------------------
subroutine MT04HT3DT04  (xn, cn) !.4 Tetraedros lineales
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)

  xn(1:3,5) = tetra_varic (xn(1:3,1:4))
  !.
  cn(1:4,1) = (/ 1, 2, 3, 5/);  cn(1:4,2) = (/ 1, 4, 2, 5/)
  cn(1:4,3) = (/ 2, 4, 3, 5/);  cn(1:4,4) = (/ 3, 4, 1, 5/)
  !.
  return
end subroutine MT04HT3DT04
! ------------------------------------------------------------------------------
function tetra_varic(Xn) result (XYZ)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter        :: u_4 = 0.25
  real(rp),intent(in)        :: Xn(3,4)
  real(rp)                   :: XYZ(3)

  XYZ(1)=u_4*sum(Xn(1,1:4)) 
  XYZ(2)=u_4*sum(Xn(2,1:4)) 
  XYZ(3)=u_4*sum(Xn(3,1:4))
  return
end function tetra_varic
! ------------------------------------------------------------------------------
subroutine MT04HT3DT04B (xn, cn) !.4 Tetraedros con burbuja
! ------------------------------------------------------------------------------
  implicit none
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)

  xn(1:3,5) = tetra_varic (xn(1:3,1:4))
  xn(1:3,6) = tetra_varic (xn(1:3,(/ 1, 2, 3, 5/)))
  xn(1:3,7) = tetra_varic (xn(1:3,(/ 1, 4, 2, 5/)))
  xn(1:3,8) = tetra_varic (xn(1:3,(/ 2, 4, 3, 5/)))
  xn(1:3,9) = tetra_varic (xn(1:3,(/ 3, 4, 1, 5/)))
  !.
  cn(1:5,1) = (/ 1, 2, 3, 5, 6/);  cn(1:5,2) = (/ 1, 4, 2, 5, 7/)
  cn(1:5,3) = (/ 2, 4, 3, 5, 8/);  cn(1:5,4) = (/ 3, 4, 1, 5, 9/)
  !.
  return
end subroutine MT04HT3DT04B
! ------------------------------------------------------------------------------
function hexa_varic(Xn) result (XYZ)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter        :: u_8 = 1.0/8.0
  real(rp),intent(in)        :: Xn(3,8)
  real(rp)                   :: XYZ(3)

  XYZ(1)=u_8*sum(Xn(1,1:8)) 
  XYZ(2)=u_8*sum(Xn(2,1:8)) 
  XYZ(3)=u_8*sum(Xn(3,1:8))
  return
end function hexa_varic
! ------------------------------------------------------------------------------
function dot_ff3d(ff,xn)  result (XYZ)
! ------------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)   :: ff(8),xn(3,8)
  real(rp)               :: XYZ(3) 
 
  XYZ(1) = dot_product(xn(1,:),ff)
  XYZ(2) = dot_product(xn(2,:),ff)
  XYZ(3) = dot_product(xn(3,:),ff)
  return
end function dot_ff3d
! ------------------------------------------------------------------------------
subroutine MH07HT3DH08 (xn, cn) !.7 Hexaedros lineales
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi = sqrt(3.0)/(2.0+sqrt(3.0))
  real(rp), parameter           :: pt1(1:3) = (/-psi, -psi, -psi/)
  real(rp), parameter           :: pt2(1:3) = (/ psi, -psi, -psi/)
  real(rp), parameter           :: pt3(1:3) = (/ psi,  psi, -psi/)
  real(rp), parameter           :: pt4(1:3) = (/-psi,  psi, -psi/)
  real(rp), parameter           :: pt5(1:3) = (/-psi, -psi,  psi/)
  real(rp), parameter           :: pt6(1:3) = (/ psi, -psi,  psi/)
  real(rp), parameter           :: pt7(1:3) = (/ psi,  psi,  psi/)
  real(rp), parameter           :: pt8(1:3) = (/-psi,  psi,  psi/)
  !.
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  !.
  real(rp)                      :: ff(8)
  !.
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=8
  !.
  call fforma3d(flg,npe,pt1,ff=ff); xn(1:3, 9) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt2,ff=ff); xn(1:3,10) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt3,ff=ff); xn(1:3,11) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt4,ff=ff); xn(1:3,12) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt5,ff=ff); xn(1:3,13) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt6,ff=ff); xn(1:3,14) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt7,ff=ff); xn(1:3,15) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt8,ff=ff); xn(1:3,16) = dot_ff3d(ff,xn(1:3,1:8))
  !.
  cn(1:8,1) = (/ 1, 2, 3, 4, 9,10,11,12/)
  cn(1:8,2) = (/ 1, 5, 6, 2, 9,13,14,10/)
  cn(1:8,3) = (/ 2, 6, 7, 3,10,14,15,11/)
  cn(1:8,4) = (/ 3, 7, 8, 4,11,15,16,12/)
  cn(1:8,5) = (/ 4, 8, 5, 1,12,16,13, 9/)
  cn(1:8,6) = (/ 5, 8, 7, 6,13,16,15,14/)
  cn(1:8,7) = (/ 9,10,11,12,13,14,15,16/)
  !.
  return
end subroutine MH07HT3DH08
! ------------------------------------------------------------------------------
subroutine MH07HT3DH08B (xn, cn) !.7 Hexaedros con burbuja
! ------------------------------------------------------------------------------
  implicit none
  real(rp), parameter           :: psi = sqrt(3.0)/(2.0+sqrt(3.0))
  real(rp), parameter           :: pt1(1:3) = (/-psi, -psi, -psi/)
  real(rp), parameter           :: pt2(1:3) = (/ psi, -psi, -psi/)
  real(rp), parameter           :: pt3(1:3) = (/ psi,  psi, -psi/)
  real(rp), parameter           :: pt4(1:3) = (/-psi,  psi, -psi/)
  real(rp), parameter           :: pt5(1:3) = (/-psi, -psi,  psi/)
  real(rp), parameter           :: pt6(1:3) = (/ psi, -psi,  psi/)
  real(rp), parameter           :: pt7(1:3) = (/ psi,  psi,  psi/)
  real(rp), parameter           :: pt8(1:3) = (/-psi,  psi,  psi/)
  !.
  real(rp),intent(inout)        :: xn(:,:)
  integer(ip),intent(out)       :: cn(:,:)
  !.
  real(rp)                      :: ff(8)
  !.
  logical                       :: flg
  integer(ip)                   :: npe

  flg=.false.; npe=8
  !.
  call fforma3d(flg,npe,pt1,ff=ff); xn(1:3, 9) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt2,ff=ff); xn(1:3,10) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt3,ff=ff); xn(1:3,11) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt4,ff=ff); xn(1:3,12) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt5,ff=ff); xn(1:3,13) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt6,ff=ff); xn(1:3,14) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt7,ff=ff); xn(1:3,15) = dot_ff3d(ff,xn(1:3,1:8))
  call fforma3d(flg,npe,pt8,ff=ff); xn(1:3,16) = dot_ff3d(ff,xn(1:3,1:8))
  !.
  cn(1:9,1) = (/ 1, 2, 3, 4, 9,10,11,12,17/)
  cn(1:9,2) = (/ 1, 5, 6, 2, 9,13,14,10,18/)
  cn(1:9,3) = (/ 2, 6, 7, 3,10,14,15,11,19/)
  cn(1:9,4) = (/ 3, 7, 8, 4,11,15,16,12,20/)
  cn(1:9,5) = (/ 4, 8, 5, 1,12,16,13, 9,21/)
  cn(1:9,6) = (/ 5, 8, 7, 6,13,16,15,14,22/)
  cn(1:9,7) = (/ 9,10,11,12,13,14,15,16,23/)
  !.
  xn(1:3,17)=hexa_varic(Xn(:,cn(1:8,1))); xn(1:3,18)=hexa_varic(Xn(:,cn(1:8,2)))
  xn(1:3,19)=hexa_varic(Xn(:,cn(1:8,3))); xn(1:3,20)=hexa_varic(Xn(:,cn(1:8,4)))
  xn(1:3,21)=hexa_varic(Xn(:,cn(1:8,5))); xn(1:3,22)=hexa_varic(Xn(:,cn(1:8,6)))
  xn(1:3,23)=hexa_varic(Xn(:,cn(1:8,7)))
  !.
  return
end subroutine MH07HT3DH08B
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
end module mod_genmesh
