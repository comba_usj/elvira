!  -----------------------------------------------------------------------------
module mod_idelem
!  -----------------------------------------------------------------------------
  use mod_precision; 
  use mod_error; 
  use mod_readcir
  use mod_UnitUtil
!  -----------------------------------------------------------------------------
  implicit none
!  -----------------------------------------------------------------------------
!  Elvio Heidenreich 7 de Mayo de 2008
!  Definicion de los campos del tipo de dato interno elemento
!  Donde:
!  	nomb: nombre del elemento
!  	tipo: numero del tipo de elemento, "OJO", no se puede repetir
!  	ndim: dimension del elemento
!  	ngdl: numero de grados de libertad del nodo
!  	nlib: numero de grados de libertad del elemento
!  	n_pe: numero de nodos del elemento
!       ndcv: numero de direcciones en las cargas volumentricas por elemento
!       n_te: numero de tensiones por punto de integracion
!       n_qp: numero de puntos de integracion por lado del elemento
!       ntqp: numero total de puntos de cuadratura por elemento
!       prme: parametro extra para pasar informacion
!  -----------------------------------------------------------------------------
  type t_idel
    private
    character (len=12)  :: nomb
    integer(ip)         :: tipo
    integer(ip)         :: ndim
    integer(ip)         :: ngdl
    integer(ip)         :: nlib
    integer(ip)         :: n_pe
    integer(ip)         :: ndcv
    integer(ip)         :: n_te
    integer(ip)         :: n_qp
    integer(ip)         :: ntqp
    integer(ip)         :: prme
  end type t_idel

  include 'idelem.inc'
  !.
!  integer(ip), private               :: lu_eid   !.numero de unidad de lectura
  !.
!  integer(ip), private               :: n_tel = 0 !.Numero total de elementos leidos
  !.
!  integer(ip), private               :: ip_loc   !.Puntero al ultimo nombre accedido
!  character (len = 12), private      :: nom_loc  !.Nombre del ultimo elemento accedido
  !.
!  integer(ip), private               :: ip_tip   !.Ultimo tipo de elemento accedido
 
!  save  n_tel 
   integer(ip), private         :: n_tel              ! number of element type
   integer(ip), private         :: idx_ea(m_elmt) = 0 ! index of active element type 
   integer(ip), private         :: idx_et(m_elmt) = 0 ! pointer to active element type 

   save n_tel, idx_ea, idx_et
!  ----------------------------------------------------------------------------- 
contains
!  ----------------------------------------------------------------------------- 
subroutine id_par_ele (nomb, tipo, ndim, ngdl, nlib, n_pe, ndcv, n_te, n_qp,   &
                       ntqp, prme)
!  ----------------------------------------------------------------------------- 
! Identificacion de los parametros del elemento a partir del nombre del mismo
!  Donde:
!  	nomb: nombre del elemento
!  	tipo: numero del tipo de elemento
!  	ndim: dimension del elemento
!  	ngdl: numero de grados de libertad del nodo
!  	nlib: numero de grados de libertad del elemento
!  	n_pe: numero de nodos del elemento
!       ndcv: numero de direcciones en las cargas volumentricas por elemento
!       n_te: numero de tensiones por punto de integracion
!       n_qp: numero de puntos de integracion por lado del elemento
!       ntqp: numero total de puntos de cuadratura por elemento
! ------------------------------------------------------------------------------
  implicit none
  character (len=*),intent(in)          :: nomb
  integer(ip), intent(out),optional     :: tipo, ndim, ngdl, nlib, n_pe, ndcv, &
                                           n_te, n_qp, ntqp, prme
  integer(ip)                           :: iflag, j
!.
  iflag = 0; j=1
  do while ((iflag==0).and.(j<=m_elmt))
    if (id_elm(j)%nomb == nomb) then
            iflag = 1
            if(idx_ea(j)==0) then
                    idx_ea(j)=1
                    n_tel = n_tel+1
                    idx_et(n_tel)=j
            endif
    else
            j=j+1
    endif
  enddo
  if (iflag == 1) then
    if (present(tipo)) tipo = id_elm(j)%tipo
    if (present(ndim)) ndim = id_elm(j)%ndim
    if (present(ngdl)) ngdl = id_elm(j)%ngdl
    if (present(nlib)) nlib = id_elm(j)%nlib
    if (present(n_pe)) n_pe = id_elm(j)%n_pe
    if (present(ndcv)) ndcv = id_elm(j)%ndcv
    if (present(n_te)) n_te = id_elm(j)%n_te
    if (present(n_qp)) n_qp = id_elm(j)%n_qp
    if (present(ntqp)) ntqp = id_elm(j)%ntqp
    if (present(prme)) prme = id_elm(j)%prme
  else
    call w_error (9,nomb,1) 
  end if
  

end subroutine id_par_ele
! ------------------------------------------------------------------------------  
subroutine id_elem_tip(tipo, nomb, ndim, ngdl, nlib, n_pe, ndcv, n_te, n_qp,   &
                       ntqp, prme)
! ------------------------------------------------------------------------------ 
! Esta subrutina identifica los parametros de un elemento de acuerdo al "tipo"
! ingresado
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip),intent(in)                    :: tipo
  character (len=*),intent(out),optional    :: nomb
  integer(ip), intent(out),optional         :: ndim, ngdl, nlib, n_pe, ndcv,   &
                                               n_te, n_qp, ntqp, prme
  integer(ip)                               :: iflag, j

  iflag = 0; j=1
  do while ((iflag==0).and.(j<=m_elmt))
    if (id_elm(j)%tipo == tipo) then
            iflag = 1
            if(idx_ea(j)==0) then
                    idx_ea(j)=1
                    n_tel = n_tel+1
                    idx_et(n_tel)=j
            endif
    else
            j=j+1
    endif
  enddo
  if (iflag == 1) then
    if (present(nomb)) nomb=id_elm(j)%nomb
    if (present(ndim)) ndim = id_elm(j)%ndim
    if (present(ngdl)) ngdl = id_elm(j)%ngdl
    if (present(nlib)) nlib = id_elm(j)%nlib
    if (present(n_pe)) n_pe = id_elm(j)%n_pe
    if (present(ndcv)) ndcv = id_elm(j)%ndcv
    if (present(n_te)) n_te = id_elm(j)%n_te
    if (present(n_qp)) n_qp = id_elm(j)%n_qp
    if (present(ntqp)) ntqp = id_elm(j)%ntqp
    if (present(prme)) prme = id_elm(j)%prme
  else
    call w_error (11,tipo,1) 
  end if
  return
end subroutine id_elem_tip
! ------------------------------------------------------------------------------  
subroutine assembly_idelem (tipo, nlib_e, nlib_i, npe)
! ------------------------------------------------------------------------------ 
! Esta subrutina identifica los parametros de un elemento de acuerdo al "tipo"
! ingresado y me devuelve los grados de libertad externos [nlib_e] e internos
! nlib_i, y los nodos por elemento n_pe
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip),intent(in)                    :: tipo
  integer(ip), intent(out),optional         :: nlib_e, nlib_i, npe
  !.
  integer(ip)                               :: ngdl, nlib, n_pe

  call id_elem_tip ( tipo, ngdl=ngdl, nlib=nlib, n_pe=n_pe)
  !.
  if (present(nlib_e)) nlib_e = n_pe*ngdl
  if (present(nlib_i)) nlib_i = nlib - n_pe*ngdl
  if (present(npe   )) npe    = n_pe
  return
end subroutine assembly_idelem
! ------------------------------------------------------------------------------
function max_npe(n_elt)
! ------------------------------------------------------------------------------
! Esta funcion me determina cual es el elemento con el numero maximo de 
! nodos, y me devuelve dicho numero de nodos
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: n_elt
  integer(ip)             :: max_npe
  
  if (n_elt > 0 ) then
    max_npe=maxval(id_elm(idx_et(1:n_elt))%n_pe)
  else
    call w_error (12,n_elt,1)
  end if
  return
end function max_npe
! ------------------------------------------------------------------------------
!function lee_idelem (lu_ide,nomb) 
! ------------------------------------------------------------------------------
! Esta funcion busca en la unidad lu_ide el nombre ("nomb") del elemento 
! Ademas lee todos los campos de la estructura "t_idel"
! ------------------------------------------------------------------------------  
!  implicit none
!  integer(ip)                   :: lu_ide
!  character (len=*),intent(in)  :: nomb
!  type(t_idel)                  :: lee_idelem
!  integer(ip)                   :: i_err
!  
!  if (search (lu_ide,nomb)) then
!    backspace(lu_ide)
!    read (lu_ide,*,iostat=i_err) lee_idelem 
!    if(i_err > 0 ) call w_error (9,nomb,1) 
!  else
!    call w_error (10,nomb,1)
!  end if
!  return
!end function lee_idelem
!  ----------------------------------------------------------------------------- 
!subroutine read_elem_type (tipo, elm_id)
!  ----------------------------------------------------------------------------- 
! Llena la base de datos del elemento a partir del tipo del mismo
!  Donde:
!  	tipo: numero del tipo de elemento
! ------------------------------------------------------------------------------
!  implicit none
!  integer(ip), intent(in)   :: tipo
!  type(t_idel), intent(out) :: elm_id
!  !.Variables locales
!  logical                  :: flg_o, flg_r
!  integer(ip)              :: lu_m
!  integer(ip)              :: io_err
!  !.
!
!  inquire(file='../conf/id_elem.conf',opened=flg_o, number=lu_m)
!  if (flg_o) then
!    call read_elmt(lu_m, tipo, elm_id, flg_r)
!  else
!    call a_unit(lu_m)
!    open (unit=lu_m,file='../conf/id_elem.conf',status='old',iostat=io_err)
!    if (io_err > 0 ) call w_error (8,'../conf/id_elem.conf', 1)
!    call read_elmt(lu_m, tipo, elm_id, flg_r)
!  end if
!  if (.not.flg_r) call w_error (11,tipo, 1)
!  return
!end subroutine read_elem_type
! ------------------------------------------------------------------------------ 
!subroutine read_elmt(lu_m, tipo, elm_id, flg_r)
! ------------------------------------------------------------------------------ 
!  implicit none
!  integer(ip), intent(in)   :: lu_m, tipo
!  type(t_idel), intent(out) :: elm_id
!  logical                   :: flg_r
!  character (len=80)        :: str_r
!  integer(ip)               :: io_err
!
!  flg_r = .false.
!  do
!    read (lu_m,'(A)',iostat=io_err) str_r
!    if   (.not.(index(str_r,'#') == 1)) then
!      backspace(lu_m); read (lu_m,*,iostat=io_err) elm_id
!     ! print *, '@@.elm_id: ',elm_id%nomb, elm_id%tipo
!      if (elm_id%tipo == tipo) then
!        flg_r = .true.; exit
!      end if
!    end if
!    if (io_err == -1) exit
!  end do
!  return
!end subroutine read_elmt
! ------------------------------------------------------------------------------  
!subroutine id_elem_tip(tipo, nomb, ndim, ngdl, nlib, n_pe, ndcv, n_te, n_qp,   &
!                       ntqp, prme)
! ------------------------------------------------------------------------------ 
! Esta subrutina identifica los parametros de un elemento de acuerdo al "tipo"
! ingresado
! ------------------------------------------------------------------------------ 
!  implicit none
!  integer(ip),intent(in)                    :: tipo
!  character (len=*),intent(out),optional    :: nomb
!  integer(ip), intent(out),optional         :: ndim, ngdl, nlib, n_pe, ndcv,   &
!                                               n_te, n_qp, ntqp, prme
!  integer(ip)                               :: iflag, i, j
!  type(t_idel)                              :: elm_id
!
!  iflag = 0
!  if (n_tel == 0) then 
!    call read_elem_type (tipo, elm_id)
!    n_tel = n_tel + 1; j = n_tel
!    !.
!    id_elm(j) =elm_id
!    !.
!    ip_loc= j; nom_loc = nomb; iflag = 1
!  else
!    if (tipo == id_elm(ip_loc)%tipo) then
!      j = ip_loc; iflag=1
!    else
!      do i=1,n_tel
!        if ( tipo == id_elm(i)%tipo ) then
!          j = i; iflag=1; ip_loc= j
!          ip_tip= tipo;   exit
!        end if
!      end do
!    endif
!    if (iflag == 0) then
!      call read_elem_type (tipo, elm_id)
!      n_tel = n_tel + 1; j = n_tel
!      !.
!      id_elm(j) =elm_id
!      !.
!      ip_loc= j; nom_loc = nomb; iflag = 1
!!    end if
!  end if
!!.esta rutina puede fallar, en algun momento hay que reemplazar este modulo
!  if (iflag == 1) then
!    if (present(nomb)) nomb=id_elm(j)%nomb
!    if (present(ndim)) ndim=id_elm(j)%ndim
!    if (present(ngdl)) ngdl=id_elm(j)%ngdl
!    if (present(nlib)) nlib=id_elm(j)%nlib
!    if (present(n_pe)) n_pe=id_elm(j)%n_pe
!    if (present(ndcv)) ndcv=id_elm(j)%ndcv
!    if (present(n_te)) n_te=id_elm(j)%n_te
!    if (present(n_qp)) n_qp=id_elm(j)%n_qp
!    if (present(ntqp)) ntqp=id_elm(j)%ntqp
!    if (present(prme)) prme=id_elm(j)%prme
!  else
!  !  print *,'n_tel: ',n_tel,id_elm(n_tel)%nomb,id_elm(n_tel)%tipo
!    call w_error (11,tipo,1)
!  end if
!  return
!end subroutine id_elem_tip
! ------------------------------------------------------------------------------  
!subroutine assembly_idelem (tipo, nlib_e, nlib_i, npe)
! ------------------------------------------------------------------------------ 
! Esta subrutina identifica los parametros de un elemento de acuerdo al "tipo"
! ingresado y me devuelve los grados de libertad externos [nlib_e] e internos
! nlib_i, y los nodos por elemento n_pe
! ------------------------------------------------------------------------------ 
!  implicit none
!  integer(ip),intent(in)                    :: tipo
!  integer(ip), intent(out),optional         :: nlib_e, nlib_i, npe
!  !.
!  integer(ip)                               :: ngdl, nlib, n_pe
!
!  call id_elem_tip ( tipo, ngdl=ngdl, nlib=nlib, n_pe=n_pe)
!  !.
!  if (present(nlib_e)) nlib_e = n_pe*ngdl
!  if (present(nlib_i)) nlib_i = nlib - n_pe*ngdl
!  if (present(npe   )) npe    = n_pe
!  return
!end subroutine assembly_idelem
! ------------------------------------------------------------------------------
!function max_npe(n_elt)
! ------------------------------------------------------------------------------
! Esta funcion me determina cual es el elemento con el numero maximo de 
! nodos, y me devuelve dicho numero de nodos
! ------------------------------------------------------------------------------
!  implicit none
!  integer(ip), intent(in) :: n_elt
!  integer(ip)             :: max_npe
!  
!  if (n_elt > 0 ) then
!    max_npe=maxval(id_elm(1:n_elt)%n_pe)
!  else
!    call w_error (12,n_elt,1)
!  end if
!  return
!end function max_npe
! ----------------------------------------------------------------------------- 
subroutine node_on_face (idim,n_pe,n_fa,n_of,iface)
!------------------------------------------------------------------------------
!  idim : dimension del elemento
!  n_pe : numero de nodos por elemento
!  n_fa : numero de la cara 
!  n_of : numero de nodos que tiene la cara
!  iface: vector que contiene la numeracion local de la cara del elemento 
!-----------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)           :: idim,n_pe,n_fa
  integer(ip), intent(out),optional :: n_of,iface(:)
  integer(ip)                       :: i3(3),i4(4)
  data i3 /2,3,1/
  data i4 /2,3,4,1/

  if (present(iface)) then
    select case ( idim )
    case (2)               !.--------- Elemento bidimensionales ----------------
      select case (n_pe)
      case (3)                            !.Elemento Triangular de 3 nodos
        iface=(/n_fa,i3(n_fa)/)
      case (4)                            !.Elemento Cuadrilatero de 4 nodos 
        iface=(/n_fa,i4(n_fa)/)
      case (6)                            !.Elemento Triangular de 6 nodos  
        iface=(/n_fa,i3(n_fa),n_fa+3/)
      case (8:9)                          !.Elemento Cuadrilatero de 8 y 9 nodos  
        iface=(/n_fa,i4(n_fa),n_fa+4/)
      case default
        call w_error (13,n_pe,1)
      end select
    case (3)               !.--------- Elementos tridimensionale ---------------
      select case (n_pe)
      case (4)                            !.Elemento tetraedrico de 4 nodos
        if (n_fa ==1) iface = (/1, 2, 3/)
        if (n_fa ==2) iface = (/1, 4, 2/)
        if (n_fa ==3) iface = (/2, 4, 3/)
        if (n_fa ==4) iface = (/3, 4, 1/)
      case (8)                            !.Elemento hexaedrico de 8 nodos
        if (n_fa ==1) iface = (/1, 2, 3, 4/)
        if (n_fa ==2) iface = (/1, 5, 6, 2/)
        if (n_fa ==3) iface = (/2, 6, 7, 3/)
        if (n_fa ==4) iface = (/3, 7, 8, 4/)
        if (n_fa ==5) iface = (/1, 4, 8, 5/)
        if (n_fa ==6) iface = (/5, 8, 7, 6/)
      case (10)                           !.Elemento tetraedrico de 10 nodos
        if (n_fa ==1) iface = (/1, 2, 3, 5, 6,7/)
        if (n_fa ==2) iface = (/1, 4, 2, 8, 9,5/)
        if (n_fa ==3) iface = (/2, 4, 3, 9,10,6/)
        if (n_fa ==4) iface = (/3, 4, 1,10, 8,7/)
      case (20)                           !.Elemento hexaedrico de 20 nodos 
        if (n_fa ==1) iface = (/1, 2, 3, 4, 9,10,11,12/)
        if (n_fa ==2) iface = (/5, 8, 7, 6,20,19,18,17/)
        if (n_fa ==3) iface = (/1, 5, 6, 2,13,17,14, 9/)
        if (n_fa ==4) iface = (/2, 6, 7, 3,14,18,15,10/)
        if (n_fa ==5) iface = (/3, 7, 8, 4,15,19,16,11/)
        if (n_fa ==6) iface = (/4, 8, 5, 1,16,20,13,12/)
      case (27)                           !.Elemento hexaedrico de 27 nodos 
        if (n_fa ==1) iface = (/1, 2, 3, 4, 9,10,11,12,21/)
        if (n_fa ==2) iface = (/5, 8, 7, 6,20,19,18,17,26/)
        if (n_fa ==3) iface = (/1, 5, 6, 2,13,17,14, 9,22/)
        if (n_fa ==4) iface = (/2, 6, 7, 3,14,18,15,10,23/)
        if (n_fa ==5) iface = (/3, 7, 8, 4,15,19,16,11,24/)
        if (n_fa ==6) iface = (/4, 8, 5, 1,16,20,13,12,25/)
      case default
        call w_error (13,n_pe,1)
      end select
    case default
      call w_error (14,idim,1)
    end select
  end if
! --- Esta parte de la subrutina me dice cuantos nodos tengo en una cara de un
! --- elemento particular
  if (present(n_of)) then
    select case ( idim )
    case (2)
      select case (n_pe)
      case (  3,4); n_of=2
      case (6,8,9); n_of=3
      case default; call w_error (13,n_pe,1)
      end select
    case(3)
      select case (n_pe)
      case ( 4); n_of=3
      case ( 8); n_of=4
      case (10); n_of=6
      case (20); n_of=8
      case (27); n_of=9
      case default; call w_error (13,n_pe,1)
      end select
    case default
      call w_error (14,idim,1)
    end select
  end if
  return
end subroutine node_on_face
!----------------------------------------------------------------------- 
end module mod_idelem
