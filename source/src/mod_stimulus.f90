! -----------------------------------------------------------------------------
module mod_stimulus
! -----------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
! -----------------------------------------------------------------------------
implicit none
! -----------------------------------------------------------------------------
!  Los campos del objeto estimulos son:
!       p_nod = puntero al nodo
!       p_his = puntero a la historia
!       ampl  = amplitud del estimulo
! -----------------------------------------------------------------------------
 type, public :: t_stm
    private
    integer(ip)           :: id_stm
    integer(ip)           :: n_stm
    real(rp), allocatable :: data(:)
  end type t_stm
! ----------------------------------------------------------------------------
  type, public :: t_ndstm
    private
    integer(ip), allocatable :: node(:)
    integer(ip), allocatable :: p_id(:)
  end type t_ndstm
! -----------------------------------------------------------------------------
!  name : nombre del grupo de nodos 
! -----------------------------------------------------------------------------
  type, public :: t_grpstm
    private
    character (len = 32), allocatable  :: name(:)
    integer(ip), allocatable           :: p_id(:)
  end type t_grpstm
! -----------------------------------------------------------------------------
  type, public:: t_obstm
    private
    integer(ip)                 :: nh_stm = 0
    type(t_stm),    allocatable :: hstm(:) 
    type(t_ndstm)               :: ndstm
    type(t_grpstm)              :: grpstm
  end type t_obstm
! -----------------------------------------------------------------------------
contains
! -----------------------------------------------------------------------------
subroutine lecc_stm (lu_m, arch_d, strout, stmls)
! -----------------------------------------------------------------------------
! Esta rutina lee una tabla que contiene:
! >>> stmndexp(:)
! Nodo stm       | Nro. de stm | tinc de stm | duracion del stm | corr. de stm 
! >>> stmndgrp(:)
! Grupo de nodos | Nro. de stm | tinc de stm | duracion del stm | corr. de stm 
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), parameter        :: max=10000
  integer(ip), intent(in)       :: lu_m
  character (len=*), intent(in) :: arch_d, strout
  type(t_obstm)                 :: stmls
  !.
  integer(ip)                   :: lu_lec,io_err,i_err
  integer(ip)                   :: i, j, n_stm, nro_grp, nro_nd
  real(rp)                      :: data(max)


  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (54,'subroutine lec_stm',1)
  !.
  read (lu_lec,*,iostat=io_err) stmls%nh_stm  !.Number of stimulus histories
  if (io_err > 0) call w_error (3,'stmls%nh_stm',1)
  !.
  allocate (stmls%hstm(stmls%nh_stm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'stmls%hstm(stmls%nh_stm)',1)
  !.
  do i=1,stmls%nh_stm
    read (lu_lec,*,iostat=io_err) stmls%hstm(i)%id_stm,n_stm,data(1:3*n_stm)
    if (io_err > 0) call w_error (55,i,1) 
    !.
    allocate(stmls%hstm(i)%data(3*n_stm),stat=i_err)
    if (i_err > 0) call w_error(56, n_stm, 1)
    !.
    stmls%hstm(i)%n_stm = n_stm; stmls%hstm(i)%data(1:3*n_stm)= data(1:3*n_stm)
  end do
  !
  read (lu_lec,*,iostat=io_err) nro_nd, nro_grp !.number of nodes and groups to be stimulated
  if (io_err > 0) call w_error (59,'nro_nd, nro_grp',1)
  !.
  if (nro_nd  > 0) then
    allocate (stmls%ndstm%node(nro_nd),stmls%ndstm%p_id(nro_nd),stat=i_err)
    if (io_err  > 0) call w_error (4,'stmls%ndstm%node(nro_nd),stmls%ndstm%p_id(nro_nd)',1)
    do i=1,nro_nd
      read (lu_lec,*,iostat=io_err) stmls%ndstm%node(i), stmls%ndstm%p_id(i)
      if (io_err > 0) call w_error (60,i,1)
    end do
  end if
  !.
  if (nro_grp > 0) then
    allocate (stmls%grpstm%name(nro_grp),stmls%grpstm%p_id(nro_grp),stat=i_err)
    if (io_err  > 0) call w_error (4,'stmls%grpstm%name(nro_grp),stmls%grpstm%p_id(nro_grp)',1)
    do i=1,nro_grp
      read (lu_lec,*,iostat=io_err) stmls%grpstm%name(i), stmls%grpstm%p_id(i)
      if (io_err > 0) call w_error (61,i,1)
    end do
  end if
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  write (std_o,10) stmls%nh_stm ; write(lu_log,10) stmls%nh_stm 
  write (std_o,*) 'Number of histories: ',stmls%nh_stm
  do i=1,stmls%nh_stm
    n_stm = stmls%hstm(i)%n_stm
    write (std_o,21) stmls%hstm(i)%id_stm,stmls%hstm(i)%n_stm, &
                     (stmls%hstm(i)%data(j),j=1,3),'...',      &
                     (stmls%hstm(i)%data(j),j=3*(n_stm-1)+1,3*n_stm)
  end do
  !.
  return
10 format ('###.Stimuli read                                  : ',I8)
20 format ('   Number of histories: ',I5)
21 format ('   ',2(I5,1X),3(E12.4,1X),A3,3(E12.4,1X))
end subroutine lecc_stm
! ------------------------------------------------------------------------------
function get_stimulus(p_stm, t, stmls) result(Jst)
! -------------------------------------------------------------------------------
!. p_stm: es el puntero a la historia del estimulo
!. t    : tiempo
!. stmls: objeto que contiene los datos del estimulo
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)  :: p_stm
  real(rp),    intent(in)  :: t
  type(t_obstm),intent(in) :: stmls
  !.
  real(rp)                 :: Jst
  !
  integer(ip)              :: j, n_stm
  real(rp)                 :: tis,tst


  Jst=0.0
  n_stm = stmls%hstm(p_stm)%n_stm 
  !.
  do j=1,3*n_stm,3
    tis = stmls%hstm(p_stm)%data(j)      !.Tiempo de inicio del estimulo
    tst = stmls%hstm(p_stm)%data(j+1)    !.Tiempo de estimulo
    if ((t >= tis).and.(t <= (tis+tst))) then
      Jst =  -stmls%hstm(p_stm)%data(j+2)    !.Corriente de estimulo
      exit
    end if
  end do
  return
end function get_stimulus
! -------------------------------------------------------------------------------
subroutine get_stmls_nd(stmls, n, iaux)
! -------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(inout)  :: n, iaux(:)
  type(t_obstm),intent(in)    :: stmls
  integer(ip)                 :: i,k
  !.

  n = 0
  if (allocated(stmls%ndstm%node)) then
    do i=1,size(stmls%ndstm%node)
      k=stmls%ndstm%node(i)
      iaux(k) = stmls%ndstm%p_id(i)
    end do
    n=i-1
  end if
end subroutine get_stmls_nd
! -----------------------------------------------------------------------------
subroutine bin_write_stm (lu_m, ln_d, stmls)
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m, ln_d(:)
  type(t_obstm)                 :: stmls
  !.
  integer(ip)                   :: l_aux(size(stmls%ndstm%node))
  integer(ip)                   :: i, j, nro_nd, cn_stm, inod


 
  !.calculo del numero de nodos estimulados en el dominio
  nro_nd = size(stmls%ndstm%node)
  !.
  cn_stm=0
  if (nro_nd > 0 ) then
    do i=1,nro_nd
      inod = stmls%ndstm%node(i)
      if (ln_d(inod) >  0) then
        cn_stm = cn_stm + 1
        l_aux(cn_stm) = i
      end if
    end do
  end if
  !.
  if (cn_stm > 0) then
    write (lu_m) '*STIMULUS', stmls%nh_stm, cn_stm  
    !.
    do i=1,stmls%nh_stm
      write (lu_m) stmls%hstm(i)%id_stm,stmls%hstm(i)%n_stm
      write (lu_m) stmls%hstm(i)%data
    end do
    !.
    do i=1,cn_stm
      j=l_aux(i)
      inod = stmls%ndstm%node(j)
      write (lu_m)  ln_d(inod), stmls%ndstm%p_id(j)
    end do
  else
    write (lu_m) '*STIMULUS', 0, 0  
  end if
  return
end subroutine bin_write_stm
! -----------------------------------------------------------------------------
subroutine ascii_write_stm (lu_m, ln_d, stmls)
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m, ln_d(:)
  type(t_obstm)                 :: stmls
  !.
  integer(ip)                   :: l_aux(size(stmls%ndstm%node))
  integer(ip)                   :: i, j, nro_nd, cn_stm, inod


  !.calculo del numero de nodos estimulados en el dominio
  nro_nd = size(stmls%ndstm%node)
  !.
  cn_stm=0
  if (nro_nd > 0 ) then
    do i=1,nro_nd
      inod = stmls%ndstm%node(i)
      if (ln_d(inod) >  0) then
        cn_stm = cn_stm + 1
        l_aux(cn_stm) = i
      end if
    end do
  end if
  !.
  if (cn_stm > 0) then
    write (lu_m,10) '*STIMULUS', stmls%nh_stm, cn_stm  
    !. 
    do i=1,stmls%nh_stm
      write (lu_m,20) stmls%hstm(i)%id_stm,stmls%hstm(i)%n_stm
      write (lu_m,30) stmls%hstm(i)%data
    end do
    !.
    do i=1,cn_stm
      j=l_aux(i)
      inod = stmls%ndstm%node(j)
      write (lu_m,20)  ln_d(inod), stmls%ndstm%p_id(j)
    end do
  else
     write (lu_m,10) '*STIMULUS', 0, 0
  end if
  return
10 format (A,2X,I8,2X,I8)
20 format (I8,2X,I8)
30 format (10(:,2X,F12.6))
end subroutine ascii_write_stm
! -----------------------------------------------------------------------------
subroutine bin_read_stm (lu_m, stmls)
! -----------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)       :: lu_m
  type(t_obstm)                 :: stmls
  !.
  character (len=9)             :: str
  integer(ip)                   :: io_err,i_err
  integer(ip)                   :: i, nro_nd

 
  read (lu_m, iostat = io_err) str, stmls%nh_stm, nro_nd  !.Nro de hist. de est. y nodos
  if (io_err > 0) call w_error (3,'stmls%nh_stm',1)
  if (nro_nd > 0) then
    allocate (stmls%hstm(stmls%nh_stm), stat=i_err)
    if (i_err > 0 ) call w_error (4,'stmls%hstm(stmls%nh_stm)',1)
    !.
    if (nro_nd > 0 ) then
      do i=1,stmls%nh_stm
        read (lu_m, iostat=io_err) stmls%hstm(i)%id_stm,stmls%hstm(i)%n_stm
        if (io_err > 0) call w_error (55,i,1) 
        !.
        allocate(stmls%hstm(i)%data(3*stmls%hstm(i)%n_stm),stat=i_err)
        if (i_err > 0) call w_error(56,'stmls%hstm(i)%data(:)', 1)
        !.
        read (lu_m, iostat=io_err) stmls%hstm(i)%data(1:3*stmls%hstm(i)%n_stm)
        if (io_err > 0) call w_error (55,i,1) 
      end do
      !.
      allocate (stmls%ndstm%node(nro_nd),stmls%ndstm%p_id(nro_nd),stat=i_err)
      if (io_err  > 0) call w_error (4,'stmls%ndstm%node(nro_nd),stmls%ndstm%p_id(nro_nd)',1)
      do i=1,nro_nd
        read (lu_m, iostat=io_err) stmls%ndstm%node(i), stmls%ndstm%p_id(i)
        if (io_err > 0) call w_error (60,i,1)
      end do
    end if
  end if
  return
end subroutine bin_read_stm
! -----------------------------------------------------------------------------
function deallocate_stmls (stmls) result(flg)
! -----------------------------------------------------------------------------
  implicit none
  type(t_obstm)             :: stmls
  integer(ip)               :: flg
  !.
  integer(ip)               :: flg_s(2)

  flg=0; flg_s = 0
  flg_s(1) = deallocate_hstm     (stmls)
  flg_s(2) = deallocate_nodep_id (stmls)

  if (any(flg_s == 1)) flg = 1
  return
end function deallocate_stmls
! -----------------------------------------------------------------------------
function deallocate_nodep_id (stmls) result(flg)
! -----------------------------------------------------------------------------
  implicit none
  type(t_obstm)             :: stmls
  integer(ip)               :: flg
  !.
  integer(ip)               ::  istat
  
  flg=0
  if (allocated(stmls%ndstm%node)) then
    deallocate (stmls%ndstm%node, stmls%ndstm%p_id, stat=istat)
    if (istat /= 0) flg = 1
  else
    flg = -1
  end if
  !.
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error liberando memoria en deallocate_nodep_id (mod_stimulus.f90)')  
end function deallocate_nodep_id
! -----------------------------------------------------------------------------
function deallocate_hstm (stmls) result(flg)
! -----------------------------------------------------------------------------
  implicit none
  type(t_obstm)             :: stmls
  integer(ip)               :: flg
  !.
  integer(ip)               :: i, istat

  flg = 0 
  if (allocated(stmls%hstm)) then
    do i=1,stmls%nh_stm
      deallocate (stmls%hstm(i)%data, stat=istat)
      if (istat /= 0) flg = 1
    end do
    deallocate (stmls%hstm, stat=istat)
    if (istat /= 0) flg = 1 
  else
    flg = -1
  end if
  !.
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error liberando memoria en deallocate_hstm (mod_stimulus.f90)')
end function deallocate_hstm
! -----------------------------------------------------------------------------
end module mod_stimulus
! -----------------------------------------------------------------------------
