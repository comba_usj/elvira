module mod_elements
!  ------------------------------------------------------------------------------
  use mod_precision
  use mod_filename
  use mod_error
  use mod_idelem
!  ------------------------------------------------------------------------------
  implicit none
!  ------------------------------------------------------------------------------
!  ------------------------------------------------------------------------------
  type, public :: t_elm
    private
    integer(ip)              :: n_elm  !       element number
    integer(ip)              :: p_pelm !       pointer to elem prop
    integer(ip)              :: tipo   !       tipo de elemento 
    integer(ip), allocatable :: conc(:)!       element connectivity
!    integer(ip), pointer     :: conc(:) => NULL()
  end type t_elm
!  ------------------------------ Elements Graf ---------------------------------
  type, public :: t_eae 
    private
    integer(ip), allocatable :: je_ee(:) ! element graf: neighbor elements
!    integer(ip), pointer :: je_ee(:) => NULL()
  end type t_eae
!  ------------------------------------------------------------------------------
  type, public :: t_obelm
    private
    integer(ip)              :: nelm=0               ! Number of elements
!    integer(ip)              :: nelt                ! Number of element type
!    integer(ip), allocatable :: tip(:)              ! Array of element type
!    integer(ip), allocatable :: num_t(:)            ! Array of numb of elm of each type
    type(t_elm), allocatable :: elm(:)              ! Element info
    type(t_eae), allocatable :: eae(:)              ! Element graf
    integer(ip), allocatable :: elm_d(:)            ! Element loading
!    type(t_elm), pointer     :: elm(:) => NULL()   ! Element info
!    type(t_eae), pointer     :: eae(:) => NULL()   ! Element graf
!    integer(ip), pointer     :: elm_d(:) => NULL() ! Element loading
  end type t_obelm
!.
contains
! ------------------------------------------------------------------------------
subroutine lec_elem (lu_m, arch_d, strout, elem)
! ------------------------------------------------------------------------------
! This soubroutine reads elements from unit lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  character (len=*), intent(in):: arch_d,strout
  type(t_obelm)                :: elem
  !.
  character (len=12)           :: nomb
  integer(ip)                  :: lu_lec,io_err,i_err
  integer(ip)                  :: i, j, k, n_elt, nele, tipo, n_pe

  lu_lec = lu_m; call unitnumber (arch_d, strout, lu_lec, io_err)
  if (io_err > 0) call w_error (7,'subroutine lec_elm',1)
  !.
  read (lu_lec,*,iostat=io_err) nele,n_elt   !.Nro. elem. and Nro type
  if (io_err > 0) call w_error (3,'nelm,nelt',1)

  elem%nelm = nele
  !elem%nelt = n_elt
  allocate (elem%elm(elem%nelm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'elem(elem%nelm)',1)
 ! allocate (elem%tip(elem%nelt), stat=i_err)
 ! if (i_err > 0 ) call w_error (4,'elem(elem%nelt)',1)
 ! allocate (elem%num_t(elem%nelt), stat=i_err)
 ! if (i_err > 0 ) call w_error (4,'elem(elem%num_t)',1)
  !. 
  k=0
  write (std_o,10) nele; write(lu_log,10) nele
  do i=1,n_elt
    read (lu_lec,*,iostat=io_err) nele,nomb
    if (io_err > 0) call w_error (3,'nele, nomb',1)
    call id_par_ele(nomb,tipo=tipo,n_pe=n_pe)
    !.
    do j=1,nele
      k = k + 1; allocate(elem%elm(k)%conc(n_pe),stat=i_err)
      if (i_err > 0) call w_error(4,'elem%elm(k)%conc(n_pe)',1)
      !.
      elem%elm(k)%tipo = tipo
      read (lu_lec,*,iostat=io_err) elem%elm(k)%n_elm,elem%elm(k)%p_pelm,    &
                                    elem%elm(k)%conc(1:n_pe)
      if (io_err > 0) call w_error (6,elem%elm(k)%n_elm,1)
    end do
    write (std_o,*) 'Element type: ',nomb
    write (std_o,20) elem%elm(k-nele+1)%n_elm,elem%elm(k-nele+1)%p_pelm,    &
                     (elem%elm(k-nele+1)%conc(j),j=1,n_pe)
    write (std_o,20) elem%elm(k)%n_elm,elem%elm(k)%p_pelm,    &
                     (elem%elm(k)%conc(j),j=1,n_pe)
  end do
  !.
  if (lu_lec /= lu_m) call close_unit (lu_lec)
  !. 
  return
10 format ('###.Elements, read                                : ',I8)
20 format (22(I7,1X))
end subroutine lec_elem
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function associated_eae(elem) result(flg)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)           :: elem
  logical                 :: flg

  flg = allocated(elem%eae)
!  flg = associated(elem%eae)
  return
end function associated_eae
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine allocate_eae (elem)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)           :: elem
  integer(ip)             :: i_err

  allocate(elem%eae(elem%nelm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'elem%eae(elem%n_elm)',1)
  return
end subroutine allocate_eae
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine allocate_eae_je_ee ( iel, n_eae, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: iel, n_eae
  type(t_obelm)           :: elem
  integer(ip)             :: i_err

  allocate(elem%eae(iel)%je_ee(n_eae), stat=i_err)
  if (i_err > 0 ) call w_error (4,'elem%eae(iel)%je_ee(nro_elm)',1)
  return
end subroutine allocate_eae_je_ee
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_nelm(elem) result(n_elm)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)               :: elem
  integer(ip)                 :: n_elm
  !.
  n_elm = elem%nelm
  return
end function get_nelm
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_numberofelmtype (elem, n_tp, tip, num)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)               :: elem

  integer(ip), intent(out)    :: n_tp, tip(:), num(:)
  integer(ip)                 :: i, tipo, flag, j
!  integer(ip)                 :: n_elt

!  n_tp = elem%nelmt
!  tip(1:n_tp) = elem%tip(1:n_tp)
!  num(1:n_tp) = elem%num(1:n_tp)
  n_tp = 0
  num  = 0
  tip  = 0
  n_tp = 1
  num(n_tp) = 1
  tip(n_tp) = elem%elm(1)%tipo
  do i=2, elem%nelm
     tipo = elem%elm(i)%tipo
     flag = 0
     do j=1,n_tp
        if(tipo == tip(j)) then
            flag=1; exit;
        end if
     end do
     if(flag==1) then
         num(j) = num(j) + 1
     else
         n_tp = n_tp + 1
         num(n_tp) = num(n_tp) + 1
         tip(n_tp) = tipo
     endif
  end do
!
!  n_tp = 0
!  num  = 0
!  do i=1, elem%nelm
!    tipo = elem%elm(i)%tipo
!    n_elt = count(elem%elm(:)%tipo == tipo, dim=1)
!    if (n_elt ==  elem%nelm) then
!      n_tp = n_tp + 1
!      num(n_tp) = n_elt
!      tip(n_tp) = tipo
!      exit
!    else
!      if (.not.any(tip(1:n_tp) == tipo))  then
!        n_tp = n_tp + 1
!        num(n_tp) = n_elt
!        tip(n_tp) = tipo
!      end if
!    end if
!  end do
  return
end subroutine get_numberofelmtype
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_elmconc_tipo (iel, tipo, nro_e, n_pe, conc, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iel, tipo 
  integer(ip), intent(out)    :: nro_e, n_pe, conc(:)
  type(t_obelm)               :: elem

  if (tipo == elem%elm(iel)%tipo) then
    nro_e       = elem%elm(iel)%n_elm
    n_pe        = size(elem%elm(iel)%conc)
    conc(1:n_pe)= elem%elm(iel)%conc(1:n_pe)
  else
    nro_e = 0; n_pe = 0; conc = 0
  end if
  return
end subroutine get_elmconc_tipo
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_elmconc (iel,n_pe,conc,elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iel
  integer(ip), intent(out)    :: n_pe,conc(:)
  type(t_obelm)               :: elem

  n_pe        = size(elem%elm(iel)%conc)
  conc(1:n_pe)= elem%elm(iel)%conc(1:n_pe)
  return
end subroutine get_elmconc
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_elemtipo (iel, elem) result (tipo)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: iel
  type(t_obelm)           :: elem
  integer(ip)             :: tipo

  tipo = elem%elm(iel)%tipo
  return
end function get_elemtipo
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_elemprop(iel,elem) result(p_pelm)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: iel
  type(t_obelm)           :: elem
  integer(ip)             :: p_pelm

  p_pelm = elem%elm(iel)%p_pelm
  return
end function get_elemprop
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine set_eae_je_ee(i, ipos , iel, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: i, ipos, iel
  type(t_obelm)               :: elem

  elem%eae(i)%je_ee(ipos) = iel
  return
end subroutine set_eae_je_ee
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine set_eae_je_ee_vec (i, n_ee, e_st, elem)
! ------------------------------------------------------------------------------
 implicit none
  integer(ip), intent(in)     :: i, n_ee, e_st(:)
  type(t_obelm)               :: elem

  elem%eae(i)%je_ee =  e_st(1:n_ee)
  return
end subroutine set_eae_je_ee_vec
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_eae_je_ee(iel, n_ee, conc, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)     :: iel
  integer(ip), intent(out)    :: n_ee, conc(:)
  type(t_obelm)               :: elem

  n_ee         = size(elem%eae(iel)%je_ee)
  conc(1:n_ee) = elem%eae(iel)%je_ee
  return
end subroutine get_eae_je_ee
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_dimeae_je_ee(elem) result (n_jel)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)               :: elem
  integer(ip)                 :: n_jel, njet
!  integer(8)                  :: n_jel, njet, old_njel
  integer(ip)                 :: i

  n_jel=0;
!  old_njel = 0
!  write(*,*) 'Elemento  nvec'
  do i=1,elem%nelm
    njet = size(elem%eae(i)%je_ee)
    n_jel=n_jel + njet
!    if(old_njel .gt. n_jel) then
!            write(*,*) '********* OVERFLOW **********'
!    endif
!    old_njel = n_jel
!    if (njet.eq.0) then
!       write(*,*) i, njet, '***** 0 *****'
!    else 
!     write(*,40) i, njet, n_jel
!    endif
  end do
!  40 format(3(I22))
  return
end function get_dimeae_je_ee
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine set_element_subdomain(elem, part)
! ------------------------------------------------------------------------------
  implicit none
  type(t_obelm)               :: elem
  integer(ip),intent(in)      :: part(:)
  !
  integer(ip)                 :: i, i_err


  allocate(elem%elm_d(elem%nelm), stat=i_err) 
  if (i_err > 0 ) call w_error (4,'elem%elm_d(elem%nelm)',1)
  !.
  do i=1,elem%nelm 
    elem%elm_d(i) = part(i)
  end do
  !.
  return
end subroutine set_element_subdomain
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine set_elm_dom(iel, idom, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip),intent(in)      :: iel, idom
  type(t_obelm)               :: elem

  write(*,10)  iel, elem%elm_d(iel), idom
  !.
  elem%elm_d(iel) = idom
  return
10 format('###.Changing element: ',I8,' from domain: ',I8,' to domain: ', I8)
end subroutine set_elm_dom
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
function get_elm_subdomain(iel, elem) result(idom)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: iel
  type(t_obelm)           :: elem
  integer(ip)             :: idom

  idom = elem%elm_d(iel)
  return
end function get_elm_subdomain
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine get_eleme_dom_prop (idom, ne_d, np_d, le_d, lp_ed, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in) :: idom
  integer(ip), intent(out):: ne_d, np_d, le_d(:), lp_ed(:)
  type(t_obelm)           :: elem
  !.
  integer(ip)             :: i, i_pe
  

  ne_d = 0; np_d = 0; le_d = 0; lp_ed = 0
  !.
  do i = 1, elem%nelm 
    if ( elem%elm_d(i) == idom ) then
      ne_d = ne_d + 1; le_d(i) = ne_d
      i_pe = elem%elm(i)%p_pelm
      if (lp_ed(i_pe) == 0) then
        np_d = np_d + 1; lp_ed(i_pe) = np_d
      end if
    end if
  end do
  return
end subroutine get_eleme_dom_prop
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine bin_write_elm (lu_m, ne_d, ln_d, le_d, lp_ed, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max = 100
  integer(ip), intent(in) :: lu_m,ne_d, ln_d(:), le_d(:), lp_ed(:)
  type(t_obelm)           :: elem
  !.
  integer(ip)             :: conc(max)
  integer(ip)             :: c_elm, i, p_pelm, n_pe
  
  write (lu_m) '#ELEMENTS', ne_d
  !.
  c_elm = 0
  do i=1,elem%nelm
    if (le_d(i) /=0) then
      c_elm = c_elm + 1
      p_pelm= elem%elm(i)%p_pelm
      ! elem%elm(i)%n_elm o c_elm
      write (lu_m) elem%elm(i)%n_elm,lp_ed(p_pelm),elem%elm(i)%tipo
      !.
      call get_elmconc (i, n_pe, conc, elem)
      call renumber_elm(n_pe, conc, ln_d)
      write (lu_m) conc(1:n_pe)
    end if
  end do
  return
end subroutine bin_write_elm
! ------------------------------------------------------------------------------ 
subroutine renumber_elm(n_pe, conc, ln_d)
! ------------------------------------------------------------------------------ 
  implicit none
  integer(ip), intent(in)    :: n_pe, ln_d(:)
  integer(ip), intent(inout) :: conc(:)
  !.
  integer(ip)                :: i, inod

  do i=1, n_pe
    inod=conc(i)
    conc(i) = ln_d(inod)
  end do
  return
end subroutine renumber_elm
! ------------------------------------------------------------------------------
subroutine bin_read_elm (lu_m, elem)
! ------------------------------------------------------------------------------
! This subroutine reads elements form unit lu_m
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), intent(in)      :: lu_m
  type(t_obelm)                :: elem
  !.
  character (len=9)            :: str
  integer(ip)                  :: io_err,i_err
  integer(ip)                  :: i, n_pe


  read (lu_m, iostat = io_err) str, elem%nelm
  if (io_err > 0) call w_error (3,'str, ne_d',1)
  !.
  allocate (elem%elm(elem%nelm), stat=i_err)
  if (i_err > 0 ) call w_error (4,'elem(elem%nelm)',1)
  !.
  do i = 1, elem%nelm
    read (lu_m, iostat=io_err) elem%elm(i)%n_elm, elem%elm(i)%p_pelm,         &
                               elem%elm(i)%tipo
    !.
    call id_elem_tip(elem%elm(i)%tipo, n_pe = n_pe)
    !.
    allocate(elem%elm(i)%conc(n_pe),stat=i_err)
    if (i_err > 0) call w_error(4,'elem%elem%conc(n_pe)',1)
    !.
    read (lu_m,iostat=io_err) elem%elm(i)%conc(1:n_pe)
  end do
  return
end subroutine bin_read_elm
! ------------------------------------------------------------------------------
function deallocate_elm (elem) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type(t_obelm)                :: elem
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: flg_e(3)

  flg_e = 0
  flg = 0
  if (allocated(elem%elm))   flg_e(1) = deallocate_elm_elm (elem)
!  if (associated(elem%elm))   flg_e(1) = deallocate_elm_elm (elem)
  !
  if (allocated(elem%eae))   flg_e(2) = deallocate_elm_eae (elem)
!  if (associated(elem%eae))   flg_e(2) = deallocate_elm_eae (elem)
  !.
  if (allocated(elem%elm_d)) flg_e(3) = deallocate_elm_elm_d (elem)
!  if (associated(elem%elm_d)) flg_e(3) = deallocate_elm_elm_d (elem)
  !.
  if (any(flg_e == 1)) flg = 1
  return
end function deallocate_elm

! ------------------------------------------------------------------------------
function deallocate_elm_elm (elem) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type(t_obelm)                :: elem
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i, istat, i_err


  if (allocated(elem%elm)) then
!  if (associated(elem%elm)) then
    i_err = 0
    do i=1,elem%nelm
      deallocate (elem%elm(i)%conc, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (elem%elm, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  if (flg == 1) write (*,10)
  return
10 format ('###.Error freeing memory in deallocate_elm_elm (mod_elements.f90)')
end function deallocate_elm_elm
! ------------------------------------------------------------------------------
function deallocate_elm_eae (elem) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type(t_obelm)                :: elem
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: i, istat, i_err


  if (allocated(elem%eae)) then
!  if (associated(elem%eae)) then
    i_err = 0
    do i=1,elem%nelm
      deallocate (elem%eae(i)%je_ee, stat=istat)
      if (istat /= 0) i_err = 1
    end do
    deallocate (elem%eae, stat=istat)
    if (istat == 0) flg = 0 
    if ((istat /= 0).or.(i_err == 1)) flg = 1
  else
    flg = -1
  end if
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_elm_eae (mod_elements.f90)')
end function deallocate_elm_eae
! ------------------------------------------------------------------------------
function deallocate_elm_elm_d (elem) result(flg)
! ------------------------------------------------------------------------------ 
  implicit none
  type(t_obelm)                :: elem
  integer(ip)                  :: flg
  !.
  integer(ip)                  :: istat

  flg = 0
  deallocate (elem%elm_d, stat=istat) 
  if ( istat /= 0 ) flg = 1
  if (flg == 1) write (*,10) 
  return
10 format ('###.Error freeing memory in deallocate_elm_eae (mod_elements.f90)')
end function deallocate_elm_elm_d
! ------------------------------------------------------------------------------
! ------------------------------------------------------------------------------
subroutine ascii_write_elm (lu_m, ne_d, ln_d, le_d, lp_ed, elem)
! ------------------------------------------------------------------------------
  implicit none
  integer(ip), parameter  :: max = 100
  integer(ip), intent(in) :: lu_m,ne_d, ln_d(:), le_d(:), lp_ed(:)
  type(t_obelm)           :: elem
  !.
  integer(ip)             :: conc(max)
  integer(ip)             :: c_elm, i, p_pelm, n_pe
  
  write (lu_m,10) '#ELEMENTS', ne_d
  !.
  c_elm = 0
  do i=1,elem%nelm
    if (le_d(i) /=0) then
      c_elm = c_elm + 1
      p_pelm= elem%elm(i)%p_pelm
      ! elem%elm(i)%n_elm o c_elm
      write (lu_m,20) elem%elm(i)%n_elm,lp_ed(p_pelm),elem%elm(i)%tipo
      !.
      call get_elmconc (i, n_pe, conc, elem)
      call renumber_elm(n_pe, conc, ln_d)
      write (lu_m,30) conc(1:n_pe)
    !  write (lu_m,30) elem%elm(i)%n_elm,lp_ed(p_pelm),elem%elm(i)%tipo, conc(1:n_pe)

    end if
  end do
  return
10 format (A,2X,I8)
20 format (3(2X,I8))
30 format (100(2X,I8))
end subroutine ascii_write_elm
! ------------------------------------------------------------------------------
end module mod_elements
! ------------------------------------------------------------------------------
