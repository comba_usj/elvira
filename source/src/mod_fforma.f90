!  -----------------------------------------------------------------------------
module mod_fforma
!  -----------------------------------------------------------------------------
  use mod_precision
  use mod_error
!  -----------------------------------------------------------------------------
implicit none
!  -----------------------------------------------------------------------------
  public  :: fforma, fforma1d, fforma2d, fforma3d, dfdef3d
  !.
  private :: ff1d_02n, ff1d_03n, ff1d_02n_B, ff2d_03n, ff2d_06n, ff2d_03n_B 
  private :: ff2d_04n, ff2d_08n, ff2d_04n_B, ff3d_04n, ff3d_08n, ff3d_04n_B 
  private :: ff3d_08n_B
!  -----------------------------------------------------------------------------
contains
!  -----------------------------------------------------------------------------
subroutine fforma (name, npe, gp, df, ff)
!  -----------------------------------------------------------------------------
!  tipo  : tipo de elemento, = 1 si tiene burbuja
!  npe   : nodos por elemento
!  gp    : puntos de gauss en 1, 2 o 3 dimensiones
!  df    : derivada de las funciones de forma df(ndim,npe,ntpg)
!  ff    : funciones de forma  df(ndim,npe,ntpg)
!  -----------------------------------------------------------------------------
  implicit none
  character (len=*), intent(in) :: name
  integer(ip),intent(in)        :: npe
  real(rp), intent(in)          :: gp(:,:)
  real(rp),intent(out),optional :: ff(:,:),df(:,:,:)
  integer(ip)                   :: idim, ntpg,i,m
  logical                       :: flg_b = .false.

  m = len_trim(name); 
  if (name(m:m) == 'B') flg_b = .true.
  !.
  idim = size(gp,dim=1); ntpg = size(gp,dim=2)
  !.
  select case (idim)
  case (1)
    do i=1,ntpg
      call fforma1d (flg_b,npe,gp(1,i),ff(:,i),df(1:1,:,i))
    end do
  case (2)
    do i=1,ntpg
      call fforma2d (flg_b,npe,gp(1:2,i),ff(:,i),df(1:2,:,i))
    end do
  case(3)
    do i=1,ntpg
      call fforma3d (flg_b,npe,gp(1:3,i),ff(:,i),df(1:3,:,i))
    end do
  case default
    call w_error (18,idim,1)  
  end select
  return
end subroutine fforma
!  -----------------------------------------------------------------------------
subroutine fforma1d (flg_b, npe, s, ff, df)
!  -----------------------------------------------------------------------------
  implicit none
  logical,     intent(in)           :: flg_b
  integer(ip), intent(in)           :: npe
  real(rp),    intent(in)           :: s
  real(rp),    intent(out),optional :: ff(npe),df(npe)

  if (flg_b) then
    call ff1d_02n_B (s, ff, df)
  else
    select case (npe)
    case (2)
      call ff1d_02n (s,ff,df)
    case (3)
      call ff1d_03n (s,ff,df)
    case default
      call  w_error (14,npe,1)
    end select
  end if
  return
end subroutine fforma1d
!  -----------------------------------------------------------------------------
subroutine fforma2d(flg_b, npe, ss, ff, df)
!  -----------------------------------------------------------------------------
  implicit none
  logical,    intent(in)        :: flg_b
  integer(ip),intent(in)        :: npe
  real(rp), intent(in)          :: ss(2) 
  real(rp),intent(out),optional :: ff(npe),df(2,npe)

  if (flg_b) then
    select case (npe)
    case (4)
      call ff2d_03n_B(ss,ff,df)
    case(5)
      call ff2d_04n_B(ss,ff,df)
    case default
      call w_error (15,npe,1)
    end select
  else
    select case (npe)
    case (3)
      call ff2d_03n (ss,ff,df)
    case (4)
      call ff2d_04n (ss,ff,df)
    case (6) 
      call ff2d_06n (ss,ff,df)
    case (8) 
      call ff2d_08n (ss,ff,df)
    case default
      call w_error (15,npe,1)
    end select
  end if
  return
end subroutine fforma2d
!  -----------------------------------------------------------------------------
subroutine fforma3d (flg_b, npe, ss, ff, df)
!  -----------------------------------------------------------------------------
  implicit none
  logical,     intent(in)       :: flg_b
  integer(ip), intent(in)       :: npe
  real(rp),    intent(in)       :: ss(3)
  real(rp),intent(out),optional :: ff(npe),df(3,npe)


  if (flg_b) then
    select case (npe)
    case (5)
      call ff3d_04n_B(ss,ff,df)
    case (9)
      call ff3d_08n_B(ss,ff,df)
    case default
      call w_error (16,npe,1)
    end select
  else
    select case (npe)
    case (4)
      call ff3d_04n(ss,ff,df)
    case (8)
      call ff3d_08n(ss,ff,df)
    case default
      call w_error (16,npe,1)
    end select
  end if
  return
end subroutine fforma3d
!  -----------------------------------------------------------------------------
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
!  ---------------------- FUNCIONES DE FORMA 1D --------------------------------
!  -----------------------------------------------------------------------------
subroutine ff1d_02n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento unidimensional de 2 nodos
!   Entrada:
!          ss : coordenadas naturales ( -1 < ss < 1 )
!   Salida:
!          ff : funciones de forma (coord. nat)
!          df : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss
  real(rp),intent(out),optional :: ff(2),df(2)

  if (present(ff)) then
    ff(1) =  0.5 - 0.5*ss
    ff(2) =  0.5 + 0.5*ss
  end if
  if (present(df)) then
    df(1) = -0.5
    df(2) =  0.5
  end if
  return
end subroutine ff1d_02n
!  -----------------------------------------------------------------------------
subroutine ff1d_03n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento unidimensional de 3 nodos
!  Entrada:
!          ss : coordenadas naturales ( -1 < ss < 1 )
!   Salida:
!          ff : funciones de forma (coord. nat)
!          df : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss
  real(rp),intent(out),optional :: ff(3),df(3)
  real(rp)                      :: ss2

  if (present(ff)) then
    ss2   =  ss*ss
    ff(1) =  0.5*(ss2 - ss)
    ff(2) =  1.0 - ss2
    ff(3) =  0.5*(ss2 + ss)
  end if
  if (present(df)) then
    df(1) =  ss - 0.5
    df(2) = -ss - ss
    df(3) =  ss + 0.5
  end if
  return
end subroutine ff1d_03n
!  -----------------------------------------------------------------------------
subroutine ff1d_02n_B (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento unidimensional de 2 nodos
!  con burbuja. Paper: the use of 2d enriched element with bubble functions for
!                      finite element analysis.
!  Entrada:
!          ss : coordenadas naturales ( -1 < ss < 1 )
!   Salida:
!          ff : funciones de forma (coord. nat)
!          df : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss
  real(rp),intent(out),optional :: ff(3),df(3)

  if (present(ff)) then
    ff(1) =  0.5 - 0.5*ss -0.5*(1.0 - ss*ss)
    ff(2) =  0.5 + 0.5*ss -0.5*(1.0 - ss*ss)
    ff(3) =  1.0 - ss*ss
  end if
  if (present(df)) then
    df(1) = -0.5 + ss
    df(2) =  0.5 + ss
    df(3) = -2.0*ss
  end if
  return
end subroutine ff1d_02n_B
!  -----------------------------------------------------------------------------
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
!  ---------------------- FUNCIONES DE FORMA 2D --------------------------------
!  -----------------------------------------------------------------------------
subroutine ff2d_03n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento bidimensional triangular 
!  3 nodos
!   Entrada:
!          ss : coordenadas naturales
!   Salida:
!          ff(3)   : funciones de forma (coord. nat)
!          df(2,3) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(3),df(2,3)

  if (present(ff)) then
    ff(1) = 1.0 - ss(1) - ss(2)
    ff(2) = ss(1)
    ff(3) = ss(2)
  end if
  if (present(df)) then
    df(1,1) =-1.0; df(2,1) =-1.0 
    df(1,2) = 1.0; df(2,2) = 0.0
    df(1,3) = 0.0; df(2,3) = 1.0
  end if
  return
end subroutine ff2d_03n
!  -----------------------------------------------------------------------------
subroutine ff2d_06n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento bidimensional triangular de 
!  6 nodos
!   Entrada:
!          ss : coordenadas naturales
!   Salida:
!          ff(6)   : funciones de forma (coord. nat)
!          df(2,6) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(6),df(2,6)

  if (present(ff)) then
    ff(1)= 2.0*(1.0 - ss(1) - ss(2))*(0.5 - ss(1) - ss(2))
    ff(2)= 2.0*ss(1)*(ss(1) - 0.5)
    ff(3)= 2.0*ss(2)*(ss(2) - 0.5)
    ff(4)= 4.0*ss(1)*(1.0 - ss(1) - ss(2))
    ff(5)= 4.0*ss(1)* ss(2)
    ff(6)= 4.0*ss(2)*(1.0 - ss(1) - ss(2))
  end if
  if (present(df)) then
    df(1,1)= -3.0+4.0*(ss(1)+ss(2));   df(2,1)= -3.0+4.0*(ss(1)+ss(2))
    df(1,2)=  4.0*ss(1)-1.0;           df(2,2)=  0.0
    df(1,3)=  0.0;                     df(2,3)=  4.0*ss(2)-1.0
    df(1,4)=  4.0-8.0*ss(1)-4.0*ss(2); df(2,4)= -4.0*ss(1)
    df(1,5)=  4.0*ss(2);               df(2,5)=  4.0*ss(1)
    df(1,6)= -4.0*ss(2);               df(2,6)=  4.0-4.0*ss(1)-8.0*ss(2)
  end if
  return
end subroutine ff2d_06n
!  -----------------------------------------------------------------------------
subroutine ff2d_03n_B (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento bidimensional triangular 
!  3 nodos con burbuja
!   Entrada:
!          ss : coordenadas naturales
!   Salida:
!          ff(4)   : funciones de forma (coord. nat)
!          df(2,4) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(4),df(2,4)
  real(rp)                      :: pn, u_pn, ff_a, df_a, df_b, df_c
  

  pn = ss(1)*ss(2); u_pn= (1.0 - ss(1) - ss(2))
  !.
  if (present(ff)) then
    ff_a  = 9.0*u_pn*pn
    ff(1) = u_pn - ff_a
    ff(2) = ss(1)- ff_a
    ff(3) = ss(2)- ff_a
    ff(4) = 3.0*ff_a
  end if
  !.
  if (present(df)) then
    df_a    = 9.0*u_pn*ss(2); df_b= 9.0*u_pn*ss(1); df_c = 9.0*pn
    !.
    df(1,1) =   df_c - df_a - 1.0; df(2,1) =   df_c - df_b - 1.0
    df(1,2) =   df_c - df_a + 1.0; df(2,2) =   df_c - df_b
    df(1,3) =   df_c - df_a      ; df(2,3) =   df_c - df_b + 1.0
    df(1,4) = (-df_c + df_a)*3.0 ; df(2,4) = (-df_c + df_b)*3.0
  end if
  !.
  return
end subroutine ff2d_03n_B
!  -----------------------------------------------------------------------------
subroutine ff2d_04n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento cuadrilatero de 4 nodos
!   Entrada:
!          ss(2): coordenadas naturales
!   Salida:
!          ff(4)   : funciones de forma (coord. nat)
!          df(2,4) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(4),df(2,4)
  real(rp)                      :: s1p,s1m,s2p,s2m

  s1p= 1.0 + ss(1); s1m= 1.0 - ss(1) 
  s2p= 1.0 + ss(2); s2m= 1.0 - ss(2)
  if (present(ff)) then
    ff(1)= 0.25*s1m*s2m  !.(1-s)*(1-t)/4.0
    ff(2)= 0.25*s1p*s2m  !.(1+s)*(1-t)/4.0
    ff(3)= 0.25*s1p*s2p  !.(1+s)*(1+t)/4.0 
    ff(4)= 0.25*s1m*s2p  !.(1-s)*(1+t)/4.0
  end if
  if (present(df)) then
    df(1,1) = -0.25*s2m; df(2,1) = -0.25*s1m
    df(1,2) =  0.25*s2m; df(2,2) = -0.25*s1p
    df(1,3) =  0.25*s2p; df(2,3) =  0.25*s1p
    df(1,4) = -0.25*s2p; df(2,4) =  0.25*s1m
  end if
  return
end subroutine ff2d_04n
!  -----------------------------------------------------------------------------
subroutine ff2d_08n(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento bidimensional de 8 nodos
!  Entrada:                                                            4 - 7 - 3
!          ss : coordenadas naturales                                  :   T   :
!   Salida:                                                            8   *S  6
!          ff(8)   : funciones de forma (coord. nat)                   :       :
!          df(2,8) : derivada de las funciones de forma (coord. nat)   1 - 5 - 2    
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(8),df(2,8)
  real(rp)                      :: s_p, s_m, t_p, t_m, s_s, t_t

  s_p = 1.0 + ss(1); s_m = 1.0 - ss(1)
  t_p = 1.0 + ss(2); t_m = 1.0 - ss(2)
  !.
  s_s = ss(1)*ss(1); t_t = ss(2)*ss(2)
  !.
  if (present(ff)) then
    ff(1) = 0.25*s_m*t_m*(s_m + t_m - 3.0)
    ff(2) = 0.25*s_p*t_m*(s_p + t_m - 3.0)
    ff(3) = 0.25*s_p*t_p*(s_p + t_p - 3.0)
    ff(4) = 0.25*s_m*t_p*(s_m + t_p - 3.0)
    ff(5) = 0.50*t_m*(1.0 - s_s)
    ff(6) = 0.50*s_p*(1.0 - t_t)
    ff(7) = 0.50*t_p*(1.0 - s_s)
    ff(8) = 0.50*s_m*(1.0 - t_t)
  end if
  if (present(df)) then
    df(1,1)= -0.25*t_m*(s_m+s_m+t_m-3.0); df(2,1)= -0.25*s_m*(t_m+s_m+t_m-3.0)
    df(1,2)=  0.25*t_m*(s_p+s_p+t_m-3.0); df(2,2)= -0.25*s_p*(t_m+s_p+t_m-3.0)
    df(1,3)=  0.25*t_p*(s_p+s_p+t_p-3.0); df(2,3)=  0.25*s_p*(t_p+s_p+t_p-3.0)
    df(1,4)= -0.25*t_p*(s_m+s_m+t_p-3.0); df(2,4)=  0.25*s_m*(t_p+s_m+t_p-3.0)
    df(1,5)= -ss(1)*t_m;                  df(2,5)= -0.50*(1.0-s_s)
    df(1,6)=  0.50*(1.0 - t_t);           df(2,6)= -ss(2)*s_p
    df(1,7)= -ss(1)*t_p;                  df(2,7)=  0.50*(1.0-s_s)
    df(1,8)= -0.50*(1.0 - t_t);           df(2,8)= -ss(2)*s_m
  end if
  return
end subroutine ff2d_08n
!  -----------------------------------------------------------------------------
subroutine ff2d_04n_B(ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y su derivada para un elemento bidimensional de 4 nodos
!  con burbuja.
!   Entrada:
!          ss(2): coordenadas naturales
!   Salida:
!          ff(5)   : funciones de forma (coord. nat)
!          df(2,5) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(2)
  real(rp),intent(out),optional :: ff(5),df(2,5)
  real(rp)                      :: s1p,s1m,s2p,s2m,s1m2,s2m2,s12a,s12b

  s1p= 1.0 + ss(1); s1m= 1.0 - ss(1); s1m2= 1.0 - ss(1)*ss(1)
  s2p= 1.0 + ss(2); s2m= 1.0 - ss(2); s2m2= 1.0 - ss(2)*ss(2)
  if (present(ff)) then
    s12a = 0.25*s1m2*s2m2
    ff(1)= 0.25*s1m*s2m - s12a !0.25*s1m2*s2m2
    ff(2)= 0.25*s1p*s2m - s12a !0.25*s1m2*s2m2
    ff(3)= 0.25*s1p*s2p - s12a !0.25*s1m2*s2m2
    ff(4)= 0.25*s1m*s2p - s12a !0.25*s1m2*s2m2
    ff(5)= s1m2*s2m2
  end if
  if (present(df)) then
     s12a= 0.5*ss(1)*s2m2;      s12b   =  0.5*ss(2)*s1m2
     df(1,1)= -0.25*s2m + s12a; df(2,1)= -0.25*s1m + s12b
     df(1,2)=  0.25*s2m + s12a; df(2,2)= -0.25*s1p + s12b
     df(1,3)=  0.25*s2p + s12a; df(2,3)=  0.25*s1p + s12b
     df(1,4)= -0.25*s2p + s12a; df(2,4)=  0.25*s1m + s12b
     df(1,5)= -2.00*s2m2*ss(1); df(2,5)= -2.00*s1m2*ss(2)
  end if
  return
end subroutine ff2d_04n_B
!  -----------------------------------------------------------------------------
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
!  ---------------------- FUNCIONES DE FORMA 3D --------------------------------
!  -----------------------------------------------------------------------------
subroutine ff3d_04n (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y sus derivada en coordenadas naturales de un tetraedro 
!  lineal de 4 nodos
!  Entrada:
!          ss : coordenadas naturales
!  Salida:
!          ff(4)   : funciones de forma (coord. nat)
!          df(3,4) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(3)
  real(rp),intent(out),optional :: ff(4),df(3,4)

 if (present(ff)) then
    ff(1) = 1.0 - ss(1) - ss(2) - ss(3)
    ff(2) = ss(1)
    ff(3) = ss(2)
    ff(4) = ss(3)
  end if
  if (present(df)) then
    df(1,1)= -1.0; df(2,1)= -1.0; df(3,1)= -1.0  
    df(1,2)=  1.0; df(2,2)=  0.0; df(3,2)=  0.0 
    df(1,3)=  0.0; df(2,3)=  1.0; df(3,3)=  0.0 
    df(1,4)=  0.0; df(2,4)=  0.0; df(3,4)=  1.0 
  end if
  return
end subroutine ff3d_04n
!  -----------------------------------------------------------------------------
subroutine ff3d_08n (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y sus derivadas en coordenadas naturales de un hexaedro 
!  lineal de 8 nodos
!  Entrada:
!          ss : coordenadas naturales
!  Salida:
!          ff(8)   : funciones de forma (coord. nat)
!          df(3,8) : derivada de las funciones de forma (coord. nat) 
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(:)
  real(rp),intent(out),optional :: ff(8),df(3,8)
  real(rp)                      :: ap1,am1,ap2,am2,ap3,am3,c1,c2,c3

  ap1 = 1.0 + ss(1); am1 = 1.0 - ss(1)
  ap2 = 1.0 + ss(2); am2 = 1.0 - ss(2)
  ap3 = 1.0 + ss(3); am3 = 1.0 - ss(3)
  if (present(ff)) then
    c1   = 0.125d0*am1*am2; ff(1)= c1*am3; ff(5)= c1*ap3
    c1   = 0.125d0*ap1*ap2; ff(3)= c1*am3; ff(7)= c1*ap3
    c1   = 0.125d0*am1*ap2; ff(4)= c1*am3; ff(8)= c1*ap3
    c1   = 0.125d0*ap1*am2; ff(2)= c1*am3; ff(6)= c1*ap3
  end if
  if (present(df)) then
    c1     =  0.125d0*am1*am2; c2     =  0.125d0*am2*am3
    c3     =  0.125d0*am1*am3; df(1,1)= -c2; df(1,2)= c2
    df(2,1)= -c3; df(2,4)= c3; df(3,1)= -c1; df(3,5)= c1
    c1     =  0.125d0*ap1*ap2; c2     =  0.125d0*ap2*ap3
    c3     =  0.125d0*ap1*ap3; df(1,8)= -c2; df(1,7)= c2
    df(2,6)= -c3; df(2,7)= c3; df(3,3)= -c1; df(3,7)= c1
    c1     =  0.125d0*am1*ap2; c2     =  0.125d0*am2*ap3
    c3     =  0.125d0*am1*ap3; df(1,5)= -c2; df(1,6)= c2
    df(2,5)= -c3; df(2,8)= c3; df(3,4)= -c1; df(3,8)= c1
    c1     =  0.125d0*ap1*am2; c2     =  0.125d0*ap2*am3
    c3     =  0.125d0*ap1*am3; df(1,4)= -c2; df(1,3)= c2
    df(2,2)= -c3; df(2,3)= c3; df(3,2)= -c1; df(3,6)= c1
  end if
  return
end subroutine ff3d_08n
!  -----------------------------------------------------------------------------
subroutine ff3d_04n_B (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y sus derivada en coordenadas naturles de un tetraedro de
!  4 nodos con burbuja
!  Entrada:
!          ss : coordenadas naturales
!  Salida:
!          ff(5)   : funciones de forma (coord. nat)
!          df(3,5) : derivada de las funciones de forma (coord. nat)
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(3)
  real(rp),intent(out),optional :: ff(5),df(3,5)
  !.
  real(rp)                      :: u_rst, rst, ff_a, df_1, df_2, df_3

  u_rst= 1.0 - ss(1) - ss(2) - ss(3);  rst  = 64.0*ss(1)*ss(2)*ss(3)
  !.
  if (present(ff)) then
    ff_a  = rst*u_rst 
    ff(1) = u_rst - ff_a
    ff(2) = ss(1) - ff_a
    ff(3) = ss(2) - ff_a
    ff(4) = ss(3) - ff_a
    ff(5) = 4.0*ff_a
  end if
  if (present(df)) then
    df_1=64.0*ss(2)*ss(3)*u_rst; df_2=64.0*ss(1)*ss(3)*u_rst;df_3=64.0*ss(1)*ss(2)*u_rst;
    !.
    df(1,1)=rst - df_1 - 1.0; df(2,1)=rst - df_2 - 1.0; df(3,1)=rst - df_3 - 1.0
    df(1,2)=rst - df_1 + 1.0; df(2,2)=rst - df_2;       df(3,2)=rst - df_3
    df(1,3)=rst - df_1      ; df(2,3)=rst - df_2 + 1.0; df(3,3)=rst - df_3
    df(1,4)=rst - df_1      ; df(2,4)=rst - df_2      ; df(3,4)=rst - df_3 + 1.0
    df(1,5)=(-rst + df_1)*4.0;df(2,5)=(-rst + df_2)*4.0;df(3,5)=(-rst + df_3)*4.0;
  end if
  return
end subroutine ff3d_04n_B
!  -----------------------------------------------------------------------------
subroutine ff3d_08n_B (ss,ff,df)
!  -----------------------------------------------------------------------------
!  Funcion de forma y sus derivadas en coordenadas naturales de un hexaedro 
!  de 8 nodos con burbuja
!  Entrada:
!          ss : coordenadas naturales
!  Salida:
!          ff(9)   : funciones de forma (coord. nat)
!          df(3,9) : derivada de las funciones de forma (coord. nat) 
!  -----------------------------------------------------------------------------
  implicit none
  real(rp), intent(in)          :: ss(3)
  real(rp),intent(out),optional :: ff(9),df(3,9)
  real(rp)                      :: ap1,am1,ap2,am2,ap3,am3,c1,c2,c3,           &
                                   am1_2,am2_2,am3_2,s_a,s_b,s_c

  ap1 = 1.0 + ss(1); am1 = 1.0 - ss(1);am1_2= 1.0 - ss(1)*ss(1)
  ap2 = 1.0 + ss(2); am2 = 1.0 - ss(2);am2_2= 1.0 - ss(2)*ss(2)
  ap3 = 1.0 + ss(3); am3 = 1.0 - ss(3);am3_2= 1.0 - ss(3)*ss(3)
  if (present(ff)) then
    s_a=   0.125*am1_2*am2_2*am3_2
    c1   = 0.125d0*am1*am2; ff(1)= c1*am3 - s_a; ff(5)= c1*ap3 - s_a
    c1   = 0.125d0*ap1*ap2; ff(3)= c1*am3 - s_a; ff(7)= c1*ap3 - s_a
    c1   = 0.125d0*am1*ap2; ff(4)= c1*am3 - s_a; ff(8)= c1*ap3 - s_a
    c1   = 0.125d0*ap1*am2; ff(2)= c1*am3 - s_a; ff(6)= c1*ap3 - s_a
    ff(9)= am1_2*am2_2*am3_2
  end if
  if (present(df)) then
    s_a=   0.25*ss(1)*am2_2*am3_2
    s_b=   0.25*ss(2)*am1_2*am3_2
    s_c=   0.25*ss(3)*am1_2*am2_2
    c1     =  0.125d0*am1*am2; c2     =  0.125d0*am2*am3
    c3     =  0.125d0*am1*am3; 
    df(1,1)= -c2 + s_a;        df(1,2)= c2 + s_a
    df(2,1)= -c3 + s_b;        df(2,4)= c3 + s_b
    df(3,1)= -c1 + s_c;        df(3,5)= c1 + s_c
    c1     =  0.125d0*ap1*ap2; c2     =  0.125d0*ap2*ap3
    c3     =  0.125d0*ap1*ap3;
    df(1,8)= -c2 + s_a;        df(1,7)= c2 + s_a
    df(2,6)= -c3 + s_b;        df(2,7)= c3 + s_b
    df(3,3)= -c1 + s_c;        df(3,7)= c1 + s_c
    c1     =  0.125d0*am1*ap2; c2     =  0.125d0*am2*ap3
    c3     =  0.125d0*am1*ap3; 
    df(1,5)= -c2 + s_a;        df(1,6)= c2 + s_a
    df(2,5)= -c3 + s_b;        df(2,8)= c3 + s_b
    df(3,4)= -c1 + s_c;        df(3,8)= c1 + s_c
    c1     =  0.125d0*ap1*am2; c2     =  0.125d0*ap2*am3
    c3     =  0.125d0*ap1*am3; 
    df(1,4)= -c2 + s_a;        df(1,3)= c2 + s_a
    df(2,2)= -c3 + s_b;        df(2,3)= c3 + s_b
    df(3,2)= -c1 + s_c;        df(3,6)= c1 + s_c
    df(1,9)= -2.0*am2_2*am3_2*ss(1)
    df(2,9)= -2.0*am1_2*am3_2*ss(2)
    df(3,9)= -2.0*am1_2*am2_2*ss(3)
  end if
  return
end subroutine ff3d_08n_B
!  -----------------------------------------------------------------------------
!  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
!  -----------------------------------------------------------------------------
!  -----------------------------------------------------------------------------
subroutine dfdef3d (df,FG,dfdef)
!  -----------------------------------------------------------------------------
! Calculates the derivative of shape functions in the deformed configuration
!
! df    : derivative of shape functions in reference configuration
! FG    : deformation gradient
! dfdef : derivative of shape functions in deformed configuration
!
!  -----------------------------------------------------------------------------
  use mod_genfunc
  implicit none
  real(rp), intent(in)   :: df(:,:),FG(:,:)
  real(rp), intent(out)  :: dfdef(size(df,dim=1),size(df,dim=2))
  real(rp)               :: invFG(3,3)

  invFG=inv(FG)
  dfdef=matmul(invFG,df)
  return
end subroutine dfdef3d
!  -----------------------------------------------------------------------------
end module mod_fforma
!  -----------------------------------------------------------------------------


