This directory contains the libraries required by ELvira
for partitioning the computational domain as well as for
solving the linear system of equations at each iteration.

The distribution comes with the libraries already compiled 
for linux with the intel V13 and gcc V4.8 compiler for 
infiniband and openmpi. It also comes with the libraries 
compiled on a MacOSX V10.7.5 using gfortran 44.8.

In case the libraries have to be compiled. Follow these
instructions

BLAS Libraries:

These are vanila versionsions of the BLAS libraries
They are required by PSBLAS libraries 

- tar -xvf blas.tgz
- make FORTRAN=<compiler you are using>
- Copy all .a files to the corresponding folder 
  libs_openmpi_intel/ or libs_infiniband_intel/ or whatever

If your system has the BLAS and SCALAPACK libraries 
implemented, it is better to use this instead of the
vanilla version.

METIS Libraries:

These libraries allow for partitioning the FE mesh
in several domains required for parallel computing

- tar -xvf ParMetis-3.1.1.tar
- Update the varaibles CC and LIBDIR in the file Makefile.in
- make 
- Copy all .a files to the corresponding folder 
  libs_openmpi_intel/ or libs_infiniband_intel/ or whatever

More information regarding compilation can be found 
within the ParMet-3.1.1 folder

PSBLAS Libraries:

These libraries allow for the numerical solution of
the system of equations in parallel

-tar -xvf psblas-2.4.0-4.tar
- CC=mpicc FC=ifort MPIFC=mpif90 ./configure --with-blas=<path to the
  BLAS library>
- make
- copy all the content of the folder Krylov/ to the corresponding
  libs_openmpi_intel/ or libs_infiniband_intel/ or whatever

More information regarding compilation can be found 
within the psblas-2.4.0-4 folder
