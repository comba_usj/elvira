#!/bin/sh

# FIXME: preliminary testing suite script.

if grep PSBFDEFINES.*SERIAL_MPI Make.inc ; then
run="sh -c ./ppde"
else
mr=mpirun
run="$mr -np 1 ./ppde "
fi

FORMATS="CSR"
if grep HAVE_RSB_KERNELS Make.inc ; then
FORMATS="$FORMATS RSB-1x1-r"
fi


f=bppde.inp

mkinp()
{
cat > $f  << EOF
7                      Number of entries below this
$METHOD               Iterative method BICGSTAB CGS  BICG BICGSTABL RGMRES
$PREC                   Preconditioner NONE  DIAG  BJAC 
$FORMAT                    Storage format for matrix A:  CSR COO JAD 
$SIZE                    Domain size (actual system is this**3)
1                      Stopping criterion
$MAXIT                     MAXIT
-1                     ITRACE
20                     IRST    restart for RGMRES  and BiCGSTABL
EOF
}

#make || exit
cd test/pargen/ || exit
#make || exit
cd runs || exit

#FORMATS="RSB-1x1-r"
for FORMAT in $FORMATS ; do
for METHOD in CGS BICGSTAB BICG RGMRES BICGSTABL ;
#for METHOD in CGS BICGSTAB ;
#for METHOD in  BICG RGMRES BICGSTABL ;
#for METHOD in CGS  ;
#for METHOD in BICGSTAB ;
do

#for PREC in DIAG ; 
#for PREC in DIAG BJAC ; 
#for PREC in NONE ;
for PREC in NONE BJAC DIAG ; 
#for PREC in BJAC DIAG ; 
#for PREC in BJAC ; 
do
#for SIZE in 1 2 3 4 5 6 7 8 9 ; 
#for SIZE in 2 3 4 5 6 7 8 9 ; 
#for SIZE in 1
#for SIZE in 4 5 6 7 8 9 ; 
#for SIZE in 1 2 3 ; 
for SIZE in 4 40 ; 
#for SIZE in 9 ; 
do
	#SIZE=$((2*3*5*6)) # use this for preconditioner timings messurements


	MAXIT=200
#	METHOD=CGS
	echo FORMAT: $FORMAT
	mkinp 
	$run < $f

	if test x$? != x0 ; then exit ; fi
done
done
done

done
