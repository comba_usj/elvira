!!$ 
!!$              Parallel Sparse BLAS  version 2.2
!!$    (C) Copyright 2006/2007/2008
!!$                       Salvatore Filippone    University of Rome Tor Vergata
!!$                       Alfredo Buttari        University of Rome Tor Vergata
!!$ 
!!$  Redistribution and use in source and binary forms, with or without
!!$  modification, are permitted provided that the following conditions
!!$  are met:
!!$    1. Redistributions of source code must retain the above copyright
!!$       notice, this list of conditions and the following disclaimer.
!!$    2. Redistributions in binary form must reproduce the above copyright
!!$       notice, this list of conditions, and the following disclaimer in the
!!$       documentation and/or other materials provided with the distribution.
!!$    3. The name of the PSBLAS group or the names of its contributors may
!!$       not be used to endorse or promote products derived from this
!!$       software without specific written permission.
!!$ 
!!$  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
!!$  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
!!$  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!!$  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PSBLAS GROUP OR ITS CONTRIBUTORS
!!$  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
!!$  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
!!$  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
!!$  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
!!$  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
!!$  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
!!$  POSSIBILITY OF SUCH DAMAGE.
!!$ 
!!$  
! File: psb_cfree.f90
!
! Subroutine: psb_cfree
!    frees a dense matrix structure
! 
! Arguments: 
!    x(:,:)   - complex, allocatable          The dense matrix to be freed.
!    desc_a   - type(psb_desc_type).        The communication descriptor.
!    info     - integer.                      Return code
subroutine psb_cfree(x, desc_a, info)
  !...free dense matrix structure...
  use psb_const_mod
  use psb_descriptor_type
  use psb_error_mod
  use psb_penv_mod
  implicit none

  !....parameters...
  complex(psb_spk_),allocatable, intent(inout)    :: x(:,:)
  type(psb_desc_type), intent(in) :: desc_a
  integer                     :: info

  !...locals....
  integer             :: ictxt,np,me, err_act
  character(len=20)   :: name


  if(psb_get_errstatus() /= 0) return 
  info=psb_success_
  call psb_erractionsave(err_act)
  name='psb_cfree'
 if (.not.allocated(desc_a%matrix_data)) then
     info=psb_err_forgot_spall_
     call psb_errpush(info,name)
     return
  end if

  ictxt=psb_cd_get_context(desc_a)

  call psb_info(ictxt, me, np)
  !     ....verify blacs grid correctness..
  if (np == -1) then
    info=psb_err_context_error_
    call psb_errpush(info,name)
    goto 9999
  endif

  if (.not.allocated(x)) then
     info=psb_err_forgot_spall_
     call psb_errpush(info,name)
     goto 9999
  end if

  !deallocate x
  deallocate(x,stat=info)
  if (info /= psb_no_err_) then
    info=psb_err_alloc_dealloc_
    call psb_errpush(info,name)
    goto 9999
  endif
  

  call psb_erractionrestore(err_act)
  return

9999 continue
  call psb_erractionrestore(err_act)
  if (err_act == psb_act_abort_) then
     call psb_error(ictxt)
     return
  end if
  return

end subroutine psb_cfree



! Subroutine: psb_cfreev
!    frees a dense matrix structure
! 
! Arguments: 
!    x(:)     - complex, allocatable        The dense matrix to be freed.
!    desc_a   - type(psb_desc_type).      The communication descriptor.
!    info     - integer.                    Return code
subroutine psb_cfreev(x, desc_a, info)
  !...free dense matrix structure...
  use psb_const_mod
  use psb_descriptor_type
  use psb_error_mod
  use psb_penv_mod

  implicit none
  !....parameters...
  complex(psb_spk_),allocatable, intent(inout)    :: x(:)
  type(psb_desc_type), intent(in) :: desc_a
  integer                     :: info

  !...locals....
  integer             :: ictxt,np,me, err_act
  character(len=20)   :: name


  if(psb_get_errstatus() /= 0) return 
  info=psb_success_
  call psb_erractionsave(err_act)
  name='psb_cfreev'


  if (.not.allocated(desc_a%matrix_data)) then
     info=psb_err_forgot_spall_
     call psb_errpush(info,name)
     goto 9999
  end if
  ictxt=psb_cd_get_context(desc_a)

  call psb_info(ictxt, me, np)
  if (np == -1) then
     info=psb_err_context_error_
     call psb_errpush(info,name)
     goto 9999

  endif

  if (.not.allocated(x)) then
     info=psb_err_forgot_spall_
     call psb_errpush(info,name)
     goto 9999
  end if

  !deallocate x
  deallocate(x,stat=info)
  if (info /= psb_no_err_) then
     info=psb_err_alloc_dealloc_
     call psb_errpush(info,name)
  endif
  
  call psb_erractionrestore(err_act)
  return

9999 continue
  call psb_erractionrestore(err_act)

  if (err_act == psb_act_ret_) then
     return
  else
     call psb_error(ictxt)
  end if
  return

end subroutine psb_cfreev
