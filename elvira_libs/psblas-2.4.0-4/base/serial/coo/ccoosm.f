C
C             Parallel Sparse BLAS  version 2.2
C   (C) Copyright 2006/2007/2008
C                      Salvatore Filippone    University of Rome Tor Vergata
C                      Alfredo Buttari        University of Rome Tor Vergata
C
C Redistribution and use in source and binary forms, with or without
C modification, are permitted provided that the following conditions
C are met:
C   1. Redistributions of source code must retain the above copyright
C      notice, this list of conditions and the following disclaimer.
C   2. Redistributions in binary form must reproduce the above copyright
C      notice, this list of conditions, and the following disclaimer in the
C      documentation and/or other materials provided with the distribution.
C   3. The name of the PSBLAS group or the names of its contributors may
C      not be used to endorse or promote products derived from this
C      software without specific written permission.
C
C THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
C ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
C TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
C PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PSBLAS GROUP OR ITS CONTRIBUTORS
C BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
C CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
C SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
C INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
C CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
C ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
C POSSIBILITY OF SUCH DAMAGE.
C
C 
      SUBROUTINE CCOOSM(TRANST,M,N,UNITD,D,ALPHA,DESCRA,A,IA,JA,INFOA,
     *                  B,LDB,BETA,C,LDC,WORK,LWORK,IERROR)
      use psb_error_mod
      use psb_const_mod
      use psb_string_mod
      IMPLICIT NONE
      complex(psb_spk_)  ALPHA, BETA
      INTEGER           LDB, LDC, LWORK, M, N, IERROR
      CHARACTER         UNITD, TRANST
      complex(psb_spk_)        A(*), B(LDB,*), C(LDC,*), D(*), WORK(*)
      INTEGER           IA(*), JA(*), INFOA(*), INT_VAL(5)
      CHARACTER         DESCRA*11
      INTEGER           I, K, ERR_ACT
      CHARACTER         DIAG, UPLO
      INTRINSIC         DBLE, IDINT
      CHARACTER*20      NAME
      integer              :: debug_level, debug_unit

      NAME = 'CCOOSM\0'
      IERROR = psb_success_
      CALL FCPSB_ERRACTIONSAVE(ERR_ACT)
      debug_unit  = psb_get_debug_unit()
      debug_level = psb_get_debug_level()

      if (debug_level >= psb_debug_serial_comp_)
     +  write(debug_unit,*) trim(name),':' ,m,ierror
      
      UPLO = '?'
      IF (psb_toupper(DESCRA(1:1)).EQ.'T' .AND.
     +  psb_toupper(DESCRA(2:2)).EQ.'U') UPLO = 'U'
      IF (psb_toupper(DESCRA(1:1)).EQ.'T' .AND.
     +  psb_toupper(DESCRA(2:2)).EQ.'L') UPLO = 'L'
      IF (UPLO.EQ.'?') THEN
        IERROR=psb_err_invalid_input_
        CALL FCPSB_ERRPUSH(IERROR,NAME,INT_VAL)
        GOTO 9999
      END IF
      IF (psb_toupper(DESCRA(3:3)).EQ.'N') DIAG = 'N'
      IF (psb_toupper(DESCRA(3:3)).EQ.'U') DIAG = 'U'
      IF(psb_toupper(UNITD).EQ.'B') THEN
        IERROR=psb_err_invalid_input_
        CALL FCPSB_ERRPUSH(IERROR,NAME,INT_VAL)
        GOTO 9999
      ENDIF
      IF (psb_toupper(UNITD).EQ.'R') THEN
        do i = 1, n
          do k = 1, m
            b(k,i) = b(k,i)*d(k)
          end do
        end do
      end if

      if ((alpha.ne.cone) .or.(beta .ne.czero)) then 
        if (lwork .lt. m) then           
          int_val(1) = 17
          IERROR=psb_err_invalid_input_
          CALL FCPSB_ERRPUSH(IERROR,NAME,INT_VAL)
          GOTO 9999
        END IF
        do i = 1, n
          call ccoosv(uplo,transt,diag,m,a,ia,ja,infoa,
     +      b(1,i),work,ierror)
          if (psb_toupper(UNITD).EQ.'L') THEN
            do k=1,m
              c(k,i) = beta*c(k,i) + alpha*d(k)*work(k)
            enddo
          else
            do k=1,m
              c(k,i) = beta*c(k,i) + alpha*work(k)
            enddo
          end if
        enddo        

      else
        do i = 1, n
          call ccoosv(uplo,transt,diag,m,a,ia,ja,infoa,
     +      b(1,i),c(1,i),ierror)
        end do
        if(IERROR.NE.0) THEN
          INT_VAL(1)=IERROR
          CALL FCPSB_ERRPUSH(psb_err_from_subroutine_i_,NAME,INT_VAL)
          GOTO 9999
        END IF
        
        IF (UNITD.EQ.'L') THEN
          do i = 1, n
            do  k = 1, m
              c(k,i) = c(k,i)*d(k)
            end do 
          end do
        end if
      end if
      CALL FCPSB_ERRACTIONRESTORE(ERR_ACT)
      RETURN

 9999 CONTINUE
      CALL FCPSB_ERRACTIONRESTORE(ERR_ACT)

      IF ( ERR_ACT .NE. 0 ) THEN 
        CALL FCPSB_SERROR()
        RETURN
      ENDIF

      RETURN
      END
