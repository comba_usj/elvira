!!$ 
!!$              Parallel Sparse BLAS  version 2.2
!!$    (C) Copyright 2006/2007/2008
!!$                       Salvatore Filippone    University of Rome Tor Vergata
!!$                       Alfredo Buttari        University of Rome Tor Vergata
!!$ 
!!$  Redistribution and use in source and binary forms, with or without
!!$  modification, are permitted provided that the following conditions
!!$  are met:
!!$    1. Redistributions of source code must retain the above copyright
!!$       notice, this list of conditions and the following disclaimer.
!!$    2. Redistributions in binary form must reproduce the above copyright
!!$       notice, this list of conditions, and the following disclaimer in the
!!$       documentation and/or other materials provided with the distribution.
!!$    3. The name of the PSBLAS group or the names of its contributors may
!!$       not be used to endorse or promote products derived from this
!!$       software without specific written permission.
!!$ 
!!$  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
!!$  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
!!$  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!!$  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PSBLAS GROUP OR ITS CONTRIBUTORS
!!$  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
!!$  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
!!$  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
!!$  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
!!$  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
!!$  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
!!$  POSSIBILITY OF SUCH DAMAGE.
!!$ 
!!$  

      subroutine psb_csp_free(a,info)
         use psb_string_mod, only : psb_get_fmt
         use psb_rsb_mod, psb_protect_name => psb_csp_free
         use psb_error_mod
         use psb_realloc_mod
         use psb_const_mod
         implicit none
         !....Parameters...
         Type(psb_cspmat_type), intent(inout)  :: A
         Integer, intent(out)        :: info
     
         info=psb_success_
     
         if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
           call psb_rsb_destroy_sparse_matrix(a,info)
         endif
         if (allocated(a%aspk)) then
           deallocate(a%aspk,STAT=INFO)
         endif
         if (allocated(a%ia1)) then
           deallocate(a%ia1,STAT=INFO)
         endif
         if ( allocated(a%ia2)) then
           deallocate(a%ia2,STAT=INFO)
         endif
         if ( allocated(a%pr)) then
           deallocate(a%pr,STAT=INFO)
         endif
         if ( allocated(a%pl)) then
           deallocate(a%pl,STAT=INFO)
         endif
         call psb_nullify_sp(a)
         Return
       End Subroutine psb_csp_free
     
     
     
       subroutine psb_ssp_free(a,info)
         use psb_string_mod, only : psb_get_fmt
         use psb_rsb_mod, psb_protect_name => psb_ssp_free
         use psb_error_mod
         use psb_realloc_mod
         use psb_const_mod
         implicit none
         !....Parameters...
         Type(psb_sspmat_type), intent(inout)  :: A
         Integer, intent(out)        :: info
         !locals
         integer             :: iret
         info=psb_success_
     
         if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
           call psb_rsb_destroy_sparse_matrix(a,info)
         endif
         if (allocated(a%aspk)) then
     !!$      write(0,*) 'Deallocating aspk'
           deallocate(a%aspk,STAT=IRET)
     !!$      write(0,*) 'Deallocated  aspk',iret
           if (iret /= psb_success_) info = max(info,1)
         endif
         if (allocated(a%ia1)) then
           deallocate(a%ia1,STAT=IRET)
           if (iret /= psb_success_) info = max(info,2)
         endif
         if (allocated(a%ia2)) then
           deallocate(a%ia2,STAT=IRET)
           if (iret /= psb_success_) info = max(info,3)
         endif
         if (allocated(a%pr)) then
           deallocate(a%pr,STAT=IRET)
           if (iret /= psb_success_) info = max(info,4)
         endif
         if (allocated(a%pl)) then
           deallocate(a%pl,STAT=IRET)
           if (iret /= psb_success_) info = max(info,5)
         endif
         call psb_nullify_sp(a)
     !!$    write(0,*) 'End of sp_free ',info
         Return
       End Subroutine psb_ssp_free
     
     
       subroutine psb_zsp_free(a,info)
         use psb_string_mod, only : psb_get_fmt
         use psb_rsb_mod, psb_protect_name => psb_zsp_free
         use psb_error_mod
         use psb_realloc_mod
         use psb_const_mod
         implicit none
         !....Parameters...
         Type(psb_zspmat_type), intent(inout)  :: A
         Integer, intent(out)        :: info
     
         info=psb_success_
     
         if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
           call psb_rsb_destroy_sparse_matrix(a,info)
         endif
         if (allocated(a%aspk)) then
           deallocate(a%aspk,STAT=INFO)
         endif
         if (allocated(a%ia1)) then
           deallocate(a%ia1,STAT=INFO)
         endif
         if ( allocated(a%ia2)) then
           deallocate(a%ia2,STAT=INFO)
         endif
         if ( allocated(a%pr)) then
           deallocate(a%pr,STAT=INFO)
         endif
         if ( allocated(a%pl)) then
           deallocate(a%pl,STAT=INFO)
         endif
         call psb_nullify_sp(a)
         Return
       End Subroutine psb_zsp_free



         subroutine psb_dsp_free(a,info)
         use psb_string_mod, only : psb_get_fmt
         use psb_rsb_mod, psb_protect_name => psb_dsp_free
         use psb_error_mod
         use psb_realloc_mod
         use psb_const_mod
         implicit none
         !....Parameters...
         Type(psb_dspmat_type), intent(inout)  :: A
         Integer, intent(out)        :: info
         !locals
         integer             :: iret
         info=psb_success_
     
         if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
           call psb_rsb_destroy_sparse_matrix(a,info)
         endif
         if (allocated(a%aspk)) then
     !!$      write(0,*) 'Deallocating aspk'
           deallocate(a%aspk,STAT=IRET)
     !!$      write(0,*) 'Deallocated  aspk',iret
           if (iret /= psb_success_) info = max(info,1)
         endif
         if (allocated(a%ia1)) then
           deallocate(a%ia1,STAT=IRET)
           if (iret /= psb_success_) info = max(info,2)
         endif
         if (allocated(a%ia2)) then
           deallocate(a%ia2,STAT=IRET)
           if (iret /= psb_success_) info = max(info,3)
         endif
         if (allocated(a%pr)) then
           deallocate(a%pr,STAT=IRET)
           if (iret /= psb_success_) info = max(info,4)
         endif
         if (allocated(a%pl)) then
           deallocate(a%pl,STAT=IRET)
           if (iret /= psb_success_) info = max(info,5)
         endif
         call psb_nullify_sp(a)
     !!$    write(0,*) 'End of sp_free ',info
         Return
       End Subroutine psb_dsp_free


