!!$ 
!!$              Parallel Sparse BLAS  version 2.2
!!$    (C) Copyright 2006/2007/2008
!!$                       Salvatore Filippone    University of Rome Tor Vergata
!!$                       Alfredo Buttari        University of Rome Tor Vergata
!!$ 
!!$  Redistribution and use in source and binary forms, with or without
!!$  modification, are permitted provided that the following conditions
!!$  are met:
!!$    1. Redistributions of source code must retain the above copyright
!!$       notice, this list of conditions and the following disclaimer.
!!$    2. Redistributions in binary form must reproduce the above copyright
!!$       notice, this list of conditions, and the following disclaimer in the
!!$       documentation and/or other materials provided with the distribution.
!!$    3. The name of the PSBLAS group or the names of its contributors may
!!$       not be used to endorse or promote products derived from this
!!$       software without specific written permission.
!!$ 
!!$  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
!!$  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
!!$  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!!$  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PSBLAS GROUP OR ITS CONTRIBUTORS
!!$  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
!!$  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
!!$  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
!!$  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
!!$  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
!!$  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
!!$  POSSIBILITY OF SUCH DAMAGE.
!!$ 
!!$  

function psb_get_ssp_nnzeros(a) result(ires)
  use psb_string_mod, only : psb_get_fmt
  use psb_rsb_mod, psb_protect_name => psb_get_ssp_nnzeros
  use psb_const_mod
  type(psb_sspmat_type), intent(in) :: a  
  integer :: ires,info

  if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
    call psb_rsb_get_matrix_nnz(a,ires,info)
  else
    call psb_sp_info(psb_nztotreq_,a,ires,info)
  endif
  if (info /= psb_success_) then 
    ires = 0
  end if
end function psb_get_ssp_nnzeros

function psb_get_csp_nnzeros(a) result(ires)
  use psb_string_mod, only : psb_get_fmt
  use psb_rsb_mod, psb_protect_name => psb_get_csp_nnzeros    
  use psb_const_mod

  type(psb_cspmat_type), intent(in) :: a  
  integer :: ires,info

  if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
    call psb_rsb_get_matrix_nnz(a,ires,info)
  else
    call psb_sp_info(psb_nztotreq_,a,ires,info)
  endif
  if (info /= psb_success_) then 
    ires = 0
  end if
end function psb_get_csp_nnzeros

function psb_get_dsp_nnzeros(a) result(ires)
  use psb_string_mod, only : psb_get_fmt
  use psb_rsb_mod, psb_protect_name => psb_get_dsp_nnzeros
  use psb_const_mod
  type(psb_dspmat_type), intent(in) :: a  
  integer :: ires,info

  if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
    call psb_rsb_get_matrix_nnz(a,ires,info)
  else
    call psb_sp_info(psb_nztotreq_,a,ires,info)
  endif
  if (info /= psb_success_) then 
    ires = 0
  end if
end function psb_get_dsp_nnzeros

function psb_get_zsp_nnzeros(a) result(ires)
  use psb_string_mod, only : psb_get_fmt
  use psb_rsb_mod, psb_protect_name => psb_get_zsp_nnzeros
  use psb_const_mod
  implicit none
  type(psb_zspmat_type), intent(in) :: a  
  integer :: ires,info

  if(psb_get_fmt(a%fida) == psb_rsb_afmt_) then
    call psb_rsb_get_matrix_nnz(a,ires,info)
  else
    call psb_sp_info(psb_nztotreq_,a,ires,info)
  endif
  if (info /= psb_success_) then 
    ires = 0
  end if
end function psb_get_zsp_nnzeros


