!!$ 
!!$              Parallel Sparse BLAS  version 2.2
!!$    (C) Copyright 2006/2007/2008
!!$                       Salvatore Filippone    University of Rome Tor Vergata
!!$                       Alfredo Buttari        University of Rome Tor Vergata
!!$ 
!!$  Redistribution and use in source and binary forms, with or without
!!$  modification, are permitted provided that the following conditions
!!$  are met:
!!$    1. Redistributions of source code must retain the above copyright
!!$       notice, this list of conditions and the following disclaimer.
!!$    2. Redistributions in binary form must reproduce the above copyright
!!$       notice, this list of conditions, and the following disclaimer in the
!!$       documentation and/or other materials provided with the distribution.
!!$    3. The name of the PSBLAS group or the names of its contributors may
!!$       not be used to endorse or promote products derived from this
!!$       software without specific written permission.
!!$ 
!!$  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
!!$  ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
!!$  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
!!$  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE PSBLAS GROUP OR ITS CONTRIBUTORS
!!$  BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
!!$  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
!!$  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
!!$  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
!!$  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
!!$  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
!!$  POSSIBILITY OF SUCH DAMAGE.
!!$ 
!!$  
module psb_string_mod

  private 
  
  public psb_tolower, psb_toupper, psb_touppers, psb_get_fmt_extra, psb_get_fmt

  interface psb_get_fmt
    ! martone
    module procedure psb_get_fmtc
  end interface

  interface psb_get_fmt_extra
    ! martone
    module procedure psb_get_fmt_extra_c
  end interface

  interface psb_tolower
    module procedure psb_tolowerc
  end interface

  interface psb_toupper
    module procedure psb_toupperc
  end interface

  interface psb_touppers
    module procedure psb_sub_toupperc
  end interface

  character(len=*), parameter   :: lcase='abcdefghijklmnopqrstuvwxyz'
  character(len=*), parameter   :: ucase='ABCDEFGHIJKLMNOPQRSTUVWXYZ'

contains 

  function  psb_get_fmtc(string)
  ! returns a base format string out of a matrix format info string
     character(len=*), intent(in)  :: string
     character(len=3) :: psb_get_fmtc
     psb_get_fmtc=psb_toupper(string(1:min(3,len(string))))
  end function psb_get_fmtc

  function  psb_get_fmt_extra_c(string)
  ! returns an extra format string out of a matrix format info string
    character(len=*), intent(in)  :: string
    character(len=len(string)) :: psb_get_fmt_extra_c
    ! Note: if len(string)<4, then string(4:) is legal because it
    ! is of length 0, as opposed to the previous function
    ! where we have to check explicitly for the length of the string.
    psb_get_fmt_extra_c=psb_tolower(string(4:))
  end function psb_get_fmt_extra_c


  function idx_bsrch(key,v) result(ipos)

    implicit none
    integer :: ipos
    character key
    character(len=*)  v

    integer lb, ub, m


    lb = 1 
    ub = len(v)
    ipos = 0 

    do 
      if (lb > ub) exit
      m = (lb+ub)/2
      if (key.eq.v(m:m))  then
        ipos = m 
        exit
      else if (key.lt.v(m:m))  then
        ub = m-1
      else 
        lb = m + 1
      end if
    enddo
    return
  end function idx_bsrch


  function  psb_tolowerc(string)
    character(len=*), intent(in)  :: string
    character(len=len(string))    :: psb_tolowerc
    integer  :: i,k

    do i=1,len(string)
      k = idx_bsrch(string(i:i),ucase)
      if (k /= 0) then 
        psb_tolowerc(i:i) = lcase(k:k)
      else          
        psb_tolowerc(i:i) = string(i:i)
      end if
    enddo
    
  end function psb_tolowerc

  function  psb_toupperc(string)
    character(len=*), intent(in)  :: string
    character(len=len(string))    :: psb_toupperc
    integer  :: i,k

    do i=1,len(string)
      k = idx_bsrch(string(i:i),lcase)
      if (k /= 0) then 
        psb_toupperc(i:i) = ucase(k:k)
      else          
        psb_toupperc(i:i) = string(i:i)
      end if
    enddo
  end function psb_toupperc

  subroutine   psb_sub_toupperc(string,strout)
    character(len=*), intent(in)  :: string
    character(len=*), intent(out)  :: strout
    integer  :: i,k

    do i=1,len(string)
      k = idx_bsrch(string(i:i),lcase)
      if (k /= 0) then 
        strout(i:i) = ucase(k:k)
      else          
        strout(i:i) = string(i:i)
      end if
    enddo

  end subroutine psb_sub_toupperc



end module psb_string_mod
