
# This is Make.inc file generated automatically by the PSBLAS configure script.
# It should be ready to use, included by Makefile.
# If it gives problems, consider editing it.

# These lines are quite portable.
.mod=.mod
.fh=.fh
.SUFFIXES: 
.SUFFIXES: .f90 .F90 .f .F .c .o


# The following ones are the variables used by the PSBLAS make scripts.

F90=gfortran
FC=gfortran
CC=mpicc
#F77=gfortran
F90COPT=-O3  
FCOPT=-O3  
CCOPT=-O3  
FMFLAG=-I
FIFLAG=-I
EXTRA_OPT=

# These three should be always set!
MPF90=mpif90
MPF77=mpif90
MPCC=mpicc

F90LINK=$(MPF90)
FLINK=$(MPF77)

LIBS= 

# BLAS,  METIS and other libraries.
BLAS=-lblas
METIS_LIB=
RSB_LIBS=
LAPACK=-llapack
EXTRA_COBJS=

PSBFDEFINES=-DHAVE_LAPACK -DHAVE_FLUSH_STMT -DHAVE_VOLATILE -DHAVE_MOVE_ALLOC -DMPI_MOD 
PSBCDEFINES=-DLowerUnderscore -DPtr64Bits 
AR=ar -cur
RANLIB=ranlib
CFLAGS=-g -O2
FFLAGS=-O3  

INSTALL=/usr/bin/install -c
INSTALL_DATA=${INSTALL} -m 644
INSTALL_DIR=/usr/local/psblas
INSTALL_LIBDIR=/usr/local/psblas/lib
INSTALL_INCLUDEDIR=/usr/local/psblas/include
INSTALL_DOCSDIR=/usr/local/psblas/docs

LIBDIR=lib
BASELIBNAME=libpsb_base.a
PRECLIBNAME=libpsb_prec.a
METHDLIBNAME=libpsb_krylov.a
UTILLIBNAME=libpsb_util.a


PSBLDLIBS=$(LAPACK) $(BLAS) $(METIS_LIB) $(RSB_LIBS) $(LIBS)
CDEFINES=$(PSBCDEFINES)
FDEFINES=$(PSBFDEFINES)


# These should be portable rules, arent they?
.c.o:
	$(CC) $(CCOPT) $(CINCLUDES) $(CDEFINES) -c $<
.f.o:
	$(FC) $(FCOPT) $(FINCLUDES)  -c $<
.f90.o:
	$(F90) $(F90COPT) $(FINCLUDES) -c $<
.F.o:
	$(FC)  $(FCOPT)   $(FINCLUDES) $(FDEFINES) -c $<
.F90.o:
	$(F90) $(F90COPT) $(FINCLUDES) $(FDEFINES) -c $<


