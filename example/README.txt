A test problem for Elvira:

Rectangle 1.0x0.1 cm^2 discretized with
900 quadrilateral elements. The problem
uses de TenTusscher model with stochastic
variability in the IKr current. The problem 
also incorporates extrinsic variability
as an example of the use of masks with 
Elvira to change parameters of the ionic
model.

0) Before running the example check that you
   have the openmpi_intel (version 1.4 or higher)
   and the mvapich2_intel (version 1.7 or higher)
   compilers.

   Elvira distribution comes with binary executables 
   compiled with openmpi_intel and mvapich2_intel.

   If you are planning to use a different fortran 
   compiler you will to compile the libraries in 
   elvira_libs (PSBLAS, Metis and PSBLAS) in addition
   to Elvira before runnig this example.

1) Running the example in Condor (openmpi)

   condor_submit elvira.sub

   The submit file runs Elvira in 4
   processors and generate output files in the post/
   directory

2) Running the example interactively (openmpi)
   
  i) You need to modify the script runelv_openmpi
     as indicated in the script
 ii) execute the script as

    ./runelv_openmpi 4 data/main_file.dat post/prueba_

    The script runs elvira in 4 processors saving the
    output file in post/

3) Running the example interactively in infiniband 
   (cluster hermes, zaragoza)

  i) Verify that you have selected mvapich2_intel as your
     mpi compiler
 ii) You need to connect to one of the nodes with 
     infiniband (her03-01 al her03-08)
iii) execute the script as

    ./runelv_infiniband 4 data/main_file.dat post/prueba_

    The script runs elvira in 4 processors saving the
    output file in post/. The four processes are run 
    as schedulled in the machines file
