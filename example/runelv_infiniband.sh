#!/bin/bash
# $1 number of processes
# $2 input data file with full path
# $3 output identifier with full path of output directory
# $4 log file identifier
# SOFT_PATH: path to main package folder

SOFT_PATH=/home/amb/jfrodrig/software_elvira/Elvira

nohup mpirun_rsh -hostfile machines -n $1 $SOFT_PATH/bin/mainelv_infiniband -i $2 -o $3 >& $4 &
